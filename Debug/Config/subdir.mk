################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Config/CanIf_Cfg.c \
../Config/Can_Lcfg.c \
../Config/Com_PbCfg.c \
../Config/EcuM_PBcfg.c \
../Config/Mcu_Cfg.c \
../Config/Os_Cfg.c \
../Config/PduR_PbCfg.c \
../Config/Port_Cfg.c 

OBJS += \
./Config/CanIf_Cfg.o \
./Config/Can_Lcfg.o \
./Config/Com_PbCfg.o \
./Config/EcuM_PBcfg.o \
./Config/Mcu_Cfg.o \
./Config/Os_Cfg.o \
./Config/PduR_PbCfg.o \
./Config/Port_Cfg.o 

C_DEPS += \
./Config/CanIf_Cfg.d \
./Config/Can_Lcfg.d \
./Config/Com_PbCfg.d \
./Config/EcuM_PBcfg.d \
./Config/Mcu_Cfg.d \
./Config/Os_Cfg.d \
./Config/PduR_PbCfg.d \
./Config/Port_Cfg.d 


# Each subdirectory must supply rules for building sources it contributes
Config/CanIf_Cfg.o: ../Config/CanIf_Cfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/CanIf_Cfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/Can_Lcfg.o: ../Config/Can_Lcfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/Can_Lcfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/Com_PbCfg.o: ../Config/Com_PbCfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/Com_PbCfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/EcuM_PBcfg.o: ../Config/EcuM_PBcfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/EcuM_PBcfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/Mcu_Cfg.o: ../Config/Mcu_Cfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/Mcu_Cfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/Os_Cfg.o: ../Config/Os_Cfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/Os_Cfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/PduR_PbCfg.o: ../Config/PduR_PbCfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/PduR_PbCfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Config/Port_Cfg.o: ../Config/Port_Cfg.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Config/Port_Cfg.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

