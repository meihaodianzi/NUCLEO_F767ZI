################################################################################
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../PduR/PduR.c \
../PduR/PduR_CanIf.c \
../PduR/PduR_CanTp.c \
../PduR/PduR_Com.c \
../PduR/PduR_Dcm.c \
../PduR/PduR_J1939Tp.c \
../PduR/PduR_LinIf.c \
../PduR/PduR_Logic.c \
../PduR/PduR_Routing.c \
../PduR/PduR_SoAd.c 

OBJS += \
./PduR/PduR.o \
./PduR/PduR_CanIf.o \
./PduR/PduR_CanTp.o \
./PduR/PduR_Com.o \
./PduR/PduR_Dcm.o \
./PduR/PduR_J1939Tp.o \
./PduR/PduR_LinIf.o \
./PduR/PduR_Logic.o \
./PduR/PduR_Routing.o \
./PduR/PduR_SoAd.o 

C_DEPS += \
./PduR/PduR.d \
./PduR/PduR_CanIf.d \
./PduR/PduR_CanTp.d \
./PduR/PduR_Com.d \
./PduR/PduR_Dcm.d \
./PduR/PduR_J1939Tp.d \
./PduR/PduR_LinIf.d \
./PduR/PduR_Logic.d \
./PduR/PduR_Routing.d \
./PduR/PduR_SoAd.d 


# Each subdirectory must supply rules for building sources it contributes
PduR/PduR.o: ../PduR/PduR.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_CanIf.o: ../PduR/PduR_CanIf.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_CanIf.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_CanTp.o: ../PduR/PduR_CanTp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_CanTp.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_Com.o: ../PduR/PduR_Com.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_Com.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_Dcm.o: ../PduR/PduR_Dcm.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_Dcm.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_J1939Tp.o: ../PduR/PduR_J1939Tp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_J1939Tp.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_LinIf.o: ../PduR/PduR_LinIf.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_LinIf.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_Logic.o: ../PduR/PduR_Logic.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_Logic.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_Routing.o: ../PduR/PduR_Routing.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_Routing.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
PduR/PduR_SoAd.o: ../PduR/PduR_SoAd.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32F767xx -DDEBUG -c -I../Drivers/CMSIS/Include -I../Drivers/CMSIS/Device/ST/STM32F7xx/Include -I../Drivers/STM32F7xx_HAL_Driver/Inc -I../Drivers/STM32F7xx_HAL_Driver/Inc/Legacy -I"D:/kunlo/04_F767FZI/01_cube/Autosar/CanIf" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/generic" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/include" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/diagnostic/Dcm" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Config" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Inc" -I"D:/kunlo/04_F767FZI/01_cube/Autosar/Com" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"PduR/PduR_SoAd.d" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

