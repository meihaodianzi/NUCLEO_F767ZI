﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class CANRecvData_Class
    {
        public UInt32 ExternID = 0;
        public UInt16 StandardID = 0;
        public byte IDType = 0;
        public byte DataType = 0;
        public byte FilterMatchIndex = 0;
        public byte DataLength = 0;
        public byte[] Data = new byte[8];

        public UInt32 SuccessRecvCount = 0;
    }
}
