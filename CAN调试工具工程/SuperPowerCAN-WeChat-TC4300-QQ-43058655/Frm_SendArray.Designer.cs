﻿namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    partial class Frm_SendArray
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pl2_SendArray = new System.Windows.Forms.Panel();
            this.chb_All = new System.Windows.Forms.CheckBox();
            this.btn2_CycleSendCancle = new System.Windows.Forms.Button();
            this.txb2_SendCycleTime = new System.Windows.Forms.TextBox();
            this.lab2_cycleTimeInterval = new System.Windows.Forms.Label();
            this.btn2_SendCycle = new System.Windows.Forms.Button();
            this.txb2_Specification27 = new System.Windows.Forms.TextBox();
            this.txb2_Specification28 = new System.Windows.Forms.TextBox();
            this.txb2_Specification29 = new System.Windows.Forms.TextBox();
            this.txb2_Specification30 = new System.Windows.Forms.TextBox();
            this.txb2_Specification31 = new System.Windows.Forms.TextBox();
            this.txb2_Specification32 = new System.Windows.Forms.TextBox();
            this.txb2_SendID32 = new System.Windows.Forms.TextBox();
            this.txb2_SendData32 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType32 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType32 = new System.Windows.Forms.ComboBox();
            this.btn2_Send32 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.chb2_Send32 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID31 = new System.Windows.Forms.TextBox();
            this.txb2_SendData31 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType31 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType31 = new System.Windows.Forms.ComboBox();
            this.btn2_Send31 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.chb2_Send31 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID30 = new System.Windows.Forms.TextBox();
            this.txb2_SendData30 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType30 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType30 = new System.Windows.Forms.ComboBox();
            this.btn2_Send30 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.chb2_Send30 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID29 = new System.Windows.Forms.TextBox();
            this.txb2_SendData29 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType29 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType29 = new System.Windows.Forms.ComboBox();
            this.btn2_Send29 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.chb2_Send29 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID28 = new System.Windows.Forms.TextBox();
            this.txb2_SendData28 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType28 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType28 = new System.Windows.Forms.ComboBox();
            this.btn2_Send28 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.chb2_Send28 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID27 = new System.Windows.Forms.TextBox();
            this.txb2_SendData27 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType27 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType27 = new System.Windows.Forms.ComboBox();
            this.btn2_Send27 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.chb2_Send27 = new System.Windows.Forms.CheckBox();
            this.txb2_Specification21 = new System.Windows.Forms.TextBox();
            this.txb2_Specification22 = new System.Windows.Forms.TextBox();
            this.txb2_Specification23 = new System.Windows.Forms.TextBox();
            this.txb2_Specification24 = new System.Windows.Forms.TextBox();
            this.txb2_Specification25 = new System.Windows.Forms.TextBox();
            this.txb2_Specification26 = new System.Windows.Forms.TextBox();
            this.txb2_SendID26 = new System.Windows.Forms.TextBox();
            this.txb2_SendData26 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType26 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType26 = new System.Windows.Forms.ComboBox();
            this.btn2_Send26 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chb2_Send26 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID25 = new System.Windows.Forms.TextBox();
            this.txb2_SendData25 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType25 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType25 = new System.Windows.Forms.ComboBox();
            this.btn2_Send25 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chb2_Send25 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID24 = new System.Windows.Forms.TextBox();
            this.txb2_SendData24 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType24 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType24 = new System.Windows.Forms.ComboBox();
            this.btn2_Send24 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.chb2_Send24 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID23 = new System.Windows.Forms.TextBox();
            this.txb2_SendData23 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType23 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType23 = new System.Windows.Forms.ComboBox();
            this.btn2_Send23 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chb2_Send23 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID22 = new System.Windows.Forms.TextBox();
            this.txb2_SendData22 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType22 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType22 = new System.Windows.Forms.ComboBox();
            this.btn2_Send22 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chb2_Send22 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID21 = new System.Windows.Forms.TextBox();
            this.txb2_SendData21 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType21 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType21 = new System.Windows.Forms.ComboBox();
            this.btn2_Send21 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.chb2_Send21 = new System.Windows.Forms.CheckBox();
            this.txb2_Specification1 = new System.Windows.Forms.TextBox();
            this.txb2_Specification2 = new System.Windows.Forms.TextBox();
            this.txb2_Specification3 = new System.Windows.Forms.TextBox();
            this.txb2_Specification4 = new System.Windows.Forms.TextBox();
            this.txb2_Specification5 = new System.Windows.Forms.TextBox();
            this.txb2_Specification6 = new System.Windows.Forms.TextBox();
            this.txb2_Specification7 = new System.Windows.Forms.TextBox();
            this.txb2_Specification8 = new System.Windows.Forms.TextBox();
            this.txb2_Specification9 = new System.Windows.Forms.TextBox();
            this.txb2_Specification10 = new System.Windows.Forms.TextBox();
            this.txb2_Specification11 = new System.Windows.Forms.TextBox();
            this.txb2_Specification12 = new System.Windows.Forms.TextBox();
            this.txb2_Specification13 = new System.Windows.Forms.TextBox();
            this.txb2_Specification14 = new System.Windows.Forms.TextBox();
            this.txb2_Specification15 = new System.Windows.Forms.TextBox();
            this.txb2_Specification16 = new System.Windows.Forms.TextBox();
            this.txb2_Specification17 = new System.Windows.Forms.TextBox();
            this.txb2_Specification18 = new System.Windows.Forms.TextBox();
            this.txb2_Specification19 = new System.Windows.Forms.TextBox();
            this.txb2_Specification20 = new System.Windows.Forms.TextBox();
            this.txb2_SendID20 = new System.Windows.Forms.TextBox();
            this.txb2_SendData20 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType20 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType20 = new System.Windows.Forms.ComboBox();
            this.btn2_Send20 = new System.Windows.Forms.Button();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.chb2_Send20 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID19 = new System.Windows.Forms.TextBox();
            this.txb2_SendData19 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType19 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType19 = new System.Windows.Forms.ComboBox();
            this.btn2_Send19 = new System.Windows.Forms.Button();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.chb2_Send19 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID18 = new System.Windows.Forms.TextBox();
            this.txb2_SendData18 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType18 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType18 = new System.Windows.Forms.ComboBox();
            this.btn2_Send18 = new System.Windows.Forms.Button();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.chb2_Send18 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID17 = new System.Windows.Forms.TextBox();
            this.txb2_SendData17 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType17 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType17 = new System.Windows.Forms.ComboBox();
            this.btn2_Send17 = new System.Windows.Forms.Button();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.chb2_Send17 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID16 = new System.Windows.Forms.TextBox();
            this.txb2_SendData16 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType16 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType16 = new System.Windows.Forms.ComboBox();
            this.btn2_Send16 = new System.Windows.Forms.Button();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.chb2_Send16 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID15 = new System.Windows.Forms.TextBox();
            this.txb2_SendData15 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType15 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType15 = new System.Windows.Forms.ComboBox();
            this.btn2_Send15 = new System.Windows.Forms.Button();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.chb2_Send15 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID14 = new System.Windows.Forms.TextBox();
            this.txb2_SendData14 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType14 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType14 = new System.Windows.Forms.ComboBox();
            this.btn2_Send14 = new System.Windows.Forms.Button();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.chb2_Send14 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID13 = new System.Windows.Forms.TextBox();
            this.txb2_SendData13 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType13 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType13 = new System.Windows.Forms.ComboBox();
            this.btn2_Send13 = new System.Windows.Forms.Button();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.chb2_Send13 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID12 = new System.Windows.Forms.TextBox();
            this.txb2_SendData12 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType12 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType12 = new System.Windows.Forms.ComboBox();
            this.btn2_Send12 = new System.Windows.Forms.Button();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.chb2_Send12 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID11 = new System.Windows.Forms.TextBox();
            this.txb2_SendData11 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType11 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType11 = new System.Windows.Forms.ComboBox();
            this.btn2_Send11 = new System.Windows.Forms.Button();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.chb2_Send11 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID10 = new System.Windows.Forms.TextBox();
            this.txb2_SendData10 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType10 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType10 = new System.Windows.Forms.ComboBox();
            this.btn2_Send10 = new System.Windows.Forms.Button();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.chb2_Send10 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID9 = new System.Windows.Forms.TextBox();
            this.txb2_SendData9 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType9 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType9 = new System.Windows.Forms.ComboBox();
            this.btn2_Send9 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.chb2_Send9 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID8 = new System.Windows.Forms.TextBox();
            this.txb2_SendData8 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType8 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType8 = new System.Windows.Forms.ComboBox();
            this.btn2_Send8 = new System.Windows.Forms.Button();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.chb2_Send8 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID7 = new System.Windows.Forms.TextBox();
            this.txb2_SendData7 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType7 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType7 = new System.Windows.Forms.ComboBox();
            this.btn2_Send7 = new System.Windows.Forms.Button();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.chb2_Send7 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID6 = new System.Windows.Forms.TextBox();
            this.txb2_SendData6 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType6 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType6 = new System.Windows.Forms.ComboBox();
            this.btn2_Send6 = new System.Windows.Forms.Button();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.chb2_Send6 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID5 = new System.Windows.Forms.TextBox();
            this.txb2_SendData5 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType5 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType5 = new System.Windows.Forms.ComboBox();
            this.btn2_Send5 = new System.Windows.Forms.Button();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.chb2_Send5 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID4 = new System.Windows.Forms.TextBox();
            this.txb2_SendData4 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType4 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType4 = new System.Windows.Forms.ComboBox();
            this.btn2_Send4 = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.chb2_Send4 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID3 = new System.Windows.Forms.TextBox();
            this.txb2_SendData3 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType3 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType3 = new System.Windows.Forms.ComboBox();
            this.btn2_Send3 = new System.Windows.Forms.Button();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.chb2_Send3 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID2 = new System.Windows.Forms.TextBox();
            this.txb2_SendData2 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType2 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType2 = new System.Windows.Forms.ComboBox();
            this.btn2_Send2 = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.chb2_Send2 = new System.Windows.Forms.CheckBox();
            this.txb2_SendID1 = new System.Windows.Forms.TextBox();
            this.txb2_SendData1 = new System.Windows.Forms.TextBox();
            this.chb2_SendDataType1 = new System.Windows.Forms.ComboBox();
            this.cmb2_SendIDType1 = new System.Windows.Forms.ComboBox();
            this.btn2_Send1 = new System.Windows.Forms.Button();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.chb2_Send1 = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_Shrink = new System.Windows.Forms.Button();
            this.btnPin = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.透明度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity10Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity20Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity30Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity40Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity50Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity60Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity70Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity80Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity90Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity100Percent = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_UpDowmExtend = new System.Windows.Forms.Button();
            this.btn_Adapt = new System.Windows.Forms.Button();
            this.tmr2_Send = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.pl2_SendArray.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.pl2_SendArray);
            this.groupBox1.Location = new System.Drawing.Point(0, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(700, 721);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "批量发送:";
            // 
            // pl2_SendArray
            // 
            this.pl2_SendArray.AutoScroll = true;
            this.pl2_SendArray.Controls.Add(this.chb_All);
            this.pl2_SendArray.Controls.Add(this.btn2_CycleSendCancle);
            this.pl2_SendArray.Controls.Add(this.txb2_SendCycleTime);
            this.pl2_SendArray.Controls.Add(this.lab2_cycleTimeInterval);
            this.pl2_SendArray.Controls.Add(this.btn2_SendCycle);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification27);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification28);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification29);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification30);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification31);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification32);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID32);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData32);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType32);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType32);
            this.pl2_SendArray.Controls.Add(this.btn2_Send32);
            this.pl2_SendArray.Controls.Add(this.label25);
            this.pl2_SendArray.Controls.Add(this.label26);
            this.pl2_SendArray.Controls.Add(this.label27);
            this.pl2_SendArray.Controls.Add(this.label28);
            this.pl2_SendArray.Controls.Add(this.chb2_Send32);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID31);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData31);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType31);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType31);
            this.pl2_SendArray.Controls.Add(this.btn2_Send31);
            this.pl2_SendArray.Controls.Add(this.label29);
            this.pl2_SendArray.Controls.Add(this.label30);
            this.pl2_SendArray.Controls.Add(this.label31);
            this.pl2_SendArray.Controls.Add(this.label32);
            this.pl2_SendArray.Controls.Add(this.chb2_Send31);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID30);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData30);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType30);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType30);
            this.pl2_SendArray.Controls.Add(this.btn2_Send30);
            this.pl2_SendArray.Controls.Add(this.label33);
            this.pl2_SendArray.Controls.Add(this.label34);
            this.pl2_SendArray.Controls.Add(this.label35);
            this.pl2_SendArray.Controls.Add(this.label36);
            this.pl2_SendArray.Controls.Add(this.chb2_Send30);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID29);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData29);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType29);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType29);
            this.pl2_SendArray.Controls.Add(this.btn2_Send29);
            this.pl2_SendArray.Controls.Add(this.label37);
            this.pl2_SendArray.Controls.Add(this.label38);
            this.pl2_SendArray.Controls.Add(this.label39);
            this.pl2_SendArray.Controls.Add(this.label40);
            this.pl2_SendArray.Controls.Add(this.chb2_Send29);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID28);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData28);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType28);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType28);
            this.pl2_SendArray.Controls.Add(this.btn2_Send28);
            this.pl2_SendArray.Controls.Add(this.label41);
            this.pl2_SendArray.Controls.Add(this.label42);
            this.pl2_SendArray.Controls.Add(this.label43);
            this.pl2_SendArray.Controls.Add(this.label44);
            this.pl2_SendArray.Controls.Add(this.chb2_Send28);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID27);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData27);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType27);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType27);
            this.pl2_SendArray.Controls.Add(this.btn2_Send27);
            this.pl2_SendArray.Controls.Add(this.label45);
            this.pl2_SendArray.Controls.Add(this.label46);
            this.pl2_SendArray.Controls.Add(this.label47);
            this.pl2_SendArray.Controls.Add(this.label48);
            this.pl2_SendArray.Controls.Add(this.chb2_Send27);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification21);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification22);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification23);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification24);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification25);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification26);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID26);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData26);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType26);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType26);
            this.pl2_SendArray.Controls.Add(this.btn2_Send26);
            this.pl2_SendArray.Controls.Add(this.label1);
            this.pl2_SendArray.Controls.Add(this.label2);
            this.pl2_SendArray.Controls.Add(this.label3);
            this.pl2_SendArray.Controls.Add(this.label4);
            this.pl2_SendArray.Controls.Add(this.chb2_Send26);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID25);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData25);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType25);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType25);
            this.pl2_SendArray.Controls.Add(this.btn2_Send25);
            this.pl2_SendArray.Controls.Add(this.label5);
            this.pl2_SendArray.Controls.Add(this.label6);
            this.pl2_SendArray.Controls.Add(this.label7);
            this.pl2_SendArray.Controls.Add(this.label8);
            this.pl2_SendArray.Controls.Add(this.chb2_Send25);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID24);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData24);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType24);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType24);
            this.pl2_SendArray.Controls.Add(this.btn2_Send24);
            this.pl2_SendArray.Controls.Add(this.label9);
            this.pl2_SendArray.Controls.Add(this.label10);
            this.pl2_SendArray.Controls.Add(this.label11);
            this.pl2_SendArray.Controls.Add(this.label12);
            this.pl2_SendArray.Controls.Add(this.chb2_Send24);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID23);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData23);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType23);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType23);
            this.pl2_SendArray.Controls.Add(this.btn2_Send23);
            this.pl2_SendArray.Controls.Add(this.label13);
            this.pl2_SendArray.Controls.Add(this.label14);
            this.pl2_SendArray.Controls.Add(this.label15);
            this.pl2_SendArray.Controls.Add(this.label16);
            this.pl2_SendArray.Controls.Add(this.chb2_Send23);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID22);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData22);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType22);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType22);
            this.pl2_SendArray.Controls.Add(this.btn2_Send22);
            this.pl2_SendArray.Controls.Add(this.label17);
            this.pl2_SendArray.Controls.Add(this.label18);
            this.pl2_SendArray.Controls.Add(this.label19);
            this.pl2_SendArray.Controls.Add(this.label20);
            this.pl2_SendArray.Controls.Add(this.chb2_Send22);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID21);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData21);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType21);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType21);
            this.pl2_SendArray.Controls.Add(this.btn2_Send21);
            this.pl2_SendArray.Controls.Add(this.label21);
            this.pl2_SendArray.Controls.Add(this.label22);
            this.pl2_SendArray.Controls.Add(this.label23);
            this.pl2_SendArray.Controls.Add(this.label24);
            this.pl2_SendArray.Controls.Add(this.chb2_Send21);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification1);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification2);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification3);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification4);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification5);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification6);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification7);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification8);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification9);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification10);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification11);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification12);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification13);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification14);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification15);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification16);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification17);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification18);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification19);
            this.pl2_SendArray.Controls.Add(this.txb2_Specification20);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID20);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData20);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType20);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType20);
            this.pl2_SendArray.Controls.Add(this.btn2_Send20);
            this.pl2_SendArray.Controls.Add(this.label153);
            this.pl2_SendArray.Controls.Add(this.label154);
            this.pl2_SendArray.Controls.Add(this.label155);
            this.pl2_SendArray.Controls.Add(this.label156);
            this.pl2_SendArray.Controls.Add(this.chb2_Send20);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID19);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData19);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType19);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType19);
            this.pl2_SendArray.Controls.Add(this.btn2_Send19);
            this.pl2_SendArray.Controls.Add(this.label157);
            this.pl2_SendArray.Controls.Add(this.label158);
            this.pl2_SendArray.Controls.Add(this.label159);
            this.pl2_SendArray.Controls.Add(this.label160);
            this.pl2_SendArray.Controls.Add(this.chb2_Send19);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID18);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData18);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType18);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType18);
            this.pl2_SendArray.Controls.Add(this.btn2_Send18);
            this.pl2_SendArray.Controls.Add(this.label145);
            this.pl2_SendArray.Controls.Add(this.label146);
            this.pl2_SendArray.Controls.Add(this.label147);
            this.pl2_SendArray.Controls.Add(this.label148);
            this.pl2_SendArray.Controls.Add(this.chb2_Send18);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID17);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData17);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType17);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType17);
            this.pl2_SendArray.Controls.Add(this.btn2_Send17);
            this.pl2_SendArray.Controls.Add(this.label149);
            this.pl2_SendArray.Controls.Add(this.label150);
            this.pl2_SendArray.Controls.Add(this.label151);
            this.pl2_SendArray.Controls.Add(this.label152);
            this.pl2_SendArray.Controls.Add(this.chb2_Send17);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID16);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData16);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType16);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType16);
            this.pl2_SendArray.Controls.Add(this.btn2_Send16);
            this.pl2_SendArray.Controls.Add(this.label129);
            this.pl2_SendArray.Controls.Add(this.label130);
            this.pl2_SendArray.Controls.Add(this.label131);
            this.pl2_SendArray.Controls.Add(this.label132);
            this.pl2_SendArray.Controls.Add(this.chb2_Send16);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID15);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData15);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType15);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType15);
            this.pl2_SendArray.Controls.Add(this.btn2_Send15);
            this.pl2_SendArray.Controls.Add(this.label133);
            this.pl2_SendArray.Controls.Add(this.label134);
            this.pl2_SendArray.Controls.Add(this.label135);
            this.pl2_SendArray.Controls.Add(this.label136);
            this.pl2_SendArray.Controls.Add(this.chb2_Send15);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID14);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData14);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType14);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType14);
            this.pl2_SendArray.Controls.Add(this.btn2_Send14);
            this.pl2_SendArray.Controls.Add(this.label137);
            this.pl2_SendArray.Controls.Add(this.label138);
            this.pl2_SendArray.Controls.Add(this.label139);
            this.pl2_SendArray.Controls.Add(this.label140);
            this.pl2_SendArray.Controls.Add(this.chb2_Send14);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID13);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData13);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType13);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType13);
            this.pl2_SendArray.Controls.Add(this.btn2_Send13);
            this.pl2_SendArray.Controls.Add(this.label141);
            this.pl2_SendArray.Controls.Add(this.label142);
            this.pl2_SendArray.Controls.Add(this.label143);
            this.pl2_SendArray.Controls.Add(this.label144);
            this.pl2_SendArray.Controls.Add(this.chb2_Send13);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID12);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData12);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType12);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType12);
            this.pl2_SendArray.Controls.Add(this.btn2_Send12);
            this.pl2_SendArray.Controls.Add(this.label117);
            this.pl2_SendArray.Controls.Add(this.label118);
            this.pl2_SendArray.Controls.Add(this.label119);
            this.pl2_SendArray.Controls.Add(this.label120);
            this.pl2_SendArray.Controls.Add(this.chb2_Send12);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID11);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData11);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType11);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType11);
            this.pl2_SendArray.Controls.Add(this.btn2_Send11);
            this.pl2_SendArray.Controls.Add(this.label121);
            this.pl2_SendArray.Controls.Add(this.label122);
            this.pl2_SendArray.Controls.Add(this.label123);
            this.pl2_SendArray.Controls.Add(this.label124);
            this.pl2_SendArray.Controls.Add(this.chb2_Send11);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID10);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData10);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType10);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType10);
            this.pl2_SendArray.Controls.Add(this.btn2_Send10);
            this.pl2_SendArray.Controls.Add(this.label125);
            this.pl2_SendArray.Controls.Add(this.label126);
            this.pl2_SendArray.Controls.Add(this.label127);
            this.pl2_SendArray.Controls.Add(this.label128);
            this.pl2_SendArray.Controls.Add(this.chb2_Send10);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID9);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData9);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType9);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType9);
            this.pl2_SendArray.Controls.Add(this.btn2_Send9);
            this.pl2_SendArray.Controls.Add(this.label105);
            this.pl2_SendArray.Controls.Add(this.label106);
            this.pl2_SendArray.Controls.Add(this.label107);
            this.pl2_SendArray.Controls.Add(this.label108);
            this.pl2_SendArray.Controls.Add(this.chb2_Send9);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID8);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData8);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType8);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType8);
            this.pl2_SendArray.Controls.Add(this.btn2_Send8);
            this.pl2_SendArray.Controls.Add(this.label109);
            this.pl2_SendArray.Controls.Add(this.label110);
            this.pl2_SendArray.Controls.Add(this.label111);
            this.pl2_SendArray.Controls.Add(this.label112);
            this.pl2_SendArray.Controls.Add(this.chb2_Send8);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID7);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData7);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType7);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType7);
            this.pl2_SendArray.Controls.Add(this.btn2_Send7);
            this.pl2_SendArray.Controls.Add(this.label113);
            this.pl2_SendArray.Controls.Add(this.label114);
            this.pl2_SendArray.Controls.Add(this.label115);
            this.pl2_SendArray.Controls.Add(this.label116);
            this.pl2_SendArray.Controls.Add(this.chb2_Send7);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID6);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData6);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType6);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType6);
            this.pl2_SendArray.Controls.Add(this.btn2_Send6);
            this.pl2_SendArray.Controls.Add(this.label93);
            this.pl2_SendArray.Controls.Add(this.label94);
            this.pl2_SendArray.Controls.Add(this.label95);
            this.pl2_SendArray.Controls.Add(this.label96);
            this.pl2_SendArray.Controls.Add(this.chb2_Send6);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID5);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData5);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType5);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType5);
            this.pl2_SendArray.Controls.Add(this.btn2_Send5);
            this.pl2_SendArray.Controls.Add(this.label97);
            this.pl2_SendArray.Controls.Add(this.label98);
            this.pl2_SendArray.Controls.Add(this.label99);
            this.pl2_SendArray.Controls.Add(this.label100);
            this.pl2_SendArray.Controls.Add(this.chb2_Send5);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID4);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData4);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType4);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType4);
            this.pl2_SendArray.Controls.Add(this.btn2_Send4);
            this.pl2_SendArray.Controls.Add(this.label101);
            this.pl2_SendArray.Controls.Add(this.label102);
            this.pl2_SendArray.Controls.Add(this.label103);
            this.pl2_SendArray.Controls.Add(this.label104);
            this.pl2_SendArray.Controls.Add(this.chb2_Send4);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID3);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData3);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType3);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType3);
            this.pl2_SendArray.Controls.Add(this.btn2_Send3);
            this.pl2_SendArray.Controls.Add(this.label89);
            this.pl2_SendArray.Controls.Add(this.label90);
            this.pl2_SendArray.Controls.Add(this.label91);
            this.pl2_SendArray.Controls.Add(this.label92);
            this.pl2_SendArray.Controls.Add(this.chb2_Send3);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID2);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData2);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType2);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType2);
            this.pl2_SendArray.Controls.Add(this.btn2_Send2);
            this.pl2_SendArray.Controls.Add(this.label85);
            this.pl2_SendArray.Controls.Add(this.label86);
            this.pl2_SendArray.Controls.Add(this.label87);
            this.pl2_SendArray.Controls.Add(this.label88);
            this.pl2_SendArray.Controls.Add(this.chb2_Send2);
            this.pl2_SendArray.Controls.Add(this.txb2_SendID1);
            this.pl2_SendArray.Controls.Add(this.txb2_SendData1);
            this.pl2_SendArray.Controls.Add(this.chb2_SendDataType1);
            this.pl2_SendArray.Controls.Add(this.cmb2_SendIDType1);
            this.pl2_SendArray.Controls.Add(this.btn2_Send1);
            this.pl2_SendArray.Controls.Add(this.label81);
            this.pl2_SendArray.Controls.Add(this.label82);
            this.pl2_SendArray.Controls.Add(this.label83);
            this.pl2_SendArray.Controls.Add(this.label84);
            this.pl2_SendArray.Controls.Add(this.chb2_Send1);
            this.pl2_SendArray.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl2_SendArray.Location = new System.Drawing.Point(3, 17);
            this.pl2_SendArray.Name = "pl2_SendArray";
            this.pl2_SendArray.Size = new System.Drawing.Size(694, 701);
            this.pl2_SendArray.TabIndex = 0;
            this.pl2_SendArray.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pl2_SendArray_MouseClick);
            // 
            // chb_All
            // 
            this.chb_All.AutoSize = true;
            this.chb_All.Location = new System.Drawing.Point(9, 827);
            this.chb_All.Name = "chb_All";
            this.chb_All.Size = new System.Drawing.Size(48, 16);
            this.chb_All.TabIndex = 596;
            this.chb_All.Tag = "";
            this.chb_All.Text = "全选";
            this.chb_All.UseVisualStyleBackColor = true;
            this.chb_All.CheckedChanged += new System.EventHandler(this.chb_All_CheckedChanged);
            // 
            // btn2_CycleSendCancle
            // 
            this.btn2_CycleSendCancle.Location = new System.Drawing.Point(476, 823);
            this.btn2_CycleSendCancle.Name = "btn2_CycleSendCancle";
            this.btn2_CycleSendCancle.Size = new System.Drawing.Size(75, 23);
            this.btn2_CycleSendCancle.TabIndex = 595;
            this.btn2_CycleSendCancle.Tag = "0";
            this.btn2_CycleSendCancle.Text = "停止";
            this.btn2_CycleSendCancle.UseVisualStyleBackColor = true;
            this.btn2_CycleSendCancle.Click += new System.EventHandler(this.btn2_CycleSendCancle_Click);
            // 
            // txb2_SendCycleTime
            // 
            this.txb2_SendCycleTime.Location = new System.Drawing.Point(613, 825);
            this.txb2_SendCycleTime.Multiline = true;
            this.txb2_SendCycleTime.Name = "txb2_SendCycleTime";
            this.txb2_SendCycleTime.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendCycleTime.TabIndex = 594;
            this.txb2_SendCycleTime.Text = "1000";
            // 
            // lab2_cycleTimeInterval
            // 
            this.lab2_cycleTimeInterval.AutoSize = true;
            this.lab2_cycleTimeInterval.Location = new System.Drawing.Point(557, 828);
            this.lab2_cycleTimeInterval.Name = "lab2_cycleTimeInterval";
            this.lab2_cycleTimeInterval.Size = new System.Drawing.Size(59, 12);
            this.lab2_cycleTimeInterval.TabIndex = 593;
            this.lab2_cycleTimeInterval.Text = "间隔(ms):";
            // 
            // btn2_SendCycle
            // 
            this.btn2_SendCycle.Location = new System.Drawing.Point(305, 823);
            this.btn2_SendCycle.Name = "btn2_SendCycle";
            this.btn2_SendCycle.Size = new System.Drawing.Size(165, 23);
            this.btn2_SendCycle.TabIndex = 592;
            this.btn2_SendCycle.Tag = "1";
            this.btn2_SendCycle.Text = "循环发送选中数据";
            this.btn2_SendCycle.UseVisualStyleBackColor = true;
            this.btn2_SendCycle.Click += new System.EventHandler(this.btn2_SendCycle_Click);
            // 
            // txb2_Specification27
            // 
            this.txb2_Specification27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification27.Location = new System.Drawing.Point(44, 669);
            this.txb2_Specification27.MaxLength = 12;
            this.txb2_Specification27.Multiline = true;
            this.txb2_Specification27.Name = "txb2_Specification27";
            this.txb2_Specification27.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification27.TabIndex = 591;
            // 
            // txb2_Specification28
            // 
            this.txb2_Specification28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification28.Location = new System.Drawing.Point(44, 695);
            this.txb2_Specification28.MaxLength = 12;
            this.txb2_Specification28.Multiline = true;
            this.txb2_Specification28.Name = "txb2_Specification28";
            this.txb2_Specification28.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification28.TabIndex = 590;
            // 
            // txb2_Specification29
            // 
            this.txb2_Specification29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification29.Location = new System.Drawing.Point(44, 721);
            this.txb2_Specification29.MaxLength = 12;
            this.txb2_Specification29.Multiline = true;
            this.txb2_Specification29.Name = "txb2_Specification29";
            this.txb2_Specification29.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification29.TabIndex = 589;
            // 
            // txb2_Specification30
            // 
            this.txb2_Specification30.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification30.Location = new System.Drawing.Point(44, 747);
            this.txb2_Specification30.MaxLength = 12;
            this.txb2_Specification30.Multiline = true;
            this.txb2_Specification30.Name = "txb2_Specification30";
            this.txb2_Specification30.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification30.TabIndex = 588;
            // 
            // txb2_Specification31
            // 
            this.txb2_Specification31.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification31.Location = new System.Drawing.Point(44, 773);
            this.txb2_Specification31.MaxLength = 12;
            this.txb2_Specification31.Multiline = true;
            this.txb2_Specification31.Name = "txb2_Specification31";
            this.txb2_Specification31.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification31.TabIndex = 587;
            // 
            // txb2_Specification32
            // 
            this.txb2_Specification32.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification32.Location = new System.Drawing.Point(44, 799);
            this.txb2_Specification32.MaxLength = 12;
            this.txb2_Specification32.Multiline = true;
            this.txb2_Specification32.Name = "txb2_Specification32";
            this.txb2_Specification32.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification32.TabIndex = 586;
            // 
            // txb2_SendID32
            // 
            this.txb2_SendID32.Location = new System.Drawing.Point(380, 798);
            this.txb2_SendID32.Multiline = true;
            this.txb2_SendID32.Name = "txb2_SendID32";
            this.txb2_SendID32.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID32.TabIndex = 585;
            this.txb2_SendID32.Tag = "32";
            this.txb2_SendID32.Text = "01234567";
            // 
            // txb2_SendData32
            // 
            this.txb2_SendData32.Location = new System.Drawing.Point(473, 798);
            this.txb2_SendData32.Multiline = true;
            this.txb2_SendData32.Name = "txb2_SendData32";
            this.txb2_SendData32.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData32.TabIndex = 584;
            this.txb2_SendData32.Tag = "32";
            this.txb2_SendData32.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType32
            // 
            this.chb2_SendDataType32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType32.FormattingEnabled = true;
            this.chb2_SendDataType32.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType32.Location = new System.Drawing.Point(284, 797);
            this.chb2_SendDataType32.Name = "chb2_SendDataType32";
            this.chb2_SendDataType32.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType32.TabIndex = 583;
            this.chb2_SendDataType32.Tag = "32";
            this.chb2_SendDataType32.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType32
            // 
            this.cmb2_SendIDType32.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType32.FormattingEnabled = true;
            this.cmb2_SendIDType32.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType32.Location = new System.Drawing.Point(177, 797);
            this.cmb2_SendIDType32.Name = "cmb2_SendIDType32";
            this.cmb2_SendIDType32.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType32.TabIndex = 582;
            this.cmb2_SendIDType32.Tag = "32";
            // 
            // btn2_Send32
            // 
            this.btn2_Send32.Location = new System.Drawing.Point(623, 797);
            this.btn2_Send32.Name = "btn2_Send32";
            this.btn2_Send32.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send32.TabIndex = 581;
            this.btn2_Send32.Tag = "32";
            this.btn2_Send32.Text = "发送";
            this.btn2_Send32.UseVisualStyleBackColor = true;
            this.btn2_Send32.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(441, 801);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 12);
            this.label25.TabIndex = 580;
            this.label25.Text = "数据：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(348, 798);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 12);
            this.label26.TabIndex = 579;
            this.label26.Text = "帧ID：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(237, 801);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 12);
            this.label27.TabIndex = 578;
            this.label27.Text = "帧类型：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(128, 801);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 12);
            this.label28.TabIndex = 577;
            this.label28.Text = "帧格式：";
            // 
            // chb2_Send32
            // 
            this.chb2_Send32.AutoSize = true;
            this.chb2_Send32.Location = new System.Drawing.Point(9, 800);
            this.chb2_Send32.Name = "chb2_Send32";
            this.chb2_Send32.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send32.TabIndex = 576;
            this.chb2_Send32.Tag = "32";
            this.chb2_Send32.Text = "32:";
            this.chb2_Send32.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID31
            // 
            this.txb2_SendID31.Location = new System.Drawing.Point(380, 772);
            this.txb2_SendID31.Multiline = true;
            this.txb2_SendID31.Name = "txb2_SendID31";
            this.txb2_SendID31.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID31.TabIndex = 575;
            this.txb2_SendID31.Tag = "31";
            this.txb2_SendID31.Text = "01234567";
            // 
            // txb2_SendData31
            // 
            this.txb2_SendData31.Location = new System.Drawing.Point(473, 772);
            this.txb2_SendData31.Multiline = true;
            this.txb2_SendData31.Name = "txb2_SendData31";
            this.txb2_SendData31.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData31.TabIndex = 574;
            this.txb2_SendData31.Tag = "31";
            this.txb2_SendData31.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType31
            // 
            this.chb2_SendDataType31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType31.FormattingEnabled = true;
            this.chb2_SendDataType31.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType31.Location = new System.Drawing.Point(284, 771);
            this.chb2_SendDataType31.Name = "chb2_SendDataType31";
            this.chb2_SendDataType31.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType31.TabIndex = 573;
            this.chb2_SendDataType31.Tag = "31";
            this.chb2_SendDataType31.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType31
            // 
            this.cmb2_SendIDType31.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType31.FormattingEnabled = true;
            this.cmb2_SendIDType31.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType31.Location = new System.Drawing.Point(177, 771);
            this.cmb2_SendIDType31.Name = "cmb2_SendIDType31";
            this.cmb2_SendIDType31.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType31.TabIndex = 572;
            this.cmb2_SendIDType31.Tag = "31";
            // 
            // btn2_Send31
            // 
            this.btn2_Send31.Location = new System.Drawing.Point(623, 771);
            this.btn2_Send31.Name = "btn2_Send31";
            this.btn2_Send31.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send31.TabIndex = 571;
            this.btn2_Send31.Tag = "31";
            this.btn2_Send31.Text = "发送";
            this.btn2_Send31.UseVisualStyleBackColor = true;
            this.btn2_Send31.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(441, 775);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 12);
            this.label29.TabIndex = 570;
            this.label29.Text = "数据：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(348, 775);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 569;
            this.label30.Text = "帧ID：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(237, 775);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 12);
            this.label31.TabIndex = 568;
            this.label31.Text = "帧类型：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(128, 775);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 567;
            this.label32.Text = "帧格式：";
            // 
            // chb2_Send31
            // 
            this.chb2_Send31.AutoSize = true;
            this.chb2_Send31.Location = new System.Drawing.Point(9, 774);
            this.chb2_Send31.Name = "chb2_Send31";
            this.chb2_Send31.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send31.TabIndex = 566;
            this.chb2_Send31.Tag = "31";
            this.chb2_Send31.Text = "31:";
            this.chb2_Send31.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID30
            // 
            this.txb2_SendID30.Location = new System.Drawing.Point(380, 746);
            this.txb2_SendID30.Multiline = true;
            this.txb2_SendID30.Name = "txb2_SendID30";
            this.txb2_SendID30.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID30.TabIndex = 565;
            this.txb2_SendID30.Tag = "30";
            this.txb2_SendID30.Text = "01234567";
            // 
            // txb2_SendData30
            // 
            this.txb2_SendData30.Location = new System.Drawing.Point(473, 746);
            this.txb2_SendData30.Multiline = true;
            this.txb2_SendData30.Name = "txb2_SendData30";
            this.txb2_SendData30.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData30.TabIndex = 564;
            this.txb2_SendData30.Tag = "30";
            this.txb2_SendData30.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType30
            // 
            this.chb2_SendDataType30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType30.FormattingEnabled = true;
            this.chb2_SendDataType30.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType30.Location = new System.Drawing.Point(284, 745);
            this.chb2_SendDataType30.Name = "chb2_SendDataType30";
            this.chb2_SendDataType30.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType30.TabIndex = 563;
            this.chb2_SendDataType30.Tag = "30";
            this.chb2_SendDataType30.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType30
            // 
            this.cmb2_SendIDType30.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType30.FormattingEnabled = true;
            this.cmb2_SendIDType30.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType30.Location = new System.Drawing.Point(177, 745);
            this.cmb2_SendIDType30.Name = "cmb2_SendIDType30";
            this.cmb2_SendIDType30.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType30.TabIndex = 562;
            this.cmb2_SendIDType30.Tag = "30";
            // 
            // btn2_Send30
            // 
            this.btn2_Send30.Location = new System.Drawing.Point(623, 745);
            this.btn2_Send30.Name = "btn2_Send30";
            this.btn2_Send30.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send30.TabIndex = 561;
            this.btn2_Send30.Tag = "30";
            this.btn2_Send30.Text = "发送";
            this.btn2_Send30.UseVisualStyleBackColor = true;
            this.btn2_Send30.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(441, 749);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 560;
            this.label33.Text = "数据：";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(348, 749);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 12);
            this.label34.TabIndex = 559;
            this.label34.Text = "帧ID：";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(237, 749);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 558;
            this.label35.Text = "帧类型：";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(128, 749);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(53, 12);
            this.label36.TabIndex = 557;
            this.label36.Text = "帧格式：";
            // 
            // chb2_Send30
            // 
            this.chb2_Send30.AutoSize = true;
            this.chb2_Send30.Location = new System.Drawing.Point(9, 748);
            this.chb2_Send30.Name = "chb2_Send30";
            this.chb2_Send30.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send30.TabIndex = 556;
            this.chb2_Send30.Tag = "30";
            this.chb2_Send30.Text = "30:";
            this.chb2_Send30.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID29
            // 
            this.txb2_SendID29.Location = new System.Drawing.Point(380, 720);
            this.txb2_SendID29.Multiline = true;
            this.txb2_SendID29.Name = "txb2_SendID29";
            this.txb2_SendID29.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID29.TabIndex = 555;
            this.txb2_SendID29.Tag = "29";
            this.txb2_SendID29.Text = "01234567";
            // 
            // txb2_SendData29
            // 
            this.txb2_SendData29.Location = new System.Drawing.Point(473, 720);
            this.txb2_SendData29.Multiline = true;
            this.txb2_SendData29.Name = "txb2_SendData29";
            this.txb2_SendData29.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData29.TabIndex = 554;
            this.txb2_SendData29.Tag = "29";
            this.txb2_SendData29.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType29
            // 
            this.chb2_SendDataType29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType29.FormattingEnabled = true;
            this.chb2_SendDataType29.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType29.Location = new System.Drawing.Point(284, 719);
            this.chb2_SendDataType29.Name = "chb2_SendDataType29";
            this.chb2_SendDataType29.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType29.TabIndex = 553;
            this.chb2_SendDataType29.Tag = "29";
            this.chb2_SendDataType29.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType29
            // 
            this.cmb2_SendIDType29.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType29.FormattingEnabled = true;
            this.cmb2_SendIDType29.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType29.Location = new System.Drawing.Point(177, 719);
            this.cmb2_SendIDType29.Name = "cmb2_SendIDType29";
            this.cmb2_SendIDType29.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType29.TabIndex = 552;
            this.cmb2_SendIDType29.Tag = "29";
            // 
            // btn2_Send29
            // 
            this.btn2_Send29.Location = new System.Drawing.Point(623, 719);
            this.btn2_Send29.Name = "btn2_Send29";
            this.btn2_Send29.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send29.TabIndex = 551;
            this.btn2_Send29.Tag = "29";
            this.btn2_Send29.Text = "发送";
            this.btn2_Send29.UseVisualStyleBackColor = true;
            this.btn2_Send29.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(441, 723);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 12);
            this.label37.TabIndex = 550;
            this.label37.Text = "数据：";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(348, 723);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 549;
            this.label38.Text = "帧ID：";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(237, 723);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 548;
            this.label39.Text = "帧类型：";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(128, 723);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 12);
            this.label40.TabIndex = 547;
            this.label40.Text = "帧格式：";
            // 
            // chb2_Send29
            // 
            this.chb2_Send29.AutoSize = true;
            this.chb2_Send29.Location = new System.Drawing.Point(9, 722);
            this.chb2_Send29.Name = "chb2_Send29";
            this.chb2_Send29.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send29.TabIndex = 546;
            this.chb2_Send29.Tag = "29";
            this.chb2_Send29.Text = "29:";
            this.chb2_Send29.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID28
            // 
            this.txb2_SendID28.Location = new System.Drawing.Point(380, 694);
            this.txb2_SendID28.Multiline = true;
            this.txb2_SendID28.Name = "txb2_SendID28";
            this.txb2_SendID28.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID28.TabIndex = 545;
            this.txb2_SendID28.Tag = "28";
            this.txb2_SendID28.Text = "01234567";
            // 
            // txb2_SendData28
            // 
            this.txb2_SendData28.Location = new System.Drawing.Point(473, 694);
            this.txb2_SendData28.Multiline = true;
            this.txb2_SendData28.Name = "txb2_SendData28";
            this.txb2_SendData28.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData28.TabIndex = 544;
            this.txb2_SendData28.Tag = "28";
            this.txb2_SendData28.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType28
            // 
            this.chb2_SendDataType28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType28.FormattingEnabled = true;
            this.chb2_SendDataType28.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType28.Location = new System.Drawing.Point(284, 693);
            this.chb2_SendDataType28.Name = "chb2_SendDataType28";
            this.chb2_SendDataType28.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType28.TabIndex = 543;
            this.chb2_SendDataType28.Tag = "28";
            this.chb2_SendDataType28.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType28
            // 
            this.cmb2_SendIDType28.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType28.FormattingEnabled = true;
            this.cmb2_SendIDType28.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType28.Location = new System.Drawing.Point(177, 693);
            this.cmb2_SendIDType28.Name = "cmb2_SendIDType28";
            this.cmb2_SendIDType28.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType28.TabIndex = 542;
            this.cmb2_SendIDType28.Tag = "28";
            // 
            // btn2_Send28
            // 
            this.btn2_Send28.Location = new System.Drawing.Point(623, 693);
            this.btn2_Send28.Name = "btn2_Send28";
            this.btn2_Send28.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send28.TabIndex = 541;
            this.btn2_Send28.Tag = "28";
            this.btn2_Send28.Text = "发送";
            this.btn2_Send28.UseVisualStyleBackColor = true;
            this.btn2_Send28.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(441, 697);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 12);
            this.label41.TabIndex = 540;
            this.label41.Text = "数据：";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(348, 697);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 12);
            this.label42.TabIndex = 539;
            this.label42.Text = "帧ID：";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(237, 697);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(53, 12);
            this.label43.TabIndex = 538;
            this.label43.Text = "帧类型：";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(128, 697);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(53, 12);
            this.label44.TabIndex = 537;
            this.label44.Text = "帧格式：";
            // 
            // chb2_Send28
            // 
            this.chb2_Send28.AutoSize = true;
            this.chb2_Send28.Location = new System.Drawing.Point(9, 696);
            this.chb2_Send28.Name = "chb2_Send28";
            this.chb2_Send28.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send28.TabIndex = 536;
            this.chb2_Send28.Tag = "28";
            this.chb2_Send28.Text = "28:";
            this.chb2_Send28.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID27
            // 
            this.txb2_SendID27.Location = new System.Drawing.Point(380, 668);
            this.txb2_SendID27.Multiline = true;
            this.txb2_SendID27.Name = "txb2_SendID27";
            this.txb2_SendID27.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID27.TabIndex = 535;
            this.txb2_SendID27.Tag = "27";
            this.txb2_SendID27.Text = "01234567";
            // 
            // txb2_SendData27
            // 
            this.txb2_SendData27.Location = new System.Drawing.Point(473, 668);
            this.txb2_SendData27.Multiline = true;
            this.txb2_SendData27.Name = "txb2_SendData27";
            this.txb2_SendData27.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData27.TabIndex = 534;
            this.txb2_SendData27.Tag = "27";
            this.txb2_SendData27.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType27
            // 
            this.chb2_SendDataType27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType27.FormattingEnabled = true;
            this.chb2_SendDataType27.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType27.Location = new System.Drawing.Point(284, 667);
            this.chb2_SendDataType27.Name = "chb2_SendDataType27";
            this.chb2_SendDataType27.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType27.TabIndex = 533;
            this.chb2_SendDataType27.Tag = "27";
            this.chb2_SendDataType27.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType27
            // 
            this.cmb2_SendIDType27.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType27.FormattingEnabled = true;
            this.cmb2_SendIDType27.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType27.Location = new System.Drawing.Point(177, 667);
            this.cmb2_SendIDType27.Name = "cmb2_SendIDType27";
            this.cmb2_SendIDType27.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType27.TabIndex = 532;
            this.cmb2_SendIDType27.Tag = "27";
            // 
            // btn2_Send27
            // 
            this.btn2_Send27.Location = new System.Drawing.Point(623, 667);
            this.btn2_Send27.Name = "btn2_Send27";
            this.btn2_Send27.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send27.TabIndex = 531;
            this.btn2_Send27.Tag = "27";
            this.btn2_Send27.Text = "发送";
            this.btn2_Send27.UseVisualStyleBackColor = true;
            this.btn2_Send27.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(441, 671);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 12);
            this.label45.TabIndex = 530;
            this.label45.Text = "数据：";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(348, 671);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 12);
            this.label46.TabIndex = 529;
            this.label46.Text = "帧ID：";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(237, 671);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(53, 12);
            this.label47.TabIndex = 528;
            this.label47.Text = "帧类型：";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(128, 671);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(53, 12);
            this.label48.TabIndex = 527;
            this.label48.Text = "帧格式：";
            // 
            // chb2_Send27
            // 
            this.chb2_Send27.AutoSize = true;
            this.chb2_Send27.Location = new System.Drawing.Point(9, 670);
            this.chb2_Send27.Name = "chb2_Send27";
            this.chb2_Send27.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send27.TabIndex = 526;
            this.chb2_Send27.Tag = "27";
            this.chb2_Send27.Text = "27:";
            this.chb2_Send27.UseVisualStyleBackColor = true;
            // 
            // txb2_Specification21
            // 
            this.txb2_Specification21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification21.Location = new System.Drawing.Point(44, 519);
            this.txb2_Specification21.MaxLength = 12;
            this.txb2_Specification21.Multiline = true;
            this.txb2_Specification21.Name = "txb2_Specification21";
            this.txb2_Specification21.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification21.TabIndex = 525;
            // 
            // txb2_Specification22
            // 
            this.txb2_Specification22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification22.Location = new System.Drawing.Point(44, 545);
            this.txb2_Specification22.MaxLength = 12;
            this.txb2_Specification22.Multiline = true;
            this.txb2_Specification22.Name = "txb2_Specification22";
            this.txb2_Specification22.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification22.TabIndex = 524;
            // 
            // txb2_Specification23
            // 
            this.txb2_Specification23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification23.Location = new System.Drawing.Point(44, 571);
            this.txb2_Specification23.MaxLength = 12;
            this.txb2_Specification23.Multiline = true;
            this.txb2_Specification23.Name = "txb2_Specification23";
            this.txb2_Specification23.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification23.TabIndex = 523;
            // 
            // txb2_Specification24
            // 
            this.txb2_Specification24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification24.Location = new System.Drawing.Point(44, 597);
            this.txb2_Specification24.MaxLength = 12;
            this.txb2_Specification24.Multiline = true;
            this.txb2_Specification24.Name = "txb2_Specification24";
            this.txb2_Specification24.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification24.TabIndex = 522;
            // 
            // txb2_Specification25
            // 
            this.txb2_Specification25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification25.Location = new System.Drawing.Point(44, 623);
            this.txb2_Specification25.MaxLength = 12;
            this.txb2_Specification25.Multiline = true;
            this.txb2_Specification25.Name = "txb2_Specification25";
            this.txb2_Specification25.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification25.TabIndex = 521;
            // 
            // txb2_Specification26
            // 
            this.txb2_Specification26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification26.Location = new System.Drawing.Point(44, 646);
            this.txb2_Specification26.MaxLength = 12;
            this.txb2_Specification26.Multiline = true;
            this.txb2_Specification26.Name = "txb2_Specification26";
            this.txb2_Specification26.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification26.TabIndex = 520;
            // 
            // txb2_SendID26
            // 
            this.txb2_SendID26.Location = new System.Drawing.Point(380, 645);
            this.txb2_SendID26.Multiline = true;
            this.txb2_SendID26.Name = "txb2_SendID26";
            this.txb2_SendID26.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID26.TabIndex = 519;
            this.txb2_SendID26.Tag = "26";
            this.txb2_SendID26.Text = "01234567";
            // 
            // txb2_SendData26
            // 
            this.txb2_SendData26.Location = new System.Drawing.Point(473, 645);
            this.txb2_SendData26.Multiline = true;
            this.txb2_SendData26.Name = "txb2_SendData26";
            this.txb2_SendData26.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData26.TabIndex = 518;
            this.txb2_SendData26.Tag = "26";
            this.txb2_SendData26.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType26
            // 
            this.chb2_SendDataType26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType26.FormattingEnabled = true;
            this.chb2_SendDataType26.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType26.Location = new System.Drawing.Point(284, 644);
            this.chb2_SendDataType26.Name = "chb2_SendDataType26";
            this.chb2_SendDataType26.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType26.TabIndex = 517;
            this.chb2_SendDataType26.Tag = "26";
            this.chb2_SendDataType26.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType26
            // 
            this.cmb2_SendIDType26.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType26.FormattingEnabled = true;
            this.cmb2_SendIDType26.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType26.Location = new System.Drawing.Point(177, 644);
            this.cmb2_SendIDType26.Name = "cmb2_SendIDType26";
            this.cmb2_SendIDType26.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType26.TabIndex = 516;
            this.cmb2_SendIDType26.Tag = "26";
            // 
            // btn2_Send26
            // 
            this.btn2_Send26.Location = new System.Drawing.Point(623, 644);
            this.btn2_Send26.Name = "btn2_Send26";
            this.btn2_Send26.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send26.TabIndex = 515;
            this.btn2_Send26.Tag = "26";
            this.btn2_Send26.Text = "发送";
            this.btn2_Send26.UseVisualStyleBackColor = true;
            this.btn2_Send26.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(441, 648);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 514;
            this.label1.Text = "数据：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(348, 645);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 513;
            this.label2.Text = "帧ID：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(237, 648);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 512;
            this.label3.Text = "帧类型：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(128, 648);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 511;
            this.label4.Text = "帧格式：";
            // 
            // chb2_Send26
            // 
            this.chb2_Send26.AutoSize = true;
            this.chb2_Send26.Location = new System.Drawing.Point(9, 647);
            this.chb2_Send26.Name = "chb2_Send26";
            this.chb2_Send26.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send26.TabIndex = 510;
            this.chb2_Send26.Tag = "26";
            this.chb2_Send26.Text = "26:";
            this.chb2_Send26.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID25
            // 
            this.txb2_SendID25.Location = new System.Drawing.Point(380, 622);
            this.txb2_SendID25.Multiline = true;
            this.txb2_SendID25.Name = "txb2_SendID25";
            this.txb2_SendID25.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID25.TabIndex = 509;
            this.txb2_SendID25.Tag = "25";
            this.txb2_SendID25.Text = "01234567";
            // 
            // txb2_SendData25
            // 
            this.txb2_SendData25.Location = new System.Drawing.Point(473, 622);
            this.txb2_SendData25.Multiline = true;
            this.txb2_SendData25.Name = "txb2_SendData25";
            this.txb2_SendData25.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData25.TabIndex = 508;
            this.txb2_SendData25.Tag = "25";
            this.txb2_SendData25.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType25
            // 
            this.chb2_SendDataType25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType25.FormattingEnabled = true;
            this.chb2_SendDataType25.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType25.Location = new System.Drawing.Point(284, 621);
            this.chb2_SendDataType25.Name = "chb2_SendDataType25";
            this.chb2_SendDataType25.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType25.TabIndex = 507;
            this.chb2_SendDataType25.Tag = "25";
            this.chb2_SendDataType25.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType25
            // 
            this.cmb2_SendIDType25.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType25.FormattingEnabled = true;
            this.cmb2_SendIDType25.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType25.Location = new System.Drawing.Point(177, 621);
            this.cmb2_SendIDType25.Name = "cmb2_SendIDType25";
            this.cmb2_SendIDType25.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType25.TabIndex = 506;
            this.cmb2_SendIDType25.Tag = "25";
            // 
            // btn2_Send25
            // 
            this.btn2_Send25.Location = new System.Drawing.Point(623, 621);
            this.btn2_Send25.Name = "btn2_Send25";
            this.btn2_Send25.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send25.TabIndex = 505;
            this.btn2_Send25.Tag = "25";
            this.btn2_Send25.Text = "发送";
            this.btn2_Send25.UseVisualStyleBackColor = true;
            this.btn2_Send25.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(441, 625);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 504;
            this.label5.Text = "数据：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(348, 625);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 503;
            this.label6.Text = "帧ID：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(237, 625);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 502;
            this.label7.Text = "帧类型：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(128, 625);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 501;
            this.label8.Text = "帧格式：";
            // 
            // chb2_Send25
            // 
            this.chb2_Send25.AutoSize = true;
            this.chb2_Send25.Location = new System.Drawing.Point(9, 624);
            this.chb2_Send25.Name = "chb2_Send25";
            this.chb2_Send25.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send25.TabIndex = 500;
            this.chb2_Send25.Tag = "25";
            this.chb2_Send25.Text = "25:";
            this.chb2_Send25.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID24
            // 
            this.txb2_SendID24.Location = new System.Drawing.Point(380, 596);
            this.txb2_SendID24.Multiline = true;
            this.txb2_SendID24.Name = "txb2_SendID24";
            this.txb2_SendID24.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID24.TabIndex = 499;
            this.txb2_SendID24.Tag = "24";
            this.txb2_SendID24.Text = "01234567";
            // 
            // txb2_SendData24
            // 
            this.txb2_SendData24.Location = new System.Drawing.Point(473, 596);
            this.txb2_SendData24.Multiline = true;
            this.txb2_SendData24.Name = "txb2_SendData24";
            this.txb2_SendData24.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData24.TabIndex = 498;
            this.txb2_SendData24.Tag = "24";
            this.txb2_SendData24.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType24
            // 
            this.chb2_SendDataType24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType24.FormattingEnabled = true;
            this.chb2_SendDataType24.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType24.Location = new System.Drawing.Point(284, 595);
            this.chb2_SendDataType24.Name = "chb2_SendDataType24";
            this.chb2_SendDataType24.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType24.TabIndex = 497;
            this.chb2_SendDataType24.Tag = "24";
            this.chb2_SendDataType24.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType24
            // 
            this.cmb2_SendIDType24.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType24.FormattingEnabled = true;
            this.cmb2_SendIDType24.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType24.Location = new System.Drawing.Point(177, 595);
            this.cmb2_SendIDType24.Name = "cmb2_SendIDType24";
            this.cmb2_SendIDType24.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType24.TabIndex = 496;
            this.cmb2_SendIDType24.Tag = "24";
            // 
            // btn2_Send24
            // 
            this.btn2_Send24.Location = new System.Drawing.Point(623, 595);
            this.btn2_Send24.Name = "btn2_Send24";
            this.btn2_Send24.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send24.TabIndex = 495;
            this.btn2_Send24.Tag = "24";
            this.btn2_Send24.Text = "发送";
            this.btn2_Send24.UseVisualStyleBackColor = true;
            this.btn2_Send24.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(441, 599);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 494;
            this.label9.Text = "数据：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(348, 599);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 493;
            this.label10.Text = "帧ID：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(237, 599);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 492;
            this.label11.Text = "帧类型：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(128, 599);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 491;
            this.label12.Text = "帧格式：";
            // 
            // chb2_Send24
            // 
            this.chb2_Send24.AutoSize = true;
            this.chb2_Send24.Location = new System.Drawing.Point(9, 598);
            this.chb2_Send24.Name = "chb2_Send24";
            this.chb2_Send24.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send24.TabIndex = 490;
            this.chb2_Send24.Tag = "24";
            this.chb2_Send24.Text = "24:";
            this.chb2_Send24.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID23
            // 
            this.txb2_SendID23.Location = new System.Drawing.Point(380, 570);
            this.txb2_SendID23.Multiline = true;
            this.txb2_SendID23.Name = "txb2_SendID23";
            this.txb2_SendID23.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID23.TabIndex = 489;
            this.txb2_SendID23.Tag = "23";
            this.txb2_SendID23.Text = "01234567";
            // 
            // txb2_SendData23
            // 
            this.txb2_SendData23.Location = new System.Drawing.Point(473, 570);
            this.txb2_SendData23.Multiline = true;
            this.txb2_SendData23.Name = "txb2_SendData23";
            this.txb2_SendData23.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData23.TabIndex = 488;
            this.txb2_SendData23.Tag = "23";
            this.txb2_SendData23.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType23
            // 
            this.chb2_SendDataType23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType23.FormattingEnabled = true;
            this.chb2_SendDataType23.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType23.Location = new System.Drawing.Point(284, 569);
            this.chb2_SendDataType23.Name = "chb2_SendDataType23";
            this.chb2_SendDataType23.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType23.TabIndex = 487;
            this.chb2_SendDataType23.Tag = "23";
            this.chb2_SendDataType23.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType23
            // 
            this.cmb2_SendIDType23.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType23.FormattingEnabled = true;
            this.cmb2_SendIDType23.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType23.Location = new System.Drawing.Point(177, 569);
            this.cmb2_SendIDType23.Name = "cmb2_SendIDType23";
            this.cmb2_SendIDType23.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType23.TabIndex = 486;
            this.cmb2_SendIDType23.Tag = "23";
            // 
            // btn2_Send23
            // 
            this.btn2_Send23.Location = new System.Drawing.Point(623, 569);
            this.btn2_Send23.Name = "btn2_Send23";
            this.btn2_Send23.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send23.TabIndex = 485;
            this.btn2_Send23.Tag = "23";
            this.btn2_Send23.Text = "发送";
            this.btn2_Send23.UseVisualStyleBackColor = true;
            this.btn2_Send23.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(441, 573);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 484;
            this.label13.Text = "数据：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(348, 573);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 483;
            this.label14.Text = "帧ID：";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(237, 573);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 12);
            this.label15.TabIndex = 482;
            this.label15.Text = "帧类型：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(128, 573);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 481;
            this.label16.Text = "帧格式：";
            // 
            // chb2_Send23
            // 
            this.chb2_Send23.AutoSize = true;
            this.chb2_Send23.Location = new System.Drawing.Point(9, 572);
            this.chb2_Send23.Name = "chb2_Send23";
            this.chb2_Send23.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send23.TabIndex = 480;
            this.chb2_Send23.Tag = "23";
            this.chb2_Send23.Text = "23:";
            this.chb2_Send23.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID22
            // 
            this.txb2_SendID22.Location = new System.Drawing.Point(380, 544);
            this.txb2_SendID22.Multiline = true;
            this.txb2_SendID22.Name = "txb2_SendID22";
            this.txb2_SendID22.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID22.TabIndex = 479;
            this.txb2_SendID22.Tag = "22";
            this.txb2_SendID22.Text = "01234567";
            // 
            // txb2_SendData22
            // 
            this.txb2_SendData22.Location = new System.Drawing.Point(473, 544);
            this.txb2_SendData22.Multiline = true;
            this.txb2_SendData22.Name = "txb2_SendData22";
            this.txb2_SendData22.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData22.TabIndex = 478;
            this.txb2_SendData22.Tag = "22";
            this.txb2_SendData22.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType22
            // 
            this.chb2_SendDataType22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType22.FormattingEnabled = true;
            this.chb2_SendDataType22.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType22.Location = new System.Drawing.Point(284, 543);
            this.chb2_SendDataType22.Name = "chb2_SendDataType22";
            this.chb2_SendDataType22.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType22.TabIndex = 477;
            this.chb2_SendDataType22.Tag = "22";
            this.chb2_SendDataType22.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType22
            // 
            this.cmb2_SendIDType22.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType22.FormattingEnabled = true;
            this.cmb2_SendIDType22.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType22.Location = new System.Drawing.Point(177, 543);
            this.cmb2_SendIDType22.Name = "cmb2_SendIDType22";
            this.cmb2_SendIDType22.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType22.TabIndex = 476;
            this.cmb2_SendIDType22.Tag = "22";
            // 
            // btn2_Send22
            // 
            this.btn2_Send22.Location = new System.Drawing.Point(623, 543);
            this.btn2_Send22.Name = "btn2_Send22";
            this.btn2_Send22.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send22.TabIndex = 475;
            this.btn2_Send22.Tag = "22";
            this.btn2_Send22.Text = "发送";
            this.btn2_Send22.UseVisualStyleBackColor = true;
            this.btn2_Send22.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(441, 547);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 12);
            this.label17.TabIndex = 474;
            this.label17.Text = "数据：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(348, 547);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 473;
            this.label18.Text = "帧ID：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(237, 547);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 12);
            this.label19.TabIndex = 472;
            this.label19.Text = "帧类型：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(128, 547);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 12);
            this.label20.TabIndex = 471;
            this.label20.Text = "帧格式：";
            // 
            // chb2_Send22
            // 
            this.chb2_Send22.AutoSize = true;
            this.chb2_Send22.Location = new System.Drawing.Point(9, 546);
            this.chb2_Send22.Name = "chb2_Send22";
            this.chb2_Send22.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send22.TabIndex = 470;
            this.chb2_Send22.Tag = "22";
            this.chb2_Send22.Text = "22:";
            this.chb2_Send22.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID21
            // 
            this.txb2_SendID21.Location = new System.Drawing.Point(380, 518);
            this.txb2_SendID21.Multiline = true;
            this.txb2_SendID21.Name = "txb2_SendID21";
            this.txb2_SendID21.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID21.TabIndex = 469;
            this.txb2_SendID21.Tag = "21";
            this.txb2_SendID21.Text = "01234567";
            // 
            // txb2_SendData21
            // 
            this.txb2_SendData21.Location = new System.Drawing.Point(473, 518);
            this.txb2_SendData21.Multiline = true;
            this.txb2_SendData21.Name = "txb2_SendData21";
            this.txb2_SendData21.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData21.TabIndex = 468;
            this.txb2_SendData21.Tag = "21";
            this.txb2_SendData21.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType21
            // 
            this.chb2_SendDataType21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType21.FormattingEnabled = true;
            this.chb2_SendDataType21.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType21.Location = new System.Drawing.Point(284, 517);
            this.chb2_SendDataType21.Name = "chb2_SendDataType21";
            this.chb2_SendDataType21.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType21.TabIndex = 467;
            this.chb2_SendDataType21.Tag = "21";
            this.chb2_SendDataType21.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType21
            // 
            this.cmb2_SendIDType21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType21.FormattingEnabled = true;
            this.cmb2_SendIDType21.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType21.Location = new System.Drawing.Point(177, 517);
            this.cmb2_SendIDType21.Name = "cmb2_SendIDType21";
            this.cmb2_SendIDType21.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType21.TabIndex = 466;
            this.cmb2_SendIDType21.Tag = "21";
            // 
            // btn2_Send21
            // 
            this.btn2_Send21.Location = new System.Drawing.Point(623, 517);
            this.btn2_Send21.Name = "btn2_Send21";
            this.btn2_Send21.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send21.TabIndex = 465;
            this.btn2_Send21.Tag = "21";
            this.btn2_Send21.Text = "发送";
            this.btn2_Send21.UseVisualStyleBackColor = true;
            this.btn2_Send21.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(441, 521);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 464;
            this.label21.Text = "数据：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(348, 521);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 463;
            this.label22.Text = "帧ID：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(237, 521);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 462;
            this.label23.Text = "帧类型：";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(128, 521);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 461;
            this.label24.Text = "帧格式：";
            // 
            // chb2_Send21
            // 
            this.chb2_Send21.AutoSize = true;
            this.chb2_Send21.Location = new System.Drawing.Point(9, 520);
            this.chb2_Send21.Name = "chb2_Send21";
            this.chb2_Send21.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send21.TabIndex = 460;
            this.chb2_Send21.Tag = "21";
            this.chb2_Send21.Text = "21:";
            this.chb2_Send21.UseVisualStyleBackColor = true;
            // 
            // txb2_Specification1
            // 
            this.txb2_Specification1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification1.Location = new System.Drawing.Point(44, 2);
            this.txb2_Specification1.MaxLength = 12;
            this.txb2_Specification1.Multiline = true;
            this.txb2_Specification1.Name = "txb2_Specification1";
            this.txb2_Specification1.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification1.TabIndex = 459;
            this.txb2_Specification1.Text = "你是我小苹果";
            // 
            // txb2_Specification2
            // 
            this.txb2_Specification2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification2.Location = new System.Drawing.Point(44, 28);
            this.txb2_Specification2.MaxLength = 12;
            this.txb2_Specification2.Multiline = true;
            this.txb2_Specification2.Name = "txb2_Specification2";
            this.txb2_Specification2.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification2.TabIndex = 458;
            // 
            // txb2_Specification3
            // 
            this.txb2_Specification3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification3.Location = new System.Drawing.Point(44, 54);
            this.txb2_Specification3.MaxLength = 12;
            this.txb2_Specification3.Multiline = true;
            this.txb2_Specification3.Name = "txb2_Specification3";
            this.txb2_Specification3.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification3.TabIndex = 457;
            // 
            // txb2_Specification4
            // 
            this.txb2_Specification4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification4.Location = new System.Drawing.Point(44, 80);
            this.txb2_Specification4.MaxLength = 12;
            this.txb2_Specification4.Multiline = true;
            this.txb2_Specification4.Name = "txb2_Specification4";
            this.txb2_Specification4.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification4.TabIndex = 456;
            // 
            // txb2_Specification5
            // 
            this.txb2_Specification5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification5.Location = new System.Drawing.Point(44, 106);
            this.txb2_Specification5.MaxLength = 12;
            this.txb2_Specification5.Multiline = true;
            this.txb2_Specification5.Name = "txb2_Specification5";
            this.txb2_Specification5.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification5.TabIndex = 455;
            // 
            // txb2_Specification6
            // 
            this.txb2_Specification6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification6.Location = new System.Drawing.Point(44, 132);
            this.txb2_Specification6.MaxLength = 12;
            this.txb2_Specification6.Multiline = true;
            this.txb2_Specification6.Name = "txb2_Specification6";
            this.txb2_Specification6.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification6.TabIndex = 454;
            // 
            // txb2_Specification7
            // 
            this.txb2_Specification7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification7.Location = new System.Drawing.Point(44, 158);
            this.txb2_Specification7.MaxLength = 12;
            this.txb2_Specification7.Multiline = true;
            this.txb2_Specification7.Name = "txb2_Specification7";
            this.txb2_Specification7.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification7.TabIndex = 453;
            // 
            // txb2_Specification8
            // 
            this.txb2_Specification8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification8.Location = new System.Drawing.Point(44, 184);
            this.txb2_Specification8.MaxLength = 12;
            this.txb2_Specification8.Multiline = true;
            this.txb2_Specification8.Name = "txb2_Specification8";
            this.txb2_Specification8.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification8.TabIndex = 452;
            // 
            // txb2_Specification9
            // 
            this.txb2_Specification9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification9.Location = new System.Drawing.Point(44, 210);
            this.txb2_Specification9.MaxLength = 12;
            this.txb2_Specification9.Multiline = true;
            this.txb2_Specification9.Name = "txb2_Specification9";
            this.txb2_Specification9.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification9.TabIndex = 451;
            // 
            // txb2_Specification10
            // 
            this.txb2_Specification10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification10.Location = new System.Drawing.Point(44, 236);
            this.txb2_Specification10.MaxLength = 12;
            this.txb2_Specification10.Multiline = true;
            this.txb2_Specification10.Name = "txb2_Specification10";
            this.txb2_Specification10.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification10.TabIndex = 450;
            // 
            // txb2_Specification11
            // 
            this.txb2_Specification11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification11.Location = new System.Drawing.Point(44, 262);
            this.txb2_Specification11.MaxLength = 12;
            this.txb2_Specification11.Multiline = true;
            this.txb2_Specification11.Name = "txb2_Specification11";
            this.txb2_Specification11.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification11.TabIndex = 449;
            // 
            // txb2_Specification12
            // 
            this.txb2_Specification12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification12.Location = new System.Drawing.Point(44, 288);
            this.txb2_Specification12.MaxLength = 12;
            this.txb2_Specification12.Multiline = true;
            this.txb2_Specification12.Name = "txb2_Specification12";
            this.txb2_Specification12.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification12.TabIndex = 448;
            // 
            // txb2_Specification13
            // 
            this.txb2_Specification13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification13.Location = new System.Drawing.Point(44, 314);
            this.txb2_Specification13.MaxLength = 12;
            this.txb2_Specification13.Multiline = true;
            this.txb2_Specification13.Name = "txb2_Specification13";
            this.txb2_Specification13.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification13.TabIndex = 447;
            // 
            // txb2_Specification14
            // 
            this.txb2_Specification14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification14.Location = new System.Drawing.Point(44, 340);
            this.txb2_Specification14.MaxLength = 12;
            this.txb2_Specification14.Multiline = true;
            this.txb2_Specification14.Name = "txb2_Specification14";
            this.txb2_Specification14.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification14.TabIndex = 446;
            // 
            // txb2_Specification15
            // 
            this.txb2_Specification15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification15.Location = new System.Drawing.Point(44, 366);
            this.txb2_Specification15.MaxLength = 12;
            this.txb2_Specification15.Multiline = true;
            this.txb2_Specification15.Name = "txb2_Specification15";
            this.txb2_Specification15.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification15.TabIndex = 445;
            // 
            // txb2_Specification16
            // 
            this.txb2_Specification16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification16.Location = new System.Drawing.Point(44, 392);
            this.txb2_Specification16.MaxLength = 12;
            this.txb2_Specification16.Multiline = true;
            this.txb2_Specification16.Name = "txb2_Specification16";
            this.txb2_Specification16.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification16.TabIndex = 444;
            // 
            // txb2_Specification17
            // 
            this.txb2_Specification17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification17.Location = new System.Drawing.Point(44, 418);
            this.txb2_Specification17.MaxLength = 12;
            this.txb2_Specification17.Multiline = true;
            this.txb2_Specification17.Name = "txb2_Specification17";
            this.txb2_Specification17.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification17.TabIndex = 443;
            // 
            // txb2_Specification18
            // 
            this.txb2_Specification18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification18.Location = new System.Drawing.Point(44, 444);
            this.txb2_Specification18.MaxLength = 12;
            this.txb2_Specification18.Multiline = true;
            this.txb2_Specification18.Name = "txb2_Specification18";
            this.txb2_Specification18.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification18.TabIndex = 442;
            // 
            // txb2_Specification19
            // 
            this.txb2_Specification19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification19.Location = new System.Drawing.Point(44, 470);
            this.txb2_Specification19.MaxLength = 12;
            this.txb2_Specification19.Multiline = true;
            this.txb2_Specification19.Name = "txb2_Specification19";
            this.txb2_Specification19.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification19.TabIndex = 441;
            // 
            // txb2_Specification20
            // 
            this.txb2_Specification20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb2_Specification20.Location = new System.Drawing.Point(44, 496);
            this.txb2_Specification20.MaxLength = 12;
            this.txb2_Specification20.Multiline = true;
            this.txb2_Specification20.Name = "txb2_Specification20";
            this.txb2_Specification20.Size = new System.Drawing.Size(78, 18);
            this.txb2_Specification20.TabIndex = 440;
            // 
            // txb2_SendID20
            // 
            this.txb2_SendID20.Location = new System.Drawing.Point(380, 495);
            this.txb2_SendID20.Multiline = true;
            this.txb2_SendID20.Name = "txb2_SendID20";
            this.txb2_SendID20.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID20.TabIndex = 439;
            this.txb2_SendID20.Tag = "20";
            this.txb2_SendID20.Text = "01234567";
            // 
            // txb2_SendData20
            // 
            this.txb2_SendData20.Location = new System.Drawing.Point(473, 495);
            this.txb2_SendData20.Multiline = true;
            this.txb2_SendData20.Name = "txb2_SendData20";
            this.txb2_SendData20.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData20.TabIndex = 438;
            this.txb2_SendData20.Tag = "20";
            this.txb2_SendData20.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType20
            // 
            this.chb2_SendDataType20.BackColor = System.Drawing.SystemColors.Window;
            this.chb2_SendDataType20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType20.FormattingEnabled = true;
            this.chb2_SendDataType20.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType20.Location = new System.Drawing.Point(284, 494);
            this.chb2_SendDataType20.Name = "chb2_SendDataType20";
            this.chb2_SendDataType20.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType20.TabIndex = 437;
            this.chb2_SendDataType20.Tag = "20";
            this.chb2_SendDataType20.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType20
            // 
            this.cmb2_SendIDType20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType20.FormattingEnabled = true;
            this.cmb2_SendIDType20.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType20.Location = new System.Drawing.Point(177, 494);
            this.cmb2_SendIDType20.Name = "cmb2_SendIDType20";
            this.cmb2_SendIDType20.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType20.TabIndex = 436;
            this.cmb2_SendIDType20.Tag = "20";
            // 
            // btn2_Send20
            // 
            this.btn2_Send20.Location = new System.Drawing.Point(623, 494);
            this.btn2_Send20.Name = "btn2_Send20";
            this.btn2_Send20.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send20.TabIndex = 435;
            this.btn2_Send20.Tag = "20";
            this.btn2_Send20.Text = "发送";
            this.btn2_Send20.UseVisualStyleBackColor = true;
            this.btn2_Send20.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(441, 498);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(41, 12);
            this.label153.TabIndex = 434;
            this.label153.Text = "数据：";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(348, 495);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(41, 12);
            this.label154.TabIndex = 433;
            this.label154.Text = "帧ID：";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(237, 498);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(53, 12);
            this.label155.TabIndex = 432;
            this.label155.Text = "帧类型：";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(128, 498);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(53, 12);
            this.label156.TabIndex = 431;
            this.label156.Text = "帧格式：";
            // 
            // chb2_Send20
            // 
            this.chb2_Send20.AutoSize = true;
            this.chb2_Send20.Location = new System.Drawing.Point(9, 497);
            this.chb2_Send20.Name = "chb2_Send20";
            this.chb2_Send20.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send20.TabIndex = 430;
            this.chb2_Send20.Tag = "20";
            this.chb2_Send20.Text = "20:";
            this.chb2_Send20.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID19
            // 
            this.txb2_SendID19.Location = new System.Drawing.Point(380, 469);
            this.txb2_SendID19.Multiline = true;
            this.txb2_SendID19.Name = "txb2_SendID19";
            this.txb2_SendID19.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID19.TabIndex = 429;
            this.txb2_SendID19.Tag = "19";
            this.txb2_SendID19.Text = "01234567";
            // 
            // txb2_SendData19
            // 
            this.txb2_SendData19.Location = new System.Drawing.Point(473, 469);
            this.txb2_SendData19.Multiline = true;
            this.txb2_SendData19.Name = "txb2_SendData19";
            this.txb2_SendData19.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData19.TabIndex = 428;
            this.txb2_SendData19.Tag = "19";
            this.txb2_SendData19.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType19
            // 
            this.chb2_SendDataType19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType19.FormattingEnabled = true;
            this.chb2_SendDataType19.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType19.Location = new System.Drawing.Point(284, 468);
            this.chb2_SendDataType19.Name = "chb2_SendDataType19";
            this.chb2_SendDataType19.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType19.TabIndex = 427;
            this.chb2_SendDataType19.Tag = "19";
            this.chb2_SendDataType19.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType19
            // 
            this.cmb2_SendIDType19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType19.FormattingEnabled = true;
            this.cmb2_SendIDType19.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType19.Location = new System.Drawing.Point(177, 468);
            this.cmb2_SendIDType19.Name = "cmb2_SendIDType19";
            this.cmb2_SendIDType19.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType19.TabIndex = 426;
            this.cmb2_SendIDType19.Tag = "19";
            // 
            // btn2_Send19
            // 
            this.btn2_Send19.Location = new System.Drawing.Point(623, 468);
            this.btn2_Send19.Name = "btn2_Send19";
            this.btn2_Send19.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send19.TabIndex = 425;
            this.btn2_Send19.Tag = "19";
            this.btn2_Send19.Text = "发送";
            this.btn2_Send19.UseVisualStyleBackColor = true;
            this.btn2_Send19.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(441, 472);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(41, 12);
            this.label157.TabIndex = 424;
            this.label157.Text = "数据：";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(348, 472);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(41, 12);
            this.label158.TabIndex = 423;
            this.label158.Text = "帧ID：";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(237, 472);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(53, 12);
            this.label159.TabIndex = 422;
            this.label159.Text = "帧类型：";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(128, 472);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(53, 12);
            this.label160.TabIndex = 421;
            this.label160.Text = "帧格式：";
            // 
            // chb2_Send19
            // 
            this.chb2_Send19.AutoSize = true;
            this.chb2_Send19.Location = new System.Drawing.Point(9, 471);
            this.chb2_Send19.Name = "chb2_Send19";
            this.chb2_Send19.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send19.TabIndex = 420;
            this.chb2_Send19.Tag = "19";
            this.chb2_Send19.Text = "19:";
            this.chb2_Send19.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID18
            // 
            this.txb2_SendID18.Location = new System.Drawing.Point(380, 443);
            this.txb2_SendID18.Multiline = true;
            this.txb2_SendID18.Name = "txb2_SendID18";
            this.txb2_SendID18.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID18.TabIndex = 419;
            this.txb2_SendID18.Tag = "18";
            this.txb2_SendID18.Text = "01234567";
            // 
            // txb2_SendData18
            // 
            this.txb2_SendData18.Location = new System.Drawing.Point(473, 443);
            this.txb2_SendData18.Multiline = true;
            this.txb2_SendData18.Name = "txb2_SendData18";
            this.txb2_SendData18.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData18.TabIndex = 418;
            this.txb2_SendData18.Tag = "18";
            this.txb2_SendData18.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType18
            // 
            this.chb2_SendDataType18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType18.FormattingEnabled = true;
            this.chb2_SendDataType18.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType18.Location = new System.Drawing.Point(284, 442);
            this.chb2_SendDataType18.Name = "chb2_SendDataType18";
            this.chb2_SendDataType18.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType18.TabIndex = 417;
            this.chb2_SendDataType18.Tag = "18";
            this.chb2_SendDataType18.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType18
            // 
            this.cmb2_SendIDType18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType18.FormattingEnabled = true;
            this.cmb2_SendIDType18.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType18.Location = new System.Drawing.Point(177, 442);
            this.cmb2_SendIDType18.Name = "cmb2_SendIDType18";
            this.cmb2_SendIDType18.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType18.TabIndex = 416;
            this.cmb2_SendIDType18.Tag = "18";
            // 
            // btn2_Send18
            // 
            this.btn2_Send18.Location = new System.Drawing.Point(623, 442);
            this.btn2_Send18.Name = "btn2_Send18";
            this.btn2_Send18.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send18.TabIndex = 415;
            this.btn2_Send18.Tag = "18";
            this.btn2_Send18.Text = "发送";
            this.btn2_Send18.UseVisualStyleBackColor = true;
            this.btn2_Send18.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(441, 446);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(41, 12);
            this.label145.TabIndex = 414;
            this.label145.Text = "数据：";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(348, 446);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(41, 12);
            this.label146.TabIndex = 413;
            this.label146.Text = "帧ID：";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(237, 446);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(53, 12);
            this.label147.TabIndex = 412;
            this.label147.Text = "帧类型：";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(128, 446);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(53, 12);
            this.label148.TabIndex = 411;
            this.label148.Text = "帧格式：";
            // 
            // chb2_Send18
            // 
            this.chb2_Send18.AutoSize = true;
            this.chb2_Send18.Location = new System.Drawing.Point(9, 445);
            this.chb2_Send18.Name = "chb2_Send18";
            this.chb2_Send18.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send18.TabIndex = 410;
            this.chb2_Send18.Tag = "18";
            this.chb2_Send18.Text = "18:";
            this.chb2_Send18.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID17
            // 
            this.txb2_SendID17.Location = new System.Drawing.Point(380, 417);
            this.txb2_SendID17.Multiline = true;
            this.txb2_SendID17.Name = "txb2_SendID17";
            this.txb2_SendID17.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID17.TabIndex = 409;
            this.txb2_SendID17.Tag = "17";
            this.txb2_SendID17.Text = "01234567";
            // 
            // txb2_SendData17
            // 
            this.txb2_SendData17.Location = new System.Drawing.Point(473, 417);
            this.txb2_SendData17.Multiline = true;
            this.txb2_SendData17.Name = "txb2_SendData17";
            this.txb2_SendData17.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData17.TabIndex = 408;
            this.txb2_SendData17.Tag = "17";
            this.txb2_SendData17.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType17
            // 
            this.chb2_SendDataType17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType17.FormattingEnabled = true;
            this.chb2_SendDataType17.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType17.Location = new System.Drawing.Point(284, 416);
            this.chb2_SendDataType17.Name = "chb2_SendDataType17";
            this.chb2_SendDataType17.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType17.TabIndex = 407;
            this.chb2_SendDataType17.Tag = "17";
            this.chb2_SendDataType17.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType17
            // 
            this.cmb2_SendIDType17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType17.FormattingEnabled = true;
            this.cmb2_SendIDType17.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType17.Location = new System.Drawing.Point(177, 416);
            this.cmb2_SendIDType17.Name = "cmb2_SendIDType17";
            this.cmb2_SendIDType17.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType17.TabIndex = 406;
            this.cmb2_SendIDType17.Tag = "17";
            // 
            // btn2_Send17
            // 
            this.btn2_Send17.Location = new System.Drawing.Point(623, 416);
            this.btn2_Send17.Name = "btn2_Send17";
            this.btn2_Send17.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send17.TabIndex = 405;
            this.btn2_Send17.Tag = "17";
            this.btn2_Send17.Text = "发送";
            this.btn2_Send17.UseVisualStyleBackColor = true;
            this.btn2_Send17.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(441, 420);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(41, 12);
            this.label149.TabIndex = 404;
            this.label149.Text = "数据：";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(348, 420);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(41, 12);
            this.label150.TabIndex = 403;
            this.label150.Text = "帧ID：";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(237, 420);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(53, 12);
            this.label151.TabIndex = 402;
            this.label151.Text = "帧类型：";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(128, 420);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(53, 12);
            this.label152.TabIndex = 401;
            this.label152.Text = "帧格式：";
            // 
            // chb2_Send17
            // 
            this.chb2_Send17.AutoSize = true;
            this.chb2_Send17.Location = new System.Drawing.Point(9, 419);
            this.chb2_Send17.Name = "chb2_Send17";
            this.chb2_Send17.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send17.TabIndex = 400;
            this.chb2_Send17.Tag = "17";
            this.chb2_Send17.Text = "17:";
            this.chb2_Send17.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID16
            // 
            this.txb2_SendID16.Location = new System.Drawing.Point(380, 391);
            this.txb2_SendID16.Multiline = true;
            this.txb2_SendID16.Name = "txb2_SendID16";
            this.txb2_SendID16.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID16.TabIndex = 399;
            this.txb2_SendID16.Tag = "16";
            this.txb2_SendID16.Text = "01234567";
            // 
            // txb2_SendData16
            // 
            this.txb2_SendData16.Location = new System.Drawing.Point(473, 391);
            this.txb2_SendData16.Multiline = true;
            this.txb2_SendData16.Name = "txb2_SendData16";
            this.txb2_SendData16.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData16.TabIndex = 398;
            this.txb2_SendData16.Tag = "16";
            this.txb2_SendData16.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType16
            // 
            this.chb2_SendDataType16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType16.FormattingEnabled = true;
            this.chb2_SendDataType16.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType16.Location = new System.Drawing.Point(284, 390);
            this.chb2_SendDataType16.Name = "chb2_SendDataType16";
            this.chb2_SendDataType16.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType16.TabIndex = 397;
            this.chb2_SendDataType16.Tag = "16";
            this.chb2_SendDataType16.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType16
            // 
            this.cmb2_SendIDType16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType16.FormattingEnabled = true;
            this.cmb2_SendIDType16.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType16.Location = new System.Drawing.Point(177, 390);
            this.cmb2_SendIDType16.Name = "cmb2_SendIDType16";
            this.cmb2_SendIDType16.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType16.TabIndex = 396;
            this.cmb2_SendIDType16.Tag = "16";
            // 
            // btn2_Send16
            // 
            this.btn2_Send16.Location = new System.Drawing.Point(623, 390);
            this.btn2_Send16.Name = "btn2_Send16";
            this.btn2_Send16.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send16.TabIndex = 395;
            this.btn2_Send16.Tag = "16";
            this.btn2_Send16.Text = "发送";
            this.btn2_Send16.UseVisualStyleBackColor = true;
            this.btn2_Send16.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(441, 394);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(41, 12);
            this.label129.TabIndex = 394;
            this.label129.Text = "数据：";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(348, 394);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(41, 12);
            this.label130.TabIndex = 393;
            this.label130.Text = "帧ID：";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(237, 394);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(53, 12);
            this.label131.TabIndex = 392;
            this.label131.Text = "帧类型：";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(128, 394);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(53, 12);
            this.label132.TabIndex = 391;
            this.label132.Text = "帧格式：";
            // 
            // chb2_Send16
            // 
            this.chb2_Send16.AutoSize = true;
            this.chb2_Send16.Location = new System.Drawing.Point(9, 393);
            this.chb2_Send16.Name = "chb2_Send16";
            this.chb2_Send16.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send16.TabIndex = 390;
            this.chb2_Send16.Tag = "16";
            this.chb2_Send16.Text = "16:";
            this.chb2_Send16.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID15
            // 
            this.txb2_SendID15.Location = new System.Drawing.Point(380, 365);
            this.txb2_SendID15.Multiline = true;
            this.txb2_SendID15.Name = "txb2_SendID15";
            this.txb2_SendID15.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID15.TabIndex = 389;
            this.txb2_SendID15.Tag = "15";
            this.txb2_SendID15.Text = "01234567";
            // 
            // txb2_SendData15
            // 
            this.txb2_SendData15.Location = new System.Drawing.Point(473, 365);
            this.txb2_SendData15.Multiline = true;
            this.txb2_SendData15.Name = "txb2_SendData15";
            this.txb2_SendData15.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData15.TabIndex = 388;
            this.txb2_SendData15.Tag = "15";
            this.txb2_SendData15.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType15
            // 
            this.chb2_SendDataType15.BackColor = System.Drawing.SystemColors.Window;
            this.chb2_SendDataType15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType15.FormattingEnabled = true;
            this.chb2_SendDataType15.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType15.Location = new System.Drawing.Point(284, 364);
            this.chb2_SendDataType15.Name = "chb2_SendDataType15";
            this.chb2_SendDataType15.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType15.TabIndex = 387;
            this.chb2_SendDataType15.Tag = "15";
            this.chb2_SendDataType15.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType15
            // 
            this.cmb2_SendIDType15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType15.FormattingEnabled = true;
            this.cmb2_SendIDType15.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType15.Location = new System.Drawing.Point(177, 364);
            this.cmb2_SendIDType15.Name = "cmb2_SendIDType15";
            this.cmb2_SendIDType15.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType15.TabIndex = 386;
            this.cmb2_SendIDType15.Tag = "15";
            // 
            // btn2_Send15
            // 
            this.btn2_Send15.Location = new System.Drawing.Point(623, 364);
            this.btn2_Send15.Name = "btn2_Send15";
            this.btn2_Send15.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send15.TabIndex = 385;
            this.btn2_Send15.Tag = "15";
            this.btn2_Send15.Text = "发送";
            this.btn2_Send15.UseVisualStyleBackColor = true;
            this.btn2_Send15.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(441, 368);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(41, 12);
            this.label133.TabIndex = 384;
            this.label133.Text = "数据：";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(348, 368);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(41, 12);
            this.label134.TabIndex = 383;
            this.label134.Text = "帧ID：";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(237, 368);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(53, 12);
            this.label135.TabIndex = 382;
            this.label135.Text = "帧类型：";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(128, 368);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(53, 12);
            this.label136.TabIndex = 381;
            this.label136.Text = "帧格式：";
            // 
            // chb2_Send15
            // 
            this.chb2_Send15.AutoSize = true;
            this.chb2_Send15.Location = new System.Drawing.Point(9, 367);
            this.chb2_Send15.Name = "chb2_Send15";
            this.chb2_Send15.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send15.TabIndex = 380;
            this.chb2_Send15.Tag = "15";
            this.chb2_Send15.Text = "15:";
            this.chb2_Send15.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID14
            // 
            this.txb2_SendID14.Location = new System.Drawing.Point(380, 339);
            this.txb2_SendID14.Multiline = true;
            this.txb2_SendID14.Name = "txb2_SendID14";
            this.txb2_SendID14.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID14.TabIndex = 379;
            this.txb2_SendID14.Tag = "14";
            this.txb2_SendID14.Text = "01234567";
            // 
            // txb2_SendData14
            // 
            this.txb2_SendData14.Location = new System.Drawing.Point(473, 339);
            this.txb2_SendData14.Multiline = true;
            this.txb2_SendData14.Name = "txb2_SendData14";
            this.txb2_SendData14.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData14.TabIndex = 378;
            this.txb2_SendData14.Tag = "14";
            this.txb2_SendData14.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType14
            // 
            this.chb2_SendDataType14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType14.FormattingEnabled = true;
            this.chb2_SendDataType14.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType14.Location = new System.Drawing.Point(284, 338);
            this.chb2_SendDataType14.Name = "chb2_SendDataType14";
            this.chb2_SendDataType14.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType14.TabIndex = 377;
            this.chb2_SendDataType14.Tag = "14";
            this.chb2_SendDataType14.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType14
            // 
            this.cmb2_SendIDType14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType14.FormattingEnabled = true;
            this.cmb2_SendIDType14.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType14.Location = new System.Drawing.Point(177, 338);
            this.cmb2_SendIDType14.Name = "cmb2_SendIDType14";
            this.cmb2_SendIDType14.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType14.TabIndex = 376;
            this.cmb2_SendIDType14.Tag = "14";
            // 
            // btn2_Send14
            // 
            this.btn2_Send14.Location = new System.Drawing.Point(623, 338);
            this.btn2_Send14.Name = "btn2_Send14";
            this.btn2_Send14.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send14.TabIndex = 375;
            this.btn2_Send14.Tag = "14";
            this.btn2_Send14.Text = "发送";
            this.btn2_Send14.UseVisualStyleBackColor = true;
            this.btn2_Send14.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(441, 342);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(41, 12);
            this.label137.TabIndex = 374;
            this.label137.Text = "数据：";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(348, 342);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(41, 12);
            this.label138.TabIndex = 373;
            this.label138.Text = "帧ID：";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(237, 342);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(53, 12);
            this.label139.TabIndex = 372;
            this.label139.Text = "帧类型：";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(128, 342);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(53, 12);
            this.label140.TabIndex = 371;
            this.label140.Text = "帧格式：";
            // 
            // chb2_Send14
            // 
            this.chb2_Send14.AutoSize = true;
            this.chb2_Send14.Location = new System.Drawing.Point(9, 341);
            this.chb2_Send14.Name = "chb2_Send14";
            this.chb2_Send14.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send14.TabIndex = 370;
            this.chb2_Send14.Tag = "14";
            this.chb2_Send14.Text = "14:";
            this.chb2_Send14.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID13
            // 
            this.txb2_SendID13.Location = new System.Drawing.Point(380, 313);
            this.txb2_SendID13.Multiline = true;
            this.txb2_SendID13.Name = "txb2_SendID13";
            this.txb2_SendID13.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID13.TabIndex = 369;
            this.txb2_SendID13.Tag = "13";
            this.txb2_SendID13.Text = "01234567";
            // 
            // txb2_SendData13
            // 
            this.txb2_SendData13.Location = new System.Drawing.Point(473, 313);
            this.txb2_SendData13.Multiline = true;
            this.txb2_SendData13.Name = "txb2_SendData13";
            this.txb2_SendData13.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData13.TabIndex = 368;
            this.txb2_SendData13.Tag = "13";
            this.txb2_SendData13.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType13
            // 
            this.chb2_SendDataType13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType13.FormattingEnabled = true;
            this.chb2_SendDataType13.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType13.Location = new System.Drawing.Point(284, 312);
            this.chb2_SendDataType13.Name = "chb2_SendDataType13";
            this.chb2_SendDataType13.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType13.TabIndex = 367;
            this.chb2_SendDataType13.Tag = "13";
            this.chb2_SendDataType13.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType13
            // 
            this.cmb2_SendIDType13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType13.FormattingEnabled = true;
            this.cmb2_SendIDType13.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType13.Location = new System.Drawing.Point(177, 312);
            this.cmb2_SendIDType13.Name = "cmb2_SendIDType13";
            this.cmb2_SendIDType13.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType13.TabIndex = 366;
            this.cmb2_SendIDType13.Tag = "13";
            // 
            // btn2_Send13
            // 
            this.btn2_Send13.Location = new System.Drawing.Point(623, 312);
            this.btn2_Send13.Name = "btn2_Send13";
            this.btn2_Send13.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send13.TabIndex = 365;
            this.btn2_Send13.Tag = "13";
            this.btn2_Send13.Text = "发送";
            this.btn2_Send13.UseVisualStyleBackColor = true;
            this.btn2_Send13.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(441, 316);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(41, 12);
            this.label141.TabIndex = 364;
            this.label141.Text = "数据：";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(348, 316);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(41, 12);
            this.label142.TabIndex = 363;
            this.label142.Text = "帧ID：";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(237, 316);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(53, 12);
            this.label143.TabIndex = 362;
            this.label143.Text = "帧类型：";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(128, 316);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(53, 12);
            this.label144.TabIndex = 361;
            this.label144.Text = "帧格式：";
            // 
            // chb2_Send13
            // 
            this.chb2_Send13.AutoSize = true;
            this.chb2_Send13.Location = new System.Drawing.Point(9, 315);
            this.chb2_Send13.Name = "chb2_Send13";
            this.chb2_Send13.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send13.TabIndex = 360;
            this.chb2_Send13.Tag = "13";
            this.chb2_Send13.Text = "13:";
            this.chb2_Send13.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID12
            // 
            this.txb2_SendID12.Location = new System.Drawing.Point(380, 287);
            this.txb2_SendID12.Multiline = true;
            this.txb2_SendID12.Name = "txb2_SendID12";
            this.txb2_SendID12.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID12.TabIndex = 359;
            this.txb2_SendID12.Tag = "12";
            this.txb2_SendID12.Text = "01234567";
            // 
            // txb2_SendData12
            // 
            this.txb2_SendData12.Location = new System.Drawing.Point(473, 287);
            this.txb2_SendData12.Multiline = true;
            this.txb2_SendData12.Name = "txb2_SendData12";
            this.txb2_SendData12.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData12.TabIndex = 358;
            this.txb2_SendData12.Tag = "12";
            this.txb2_SendData12.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType12
            // 
            this.chb2_SendDataType12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType12.FormattingEnabled = true;
            this.chb2_SendDataType12.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType12.Location = new System.Drawing.Point(284, 286);
            this.chb2_SendDataType12.Name = "chb2_SendDataType12";
            this.chb2_SendDataType12.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType12.TabIndex = 357;
            this.chb2_SendDataType12.Tag = "12";
            this.chb2_SendDataType12.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType12
            // 
            this.cmb2_SendIDType12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType12.FormattingEnabled = true;
            this.cmb2_SendIDType12.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType12.Location = new System.Drawing.Point(177, 286);
            this.cmb2_SendIDType12.Name = "cmb2_SendIDType12";
            this.cmb2_SendIDType12.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType12.TabIndex = 356;
            this.cmb2_SendIDType12.Tag = "12";
            // 
            // btn2_Send12
            // 
            this.btn2_Send12.Location = new System.Drawing.Point(623, 286);
            this.btn2_Send12.Name = "btn2_Send12";
            this.btn2_Send12.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send12.TabIndex = 355;
            this.btn2_Send12.Tag = "12";
            this.btn2_Send12.Text = "发送";
            this.btn2_Send12.UseVisualStyleBackColor = true;
            this.btn2_Send12.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(441, 290);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(41, 12);
            this.label117.TabIndex = 354;
            this.label117.Text = "数据：";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(348, 290);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(41, 12);
            this.label118.TabIndex = 353;
            this.label118.Text = "帧ID：";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(237, 290);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(53, 12);
            this.label119.TabIndex = 352;
            this.label119.Text = "帧类型：";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(128, 290);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(53, 12);
            this.label120.TabIndex = 351;
            this.label120.Text = "帧格式：";
            // 
            // chb2_Send12
            // 
            this.chb2_Send12.AutoSize = true;
            this.chb2_Send12.Location = new System.Drawing.Point(9, 289);
            this.chb2_Send12.Name = "chb2_Send12";
            this.chb2_Send12.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send12.TabIndex = 350;
            this.chb2_Send12.Tag = "12";
            this.chb2_Send12.Text = "12:";
            this.chb2_Send12.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID11
            // 
            this.txb2_SendID11.Location = new System.Drawing.Point(380, 261);
            this.txb2_SendID11.Multiline = true;
            this.txb2_SendID11.Name = "txb2_SendID11";
            this.txb2_SendID11.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID11.TabIndex = 349;
            this.txb2_SendID11.Tag = "11";
            this.txb2_SendID11.Text = "01234567";
            // 
            // txb2_SendData11
            // 
            this.txb2_SendData11.Location = new System.Drawing.Point(473, 261);
            this.txb2_SendData11.Multiline = true;
            this.txb2_SendData11.Name = "txb2_SendData11";
            this.txb2_SendData11.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData11.TabIndex = 348;
            this.txb2_SendData11.Tag = "11";
            this.txb2_SendData11.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType11
            // 
            this.chb2_SendDataType11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType11.FormattingEnabled = true;
            this.chb2_SendDataType11.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType11.Location = new System.Drawing.Point(284, 260);
            this.chb2_SendDataType11.Name = "chb2_SendDataType11";
            this.chb2_SendDataType11.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType11.TabIndex = 347;
            this.chb2_SendDataType11.Tag = "11";
            this.chb2_SendDataType11.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType11
            // 
            this.cmb2_SendIDType11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType11.FormattingEnabled = true;
            this.cmb2_SendIDType11.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType11.Location = new System.Drawing.Point(177, 260);
            this.cmb2_SendIDType11.Name = "cmb2_SendIDType11";
            this.cmb2_SendIDType11.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType11.TabIndex = 346;
            this.cmb2_SendIDType11.Tag = "11";
            // 
            // btn2_Send11
            // 
            this.btn2_Send11.Location = new System.Drawing.Point(623, 260);
            this.btn2_Send11.Name = "btn2_Send11";
            this.btn2_Send11.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send11.TabIndex = 345;
            this.btn2_Send11.Tag = "11";
            this.btn2_Send11.Text = "发送";
            this.btn2_Send11.UseVisualStyleBackColor = true;
            this.btn2_Send11.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(441, 264);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(41, 12);
            this.label121.TabIndex = 344;
            this.label121.Text = "数据：";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(348, 264);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(41, 12);
            this.label122.TabIndex = 343;
            this.label122.Text = "帧ID：";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(237, 264);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(53, 12);
            this.label123.TabIndex = 342;
            this.label123.Text = "帧类型：";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(128, 264);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(53, 12);
            this.label124.TabIndex = 341;
            this.label124.Text = "帧格式：";
            // 
            // chb2_Send11
            // 
            this.chb2_Send11.AutoSize = true;
            this.chb2_Send11.Location = new System.Drawing.Point(9, 263);
            this.chb2_Send11.Name = "chb2_Send11";
            this.chb2_Send11.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send11.TabIndex = 340;
            this.chb2_Send11.Tag = "11";
            this.chb2_Send11.Text = "11:";
            this.chb2_Send11.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID10
            // 
            this.txb2_SendID10.Location = new System.Drawing.Point(380, 235);
            this.txb2_SendID10.Multiline = true;
            this.txb2_SendID10.Name = "txb2_SendID10";
            this.txb2_SendID10.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID10.TabIndex = 339;
            this.txb2_SendID10.Tag = "10";
            this.txb2_SendID10.Text = "01234567";
            // 
            // txb2_SendData10
            // 
            this.txb2_SendData10.Location = new System.Drawing.Point(473, 235);
            this.txb2_SendData10.Multiline = true;
            this.txb2_SendData10.Name = "txb2_SendData10";
            this.txb2_SendData10.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData10.TabIndex = 338;
            this.txb2_SendData10.Tag = "10";
            this.txb2_SendData10.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType10
            // 
            this.chb2_SendDataType10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType10.FormattingEnabled = true;
            this.chb2_SendDataType10.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType10.Location = new System.Drawing.Point(284, 234);
            this.chb2_SendDataType10.Name = "chb2_SendDataType10";
            this.chb2_SendDataType10.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType10.TabIndex = 337;
            this.chb2_SendDataType10.Tag = "10";
            this.chb2_SendDataType10.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType10
            // 
            this.cmb2_SendIDType10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType10.FormattingEnabled = true;
            this.cmb2_SendIDType10.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType10.Location = new System.Drawing.Point(177, 234);
            this.cmb2_SendIDType10.Name = "cmb2_SendIDType10";
            this.cmb2_SendIDType10.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType10.TabIndex = 336;
            this.cmb2_SendIDType10.Tag = "10";
            // 
            // btn2_Send10
            // 
            this.btn2_Send10.Location = new System.Drawing.Point(623, 234);
            this.btn2_Send10.Name = "btn2_Send10";
            this.btn2_Send10.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send10.TabIndex = 335;
            this.btn2_Send10.Tag = "10";
            this.btn2_Send10.Text = "发送";
            this.btn2_Send10.UseVisualStyleBackColor = true;
            this.btn2_Send10.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(441, 238);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(41, 12);
            this.label125.TabIndex = 334;
            this.label125.Text = "数据：";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(348, 238);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(41, 12);
            this.label126.TabIndex = 333;
            this.label126.Text = "帧ID：";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(237, 238);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(53, 12);
            this.label127.TabIndex = 332;
            this.label127.Text = "帧类型：";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(128, 238);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(53, 12);
            this.label128.TabIndex = 331;
            this.label128.Text = "帧格式：";
            // 
            // chb2_Send10
            // 
            this.chb2_Send10.AutoSize = true;
            this.chb2_Send10.Location = new System.Drawing.Point(9, 237);
            this.chb2_Send10.Name = "chb2_Send10";
            this.chb2_Send10.Size = new System.Drawing.Size(42, 16);
            this.chb2_Send10.TabIndex = 330;
            this.chb2_Send10.Tag = "10";
            this.chb2_Send10.Text = "10:";
            this.chb2_Send10.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID9
            // 
            this.txb2_SendID9.Location = new System.Drawing.Point(380, 209);
            this.txb2_SendID9.Multiline = true;
            this.txb2_SendID9.Name = "txb2_SendID9";
            this.txb2_SendID9.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID9.TabIndex = 329;
            this.txb2_SendID9.Tag = "9";
            this.txb2_SendID9.Text = "01234567";
            // 
            // txb2_SendData9
            // 
            this.txb2_SendData9.Location = new System.Drawing.Point(473, 209);
            this.txb2_SendData9.Multiline = true;
            this.txb2_SendData9.Name = "txb2_SendData9";
            this.txb2_SendData9.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData9.TabIndex = 328;
            this.txb2_SendData9.Tag = "9";
            this.txb2_SendData9.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType9
            // 
            this.chb2_SendDataType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType9.FormattingEnabled = true;
            this.chb2_SendDataType9.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType9.Location = new System.Drawing.Point(284, 208);
            this.chb2_SendDataType9.Name = "chb2_SendDataType9";
            this.chb2_SendDataType9.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType9.TabIndex = 327;
            this.chb2_SendDataType9.Tag = "9";
            this.chb2_SendDataType9.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType9
            // 
            this.cmb2_SendIDType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType9.FormattingEnabled = true;
            this.cmb2_SendIDType9.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType9.Location = new System.Drawing.Point(177, 208);
            this.cmb2_SendIDType9.Name = "cmb2_SendIDType9";
            this.cmb2_SendIDType9.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType9.TabIndex = 326;
            this.cmb2_SendIDType9.Tag = "9";
            // 
            // btn2_Send9
            // 
            this.btn2_Send9.Location = new System.Drawing.Point(623, 208);
            this.btn2_Send9.Name = "btn2_Send9";
            this.btn2_Send9.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send9.TabIndex = 325;
            this.btn2_Send9.Tag = "9";
            this.btn2_Send9.Text = "发送";
            this.btn2_Send9.UseVisualStyleBackColor = true;
            this.btn2_Send9.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(441, 212);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(41, 12);
            this.label105.TabIndex = 324;
            this.label105.Text = "数据：";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(348, 212);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(41, 12);
            this.label106.TabIndex = 323;
            this.label106.Text = "帧ID：";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(237, 212);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(53, 12);
            this.label107.TabIndex = 322;
            this.label107.Text = "帧类型：";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(128, 212);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(53, 12);
            this.label108.TabIndex = 321;
            this.label108.Text = "帧格式：";
            // 
            // chb2_Send9
            // 
            this.chb2_Send9.AutoSize = true;
            this.chb2_Send9.Location = new System.Drawing.Point(9, 211);
            this.chb2_Send9.Name = "chb2_Send9";
            this.chb2_Send9.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send9.TabIndex = 320;
            this.chb2_Send9.Tag = "9";
            this.chb2_Send9.Text = "9:";
            this.chb2_Send9.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID8
            // 
            this.txb2_SendID8.Location = new System.Drawing.Point(380, 183);
            this.txb2_SendID8.Multiline = true;
            this.txb2_SendID8.Name = "txb2_SendID8";
            this.txb2_SendID8.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID8.TabIndex = 319;
            this.txb2_SendID8.Tag = "8";
            this.txb2_SendID8.Text = "01234567";
            // 
            // txb2_SendData8
            // 
            this.txb2_SendData8.Location = new System.Drawing.Point(473, 183);
            this.txb2_SendData8.Multiline = true;
            this.txb2_SendData8.Name = "txb2_SendData8";
            this.txb2_SendData8.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData8.TabIndex = 318;
            this.txb2_SendData8.Tag = "8";
            this.txb2_SendData8.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType8
            // 
            this.chb2_SendDataType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType8.FormattingEnabled = true;
            this.chb2_SendDataType8.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType8.Location = new System.Drawing.Point(284, 182);
            this.chb2_SendDataType8.Name = "chb2_SendDataType8";
            this.chb2_SendDataType8.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType8.TabIndex = 317;
            this.chb2_SendDataType8.Tag = "8";
            this.chb2_SendDataType8.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType8
            // 
            this.cmb2_SendIDType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType8.FormattingEnabled = true;
            this.cmb2_SendIDType8.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType8.Location = new System.Drawing.Point(177, 182);
            this.cmb2_SendIDType8.Name = "cmb2_SendIDType8";
            this.cmb2_SendIDType8.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType8.TabIndex = 316;
            this.cmb2_SendIDType8.Tag = "8";
            // 
            // btn2_Send8
            // 
            this.btn2_Send8.Location = new System.Drawing.Point(623, 182);
            this.btn2_Send8.Name = "btn2_Send8";
            this.btn2_Send8.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send8.TabIndex = 315;
            this.btn2_Send8.Tag = "8";
            this.btn2_Send8.Text = "发送";
            this.btn2_Send8.UseVisualStyleBackColor = true;
            this.btn2_Send8.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(441, 186);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(41, 12);
            this.label109.TabIndex = 314;
            this.label109.Text = "数据：";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(348, 186);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(41, 12);
            this.label110.TabIndex = 313;
            this.label110.Text = "帧ID：";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(237, 186);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(53, 12);
            this.label111.TabIndex = 312;
            this.label111.Text = "帧类型：";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(128, 186);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(53, 12);
            this.label112.TabIndex = 311;
            this.label112.Text = "帧格式：";
            // 
            // chb2_Send8
            // 
            this.chb2_Send8.AutoSize = true;
            this.chb2_Send8.Location = new System.Drawing.Point(9, 185);
            this.chb2_Send8.Name = "chb2_Send8";
            this.chb2_Send8.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send8.TabIndex = 310;
            this.chb2_Send8.Tag = "8";
            this.chb2_Send8.Text = "8:";
            this.chb2_Send8.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID7
            // 
            this.txb2_SendID7.Location = new System.Drawing.Point(380, 157);
            this.txb2_SendID7.Multiline = true;
            this.txb2_SendID7.Name = "txb2_SendID7";
            this.txb2_SendID7.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID7.TabIndex = 309;
            this.txb2_SendID7.Tag = "7";
            this.txb2_SendID7.Text = "01234567";
            // 
            // txb2_SendData7
            // 
            this.txb2_SendData7.Location = new System.Drawing.Point(473, 157);
            this.txb2_SendData7.Multiline = true;
            this.txb2_SendData7.Name = "txb2_SendData7";
            this.txb2_SendData7.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData7.TabIndex = 308;
            this.txb2_SendData7.Tag = "7";
            this.txb2_SendData7.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType7
            // 
            this.chb2_SendDataType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType7.FormattingEnabled = true;
            this.chb2_SendDataType7.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType7.Location = new System.Drawing.Point(284, 156);
            this.chb2_SendDataType7.Name = "chb2_SendDataType7";
            this.chb2_SendDataType7.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType7.TabIndex = 307;
            this.chb2_SendDataType7.Tag = "7";
            this.chb2_SendDataType7.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType7
            // 
            this.cmb2_SendIDType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType7.FormattingEnabled = true;
            this.cmb2_SendIDType7.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType7.Location = new System.Drawing.Point(177, 156);
            this.cmb2_SendIDType7.Name = "cmb2_SendIDType7";
            this.cmb2_SendIDType7.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType7.TabIndex = 306;
            this.cmb2_SendIDType7.Tag = "7";
            // 
            // btn2_Send7
            // 
            this.btn2_Send7.Location = new System.Drawing.Point(623, 156);
            this.btn2_Send7.Name = "btn2_Send7";
            this.btn2_Send7.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send7.TabIndex = 305;
            this.btn2_Send7.Tag = "7";
            this.btn2_Send7.Text = "发送";
            this.btn2_Send7.UseVisualStyleBackColor = true;
            this.btn2_Send7.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(441, 160);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(41, 12);
            this.label113.TabIndex = 304;
            this.label113.Text = "数据：";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(348, 160);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(41, 12);
            this.label114.TabIndex = 303;
            this.label114.Text = "帧ID：";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(237, 160);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(53, 12);
            this.label115.TabIndex = 302;
            this.label115.Text = "帧类型：";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(128, 160);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(53, 12);
            this.label116.TabIndex = 301;
            this.label116.Text = "帧格式：";
            // 
            // chb2_Send7
            // 
            this.chb2_Send7.AutoSize = true;
            this.chb2_Send7.Location = new System.Drawing.Point(9, 159);
            this.chb2_Send7.Name = "chb2_Send7";
            this.chb2_Send7.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send7.TabIndex = 300;
            this.chb2_Send7.Tag = "7";
            this.chb2_Send7.Text = "7:";
            this.chb2_Send7.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID6
            // 
            this.txb2_SendID6.Location = new System.Drawing.Point(380, 131);
            this.txb2_SendID6.Multiline = true;
            this.txb2_SendID6.Name = "txb2_SendID6";
            this.txb2_SendID6.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID6.TabIndex = 299;
            this.txb2_SendID6.Tag = "6";
            this.txb2_SendID6.Text = "01234567";
            // 
            // txb2_SendData6
            // 
            this.txb2_SendData6.Location = new System.Drawing.Point(473, 131);
            this.txb2_SendData6.Multiline = true;
            this.txb2_SendData6.Name = "txb2_SendData6";
            this.txb2_SendData6.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData6.TabIndex = 298;
            this.txb2_SendData6.Tag = "6";
            this.txb2_SendData6.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType6
            // 
            this.chb2_SendDataType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType6.FormattingEnabled = true;
            this.chb2_SendDataType6.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType6.Location = new System.Drawing.Point(284, 130);
            this.chb2_SendDataType6.Name = "chb2_SendDataType6";
            this.chb2_SendDataType6.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType6.TabIndex = 297;
            this.chb2_SendDataType6.Tag = "6";
            this.chb2_SendDataType6.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType6
            // 
            this.cmb2_SendIDType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType6.FormattingEnabled = true;
            this.cmb2_SendIDType6.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType6.Location = new System.Drawing.Point(177, 130);
            this.cmb2_SendIDType6.Name = "cmb2_SendIDType6";
            this.cmb2_SendIDType6.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType6.TabIndex = 296;
            this.cmb2_SendIDType6.Tag = "6";
            // 
            // btn2_Send6
            // 
            this.btn2_Send6.Location = new System.Drawing.Point(623, 130);
            this.btn2_Send6.Name = "btn2_Send6";
            this.btn2_Send6.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send6.TabIndex = 295;
            this.btn2_Send6.Tag = "6";
            this.btn2_Send6.Text = "发送";
            this.btn2_Send6.UseVisualStyleBackColor = true;
            this.btn2_Send6.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(441, 134);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(41, 12);
            this.label93.TabIndex = 294;
            this.label93.Text = "数据：";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(348, 134);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(41, 12);
            this.label94.TabIndex = 293;
            this.label94.Text = "帧ID：";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(237, 134);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(53, 12);
            this.label95.TabIndex = 292;
            this.label95.Text = "帧类型：";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(128, 134);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(53, 12);
            this.label96.TabIndex = 291;
            this.label96.Text = "帧格式：";
            // 
            // chb2_Send6
            // 
            this.chb2_Send6.AutoSize = true;
            this.chb2_Send6.Location = new System.Drawing.Point(9, 133);
            this.chb2_Send6.Name = "chb2_Send6";
            this.chb2_Send6.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send6.TabIndex = 290;
            this.chb2_Send6.Tag = "6";
            this.chb2_Send6.Text = "6:";
            this.chb2_Send6.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID5
            // 
            this.txb2_SendID5.Location = new System.Drawing.Point(380, 105);
            this.txb2_SendID5.Multiline = true;
            this.txb2_SendID5.Name = "txb2_SendID5";
            this.txb2_SendID5.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID5.TabIndex = 289;
            this.txb2_SendID5.Tag = "5";
            this.txb2_SendID5.Text = "01234567";
            // 
            // txb2_SendData5
            // 
            this.txb2_SendData5.Location = new System.Drawing.Point(473, 105);
            this.txb2_SendData5.Multiline = true;
            this.txb2_SendData5.Name = "txb2_SendData5";
            this.txb2_SendData5.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData5.TabIndex = 288;
            this.txb2_SendData5.Tag = "5";
            this.txb2_SendData5.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType5
            // 
            this.chb2_SendDataType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType5.FormattingEnabled = true;
            this.chb2_SendDataType5.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType5.Location = new System.Drawing.Point(284, 104);
            this.chb2_SendDataType5.Name = "chb2_SendDataType5";
            this.chb2_SendDataType5.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType5.TabIndex = 287;
            this.chb2_SendDataType5.Tag = "5";
            this.chb2_SendDataType5.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType5
            // 
            this.cmb2_SendIDType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType5.FormattingEnabled = true;
            this.cmb2_SendIDType5.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType5.Location = new System.Drawing.Point(177, 104);
            this.cmb2_SendIDType5.Name = "cmb2_SendIDType5";
            this.cmb2_SendIDType5.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType5.TabIndex = 286;
            this.cmb2_SendIDType5.Tag = "5";
            // 
            // btn2_Send5
            // 
            this.btn2_Send5.Location = new System.Drawing.Point(623, 104);
            this.btn2_Send5.Name = "btn2_Send5";
            this.btn2_Send5.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send5.TabIndex = 285;
            this.btn2_Send5.Tag = "5";
            this.btn2_Send5.Text = "发送";
            this.btn2_Send5.UseVisualStyleBackColor = true;
            this.btn2_Send5.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(441, 108);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(41, 12);
            this.label97.TabIndex = 284;
            this.label97.Text = "数据：";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(348, 108);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(41, 12);
            this.label98.TabIndex = 283;
            this.label98.Text = "帧ID：";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(237, 108);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(53, 12);
            this.label99.TabIndex = 282;
            this.label99.Text = "帧类型：";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(128, 108);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 281;
            this.label100.Text = "帧格式：";
            // 
            // chb2_Send5
            // 
            this.chb2_Send5.AutoSize = true;
            this.chb2_Send5.Location = new System.Drawing.Point(9, 107);
            this.chb2_Send5.Name = "chb2_Send5";
            this.chb2_Send5.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send5.TabIndex = 280;
            this.chb2_Send5.Tag = "5";
            this.chb2_Send5.Text = "5:";
            this.chb2_Send5.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID4
            // 
            this.txb2_SendID4.Location = new System.Drawing.Point(380, 79);
            this.txb2_SendID4.Multiline = true;
            this.txb2_SendID4.Name = "txb2_SendID4";
            this.txb2_SendID4.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID4.TabIndex = 279;
            this.txb2_SendID4.Tag = "4";
            this.txb2_SendID4.Text = "01234567";
            // 
            // txb2_SendData4
            // 
            this.txb2_SendData4.Location = new System.Drawing.Point(473, 79);
            this.txb2_SendData4.Multiline = true;
            this.txb2_SendData4.Name = "txb2_SendData4";
            this.txb2_SendData4.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData4.TabIndex = 278;
            this.txb2_SendData4.Tag = "4";
            this.txb2_SendData4.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType4
            // 
            this.chb2_SendDataType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType4.FormattingEnabled = true;
            this.chb2_SendDataType4.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType4.Location = new System.Drawing.Point(284, 78);
            this.chb2_SendDataType4.Name = "chb2_SendDataType4";
            this.chb2_SendDataType4.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType4.TabIndex = 277;
            this.chb2_SendDataType4.Tag = "4";
            this.chb2_SendDataType4.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType4
            // 
            this.cmb2_SendIDType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType4.FormattingEnabled = true;
            this.cmb2_SendIDType4.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType4.Location = new System.Drawing.Point(177, 78);
            this.cmb2_SendIDType4.Name = "cmb2_SendIDType4";
            this.cmb2_SendIDType4.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType4.TabIndex = 276;
            this.cmb2_SendIDType4.Tag = "4";
            // 
            // btn2_Send4
            // 
            this.btn2_Send4.Location = new System.Drawing.Point(623, 78);
            this.btn2_Send4.Name = "btn2_Send4";
            this.btn2_Send4.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send4.TabIndex = 275;
            this.btn2_Send4.Tag = "4";
            this.btn2_Send4.Text = "发送";
            this.btn2_Send4.UseVisualStyleBackColor = true;
            this.btn2_Send4.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(441, 82);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(41, 12);
            this.label101.TabIndex = 274;
            this.label101.Text = "数据：";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(348, 82);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(41, 12);
            this.label102.TabIndex = 273;
            this.label102.Text = "帧ID：";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(237, 82);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(53, 12);
            this.label103.TabIndex = 272;
            this.label103.Text = "帧类型：";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(128, 82);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(53, 12);
            this.label104.TabIndex = 271;
            this.label104.Text = "帧格式：";
            // 
            // chb2_Send4
            // 
            this.chb2_Send4.AutoSize = true;
            this.chb2_Send4.Location = new System.Drawing.Point(9, 81);
            this.chb2_Send4.Name = "chb2_Send4";
            this.chb2_Send4.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send4.TabIndex = 270;
            this.chb2_Send4.Tag = "4";
            this.chb2_Send4.Text = "4:";
            this.chb2_Send4.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID3
            // 
            this.txb2_SendID3.Location = new System.Drawing.Point(380, 53);
            this.txb2_SendID3.Multiline = true;
            this.txb2_SendID3.Name = "txb2_SendID3";
            this.txb2_SendID3.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID3.TabIndex = 269;
            this.txb2_SendID3.Tag = "3";
            this.txb2_SendID3.Text = "01234567";
            // 
            // txb2_SendData3
            // 
            this.txb2_SendData3.Location = new System.Drawing.Point(473, 53);
            this.txb2_SendData3.Multiline = true;
            this.txb2_SendData3.Name = "txb2_SendData3";
            this.txb2_SendData3.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData3.TabIndex = 268;
            this.txb2_SendData3.Tag = "3";
            this.txb2_SendData3.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType3
            // 
            this.chb2_SendDataType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType3.FormattingEnabled = true;
            this.chb2_SendDataType3.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType3.Location = new System.Drawing.Point(284, 52);
            this.chb2_SendDataType3.Name = "chb2_SendDataType3";
            this.chb2_SendDataType3.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType3.TabIndex = 267;
            this.chb2_SendDataType3.Tag = "3";
            this.chb2_SendDataType3.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType3
            // 
            this.cmb2_SendIDType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType3.FormattingEnabled = true;
            this.cmb2_SendIDType3.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType3.Location = new System.Drawing.Point(177, 52);
            this.cmb2_SendIDType3.Name = "cmb2_SendIDType3";
            this.cmb2_SendIDType3.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType3.TabIndex = 266;
            this.cmb2_SendIDType3.Tag = "3";
            // 
            // btn2_Send3
            // 
            this.btn2_Send3.Location = new System.Drawing.Point(623, 52);
            this.btn2_Send3.Name = "btn2_Send3";
            this.btn2_Send3.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send3.TabIndex = 265;
            this.btn2_Send3.Tag = "3";
            this.btn2_Send3.Text = "发送";
            this.btn2_Send3.UseVisualStyleBackColor = true;
            this.btn2_Send3.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(441, 56);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(41, 12);
            this.label89.TabIndex = 264;
            this.label89.Text = "数据：";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(348, 56);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(41, 12);
            this.label90.TabIndex = 263;
            this.label90.Text = "帧ID：";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(237, 56);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(53, 12);
            this.label91.TabIndex = 262;
            this.label91.Text = "帧类型：";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(128, 56);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(53, 12);
            this.label92.TabIndex = 261;
            this.label92.Text = "帧格式：";
            // 
            // chb2_Send3
            // 
            this.chb2_Send3.AutoSize = true;
            this.chb2_Send3.Location = new System.Drawing.Point(9, 55);
            this.chb2_Send3.Name = "chb2_Send3";
            this.chb2_Send3.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send3.TabIndex = 260;
            this.chb2_Send3.Tag = "3";
            this.chb2_Send3.Text = "3:";
            this.chb2_Send3.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID2
            // 
            this.txb2_SendID2.Location = new System.Drawing.Point(380, 27);
            this.txb2_SendID2.Multiline = true;
            this.txb2_SendID2.Name = "txb2_SendID2";
            this.txb2_SendID2.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID2.TabIndex = 259;
            this.txb2_SendID2.Tag = "2";
            this.txb2_SendID2.Text = "01234567";
            // 
            // txb2_SendData2
            // 
            this.txb2_SendData2.Location = new System.Drawing.Point(473, 27);
            this.txb2_SendData2.Multiline = true;
            this.txb2_SendData2.Name = "txb2_SendData2";
            this.txb2_SendData2.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData2.TabIndex = 258;
            this.txb2_SendData2.Tag = "2";
            this.txb2_SendData2.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType2
            // 
            this.chb2_SendDataType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType2.FormattingEnabled = true;
            this.chb2_SendDataType2.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType2.Location = new System.Drawing.Point(284, 26);
            this.chb2_SendDataType2.Name = "chb2_SendDataType2";
            this.chb2_SendDataType2.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType2.TabIndex = 257;
            this.chb2_SendDataType2.Tag = "2";
            this.chb2_SendDataType2.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType2
            // 
            this.cmb2_SendIDType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType2.FormattingEnabled = true;
            this.cmb2_SendIDType2.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType2.Location = new System.Drawing.Point(177, 26);
            this.cmb2_SendIDType2.Name = "cmb2_SendIDType2";
            this.cmb2_SendIDType2.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType2.TabIndex = 256;
            this.cmb2_SendIDType2.Tag = "2";
            // 
            // btn2_Send2
            // 
            this.btn2_Send2.Location = new System.Drawing.Point(623, 26);
            this.btn2_Send2.Name = "btn2_Send2";
            this.btn2_Send2.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send2.TabIndex = 255;
            this.btn2_Send2.Tag = "2";
            this.btn2_Send2.Text = "发送";
            this.btn2_Send2.UseVisualStyleBackColor = true;
            this.btn2_Send2.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(441, 30);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(41, 12);
            this.label85.TabIndex = 254;
            this.label85.Text = "数据：";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(348, 30);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(41, 12);
            this.label86.TabIndex = 253;
            this.label86.Text = "帧ID：";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(237, 30);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(53, 12);
            this.label87.TabIndex = 252;
            this.label87.Text = "帧类型：";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(128, 30);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(53, 12);
            this.label88.TabIndex = 251;
            this.label88.Text = "帧格式：";
            // 
            // chb2_Send2
            // 
            this.chb2_Send2.AutoSize = true;
            this.chb2_Send2.Location = new System.Drawing.Point(9, 29);
            this.chb2_Send2.Name = "chb2_Send2";
            this.chb2_Send2.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send2.TabIndex = 250;
            this.chb2_Send2.Tag = "2";
            this.chb2_Send2.Text = "2:";
            this.chb2_Send2.UseVisualStyleBackColor = true;
            // 
            // txb2_SendID1
            // 
            this.txb2_SendID1.Location = new System.Drawing.Point(380, 1);
            this.txb2_SendID1.Multiline = true;
            this.txb2_SendID1.Name = "txb2_SendID1";
            this.txb2_SendID1.Size = new System.Drawing.Size(55, 18);
            this.txb2_SendID1.TabIndex = 249;
            this.txb2_SendID1.Tag = "1";
            this.txb2_SendID1.Text = "01234567";
            // 
            // txb2_SendData1
            // 
            this.txb2_SendData1.Location = new System.Drawing.Point(473, 1);
            this.txb2_SendData1.Multiline = true;
            this.txb2_SendData1.Name = "txb2_SendData1";
            this.txb2_SendData1.Size = new System.Drawing.Size(144, 18);
            this.txb2_SendData1.TabIndex = 248;
            this.txb2_SendData1.Tag = "1";
            this.txb2_SendData1.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb2_SendDataType1
            // 
            this.chb2_SendDataType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb2_SendDataType1.FormattingEnabled = true;
            this.chb2_SendDataType1.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb2_SendDataType1.Location = new System.Drawing.Point(284, 0);
            this.chb2_SendDataType1.Name = "chb2_SendDataType1";
            this.chb2_SendDataType1.Size = new System.Drawing.Size(60, 20);
            this.chb2_SendDataType1.TabIndex = 247;
            this.chb2_SendDataType1.Tag = "1";
            this.chb2_SendDataType1.TextChanged += new System.EventHandler(this.frm2_DataType);
            // 
            // cmb2_SendIDType1
            // 
            this.cmb2_SendIDType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb2_SendIDType1.FormattingEnabled = true;
            this.cmb2_SendIDType1.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb2_SendIDType1.Location = new System.Drawing.Point(177, 0);
            this.cmb2_SendIDType1.Name = "cmb2_SendIDType1";
            this.cmb2_SendIDType1.Size = new System.Drawing.Size(60, 20);
            this.cmb2_SendIDType1.TabIndex = 246;
            this.cmb2_SendIDType1.Tag = "1";
            // 
            // btn2_Send1
            // 
            this.btn2_Send1.Location = new System.Drawing.Point(623, 0);
            this.btn2_Send1.Name = "btn2_Send1";
            this.btn2_Send1.Size = new System.Drawing.Size(45, 20);
            this.btn2_Send1.TabIndex = 245;
            this.btn2_Send1.Tag = "1";
            this.btn2_Send1.Text = "发送";
            this.btn2_Send1.UseVisualStyleBackColor = true;
            this.btn2_Send1.Click += new System.EventHandler(this.btn2_Send);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(441, 4);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(41, 12);
            this.label81.TabIndex = 244;
            this.label81.Text = "数据：";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(348, 4);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(41, 12);
            this.label82.TabIndex = 243;
            this.label82.Text = "帧ID：";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(237, 4);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(53, 12);
            this.label83.TabIndex = 242;
            this.label83.Text = "帧类型：";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(128, 4);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(53, 12);
            this.label84.TabIndex = 241;
            this.label84.Text = "帧格式：";
            // 
            // chb2_Send1
            // 
            this.chb2_Send1.AutoSize = true;
            this.chb2_Send1.Location = new System.Drawing.Point(9, 3);
            this.chb2_Send1.Name = "chb2_Send1";
            this.chb2_Send1.Size = new System.Drawing.Size(36, 16);
            this.chb2_Send1.TabIndex = 240;
            this.chb2_Send1.Tag = "1";
            this.chb2_Send1.Text = "1:";
            this.chb2_Send1.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem11,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6,
            this.toolStripMenuItem7,
            this.toolStripMenuItem8,
            this.toolStripMenuItem9,
            this.toolStripMenuItem10});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(702, 25);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(68, 21);
            this.toolStripMenuItem11.Text = "透明度：";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem1.Text = "10%";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem2.Text = "20%";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem3.Text = "30%";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem4.Text = "40%";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem5.Text = "50%";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem6.Text = "60%";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem7.Text = "70%";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem8.Text = "80%";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(45, 21);
            this.toolStripMenuItem9.Text = "90%";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.MenuClick);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.BackColor = System.Drawing.SystemColors.Highlight;
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(52, 21);
            this.toolStripMenuItem10.Text = "100%";
            this.toolStripMenuItem10.Click += new System.EventHandler(this.MenuClick);
            // 
            // btn_Shrink
            // 
            this.btn_Shrink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Shrink.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Shrink.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Shrink;
            this.btn_Shrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Shrink.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_Shrink.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Shrink.Location = new System.Drawing.Point(645, 0);
            this.btn_Shrink.Name = "btn_Shrink";
            this.btn_Shrink.Size = new System.Drawing.Size(24, 26);
            this.btn_Shrink.TabIndex = 8;
            this.btn_Shrink.UseVisualStyleBackColor = false;
            this.btn_Shrink.Click += new System.EventHandler(this.btn_Shrink_Click);
            // 
            // btnPin
            // 
            this.btnPin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPin.BackColor = System.Drawing.SystemColors.Control;
            this.btnPin.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Pin0;
            this.btnPin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPin.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnPin.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPin.Location = new System.Drawing.Point(678, 0);
            this.btnPin.Name = "btnPin";
            this.btnPin.Size = new System.Drawing.Size(24, 26);
            this.btnPin.TabIndex = 7;
            this.btnPin.UseVisualStyleBackColor = false;
            this.btnPin.Click += new System.EventHandler(this.btnPin_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.透明度ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 26);
            // 
            // 透明度ToolStripMenuItem
            // 
            this.透明度ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Opacity10Percent,
            this.tsm_Opacity20Percent,
            this.tsm_Opacity30Percent,
            this.tsm_Opacity40Percent,
            this.tsm_Opacity50Percent,
            this.tsm_Opacity60Percent,
            this.tsm_Opacity70Percent,
            this.tsm_Opacity80Percent,
            this.tsm_Opacity90Percent,
            this.tsm_Opacity100Percent});
            this.透明度ToolStripMenuItem.Name = "透明度ToolStripMenuItem";
            this.透明度ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.透明度ToolStripMenuItem.Text = "透明度";
            // 
            // tsm_Opacity10Percent
            // 
            this.tsm_Opacity10Percent.Name = "tsm_Opacity10Percent";
            this.tsm_Opacity10Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity10Percent.Text = "10%";
            this.tsm_Opacity10Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity20Percent
            // 
            this.tsm_Opacity20Percent.Name = "tsm_Opacity20Percent";
            this.tsm_Opacity20Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity20Percent.Text = "20%";
            this.tsm_Opacity20Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity30Percent
            // 
            this.tsm_Opacity30Percent.Name = "tsm_Opacity30Percent";
            this.tsm_Opacity30Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity30Percent.Text = "30%";
            this.tsm_Opacity30Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity40Percent
            // 
            this.tsm_Opacity40Percent.Name = "tsm_Opacity40Percent";
            this.tsm_Opacity40Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity40Percent.Text = "40%";
            this.tsm_Opacity40Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity50Percent
            // 
            this.tsm_Opacity50Percent.Name = "tsm_Opacity50Percent";
            this.tsm_Opacity50Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity50Percent.Text = "50%";
            this.tsm_Opacity50Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity60Percent
            // 
            this.tsm_Opacity60Percent.Name = "tsm_Opacity60Percent";
            this.tsm_Opacity60Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity60Percent.Text = "60%";
            this.tsm_Opacity60Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity70Percent
            // 
            this.tsm_Opacity70Percent.Name = "tsm_Opacity70Percent";
            this.tsm_Opacity70Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity70Percent.Text = "70%";
            this.tsm_Opacity70Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity80Percent
            // 
            this.tsm_Opacity80Percent.Name = "tsm_Opacity80Percent";
            this.tsm_Opacity80Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity80Percent.Text = "80%";
            this.tsm_Opacity80Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity90Percent
            // 
            this.tsm_Opacity90Percent.Name = "tsm_Opacity90Percent";
            this.tsm_Opacity90Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity90Percent.Text = "90%";
            this.tsm_Opacity90Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // tsm_Opacity100Percent
            // 
            this.tsm_Opacity100Percent.Checked = true;
            this.tsm_Opacity100Percent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsm_Opacity100Percent.Name = "tsm_Opacity100Percent";
            this.tsm_Opacity100Percent.Size = new System.Drawing.Size(152, 22);
            this.tsm_Opacity100Percent.Text = "100%";
            this.tsm_Opacity100Percent.Click += new System.EventHandler(this.MenuClick);
            // 
            // btn_UpDowmExtend
            // 
            this.btn_UpDowmExtend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_UpDowmExtend.BackColor = System.Drawing.SystemColors.Control;
            this.btn_UpDowmExtend.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.UpDownExtend;
            this.btn_UpDowmExtend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_UpDowmExtend.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_UpDowmExtend.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_UpDowmExtend.Location = new System.Drawing.Point(612, 0);
            this.btn_UpDowmExtend.Name = "btn_UpDowmExtend";
            this.btn_UpDowmExtend.Size = new System.Drawing.Size(24, 26);
            this.btn_UpDowmExtend.TabIndex = 9;
            this.btn_UpDowmExtend.UseVisualStyleBackColor = false;
            this.btn_UpDowmExtend.Click += new System.EventHandler(this.btn_UpDowmExtend_Click);
            // 
            // btn_Adapt
            // 
            this.btn_Adapt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Adapt.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Adapt.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Adapt;
            this.btn_Adapt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Adapt.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_Adapt.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Adapt.Location = new System.Drawing.Point(579, 0);
            this.btn_Adapt.Name = "btn_Adapt";
            this.btn_Adapt.Size = new System.Drawing.Size(24, 26);
            this.btn_Adapt.TabIndex = 10;
            this.btn_Adapt.UseVisualStyleBackColor = false;
            this.btn_Adapt.Click += new System.EventHandler(this.btn_Adapt_Click);
            // 
            // tmr2_Send
            // 
            this.tmr2_Send.Tick += new System.EventHandler(this.tmr2_Send_Tick);
            // 
            // Frm_SendArray
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 750);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.btn_Adapt);
            this.Controls.Add(this.btn_UpDowmExtend);
            this.Controls.Add(this.btn_Shrink);
            this.Controls.Add(this.btnPin);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Frm_SendArray";
            this.Text = "批量发送-WeChat：TC4300_QQ：43058655_@CRJ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_SendArray_FormClosing);
            this.Load += new System.EventHandler(this.Frm_SendArray_Load);
            this.groupBox1.ResumeLayout(false);
            this.pl2_SendArray.ResumeLayout(false);
            this.pl2_SendArray.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button btn_Shrink;
        private System.Windows.Forms.Button btnPin;
        private System.Windows.Forms.Panel pl2_SendArray;
        private System.Windows.Forms.TextBox txb2_Specification27;
        private System.Windows.Forms.TextBox txb2_Specification28;
        private System.Windows.Forms.TextBox txb2_Specification29;
        private System.Windows.Forms.TextBox txb2_Specification30;
        private System.Windows.Forms.TextBox txb2_Specification31;
        private System.Windows.Forms.TextBox txb2_Specification32;
        private System.Windows.Forms.TextBox txb2_SendID32;
        private System.Windows.Forms.TextBox txb2_SendData32;
        private System.Windows.Forms.ComboBox chb2_SendDataType32;
        private System.Windows.Forms.ComboBox cmb2_SendIDType32;
        private System.Windows.Forms.Button btn2_Send32;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox chb2_Send32;
        private System.Windows.Forms.TextBox txb2_SendID31;
        private System.Windows.Forms.TextBox txb2_SendData31;
        private System.Windows.Forms.ComboBox chb2_SendDataType31;
        private System.Windows.Forms.ComboBox cmb2_SendIDType31;
        private System.Windows.Forms.Button btn2_Send31;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chb2_Send31;
        private System.Windows.Forms.TextBox txb2_SendID30;
        private System.Windows.Forms.TextBox txb2_SendData30;
        private System.Windows.Forms.ComboBox chb2_SendDataType30;
        private System.Windows.Forms.ComboBox cmb2_SendIDType30;
        private System.Windows.Forms.Button btn2_Send30;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chb2_Send30;
        private System.Windows.Forms.TextBox txb2_SendID29;
        private System.Windows.Forms.TextBox txb2_SendData29;
        private System.Windows.Forms.ComboBox chb2_SendDataType29;
        private System.Windows.Forms.ComboBox cmb2_SendIDType29;
        private System.Windows.Forms.Button btn2_Send29;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.CheckBox chb2_Send29;
        private System.Windows.Forms.TextBox txb2_SendID28;
        private System.Windows.Forms.TextBox txb2_SendData28;
        private System.Windows.Forms.ComboBox chb2_SendDataType28;
        private System.Windows.Forms.ComboBox cmb2_SendIDType28;
        private System.Windows.Forms.Button btn2_Send28;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox chb2_Send28;
        private System.Windows.Forms.TextBox txb2_SendID27;
        private System.Windows.Forms.TextBox txb2_SendData27;
        private System.Windows.Forms.ComboBox chb2_SendDataType27;
        private System.Windows.Forms.ComboBox cmb2_SendIDType27;
        private System.Windows.Forms.Button btn2_Send27;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.CheckBox chb2_Send27;
        private System.Windows.Forms.TextBox txb2_Specification21;
        private System.Windows.Forms.TextBox txb2_Specification22;
        private System.Windows.Forms.TextBox txb2_Specification23;
        private System.Windows.Forms.TextBox txb2_Specification24;
        private System.Windows.Forms.TextBox txb2_Specification25;
        private System.Windows.Forms.TextBox txb2_Specification26;
        private System.Windows.Forms.TextBox txb2_SendID26;
        private System.Windows.Forms.TextBox txb2_SendData26;
        private System.Windows.Forms.ComboBox chb2_SendDataType26;
        private System.Windows.Forms.ComboBox cmb2_SendIDType26;
        private System.Windows.Forms.Button btn2_Send26;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chb2_Send26;
        private System.Windows.Forms.TextBox txb2_SendID25;
        private System.Windows.Forms.TextBox txb2_SendData25;
        private System.Windows.Forms.ComboBox chb2_SendDataType25;
        private System.Windows.Forms.ComboBox cmb2_SendIDType25;
        private System.Windows.Forms.Button btn2_Send25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chb2_Send25;
        private System.Windows.Forms.TextBox txb2_SendID24;
        private System.Windows.Forms.TextBox txb2_SendData24;
        private System.Windows.Forms.ComboBox chb2_SendDataType24;
        private System.Windows.Forms.ComboBox cmb2_SendIDType24;
        private System.Windows.Forms.Button btn2_Send24;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chb2_Send24;
        private System.Windows.Forms.TextBox txb2_SendID23;
        private System.Windows.Forms.TextBox txb2_SendData23;
        private System.Windows.Forms.ComboBox chb2_SendDataType23;
        private System.Windows.Forms.ComboBox cmb2_SendIDType23;
        private System.Windows.Forms.Button btn2_Send23;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chb2_Send23;
        private System.Windows.Forms.TextBox txb2_SendID22;
        private System.Windows.Forms.TextBox txb2_SendData22;
        private System.Windows.Forms.ComboBox chb2_SendDataType22;
        private System.Windows.Forms.ComboBox cmb2_SendIDType22;
        private System.Windows.Forms.Button btn2_Send22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chb2_Send22;
        private System.Windows.Forms.TextBox txb2_SendID21;
        private System.Windows.Forms.TextBox txb2_SendData21;
        private System.Windows.Forms.ComboBox chb2_SendDataType21;
        private System.Windows.Forms.ComboBox cmb2_SendIDType21;
        private System.Windows.Forms.Button btn2_Send21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox chb2_Send21;
        private System.Windows.Forms.TextBox txb2_Specification1;
        private System.Windows.Forms.TextBox txb2_Specification2;
        private System.Windows.Forms.TextBox txb2_Specification3;
        private System.Windows.Forms.TextBox txb2_Specification4;
        private System.Windows.Forms.TextBox txb2_Specification5;
        private System.Windows.Forms.TextBox txb2_Specification6;
        private System.Windows.Forms.TextBox txb2_Specification7;
        private System.Windows.Forms.TextBox txb2_Specification8;
        private System.Windows.Forms.TextBox txb2_Specification9;
        private System.Windows.Forms.TextBox txb2_Specification10;
        private System.Windows.Forms.TextBox txb2_Specification11;
        private System.Windows.Forms.TextBox txb2_Specification12;
        private System.Windows.Forms.TextBox txb2_Specification13;
        private System.Windows.Forms.TextBox txb2_Specification14;
        private System.Windows.Forms.TextBox txb2_Specification15;
        private System.Windows.Forms.TextBox txb2_Specification16;
        private System.Windows.Forms.TextBox txb2_Specification17;
        private System.Windows.Forms.TextBox txb2_Specification18;
        private System.Windows.Forms.TextBox txb2_Specification19;
        private System.Windows.Forms.TextBox txb2_Specification20;
        private System.Windows.Forms.TextBox txb2_SendID20;
        private System.Windows.Forms.TextBox txb2_SendData20;
        private System.Windows.Forms.ComboBox chb2_SendDataType20;
        private System.Windows.Forms.ComboBox cmb2_SendIDType20;
        private System.Windows.Forms.Button btn2_Send20;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.CheckBox chb2_Send20;
        private System.Windows.Forms.TextBox txb2_SendID19;
        private System.Windows.Forms.TextBox txb2_SendData19;
        private System.Windows.Forms.ComboBox chb2_SendDataType19;
        private System.Windows.Forms.ComboBox cmb2_SendIDType19;
        private System.Windows.Forms.Button btn2_Send19;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.CheckBox chb2_Send19;
        private System.Windows.Forms.TextBox txb2_SendID18;
        private System.Windows.Forms.TextBox txb2_SendData18;
        private System.Windows.Forms.ComboBox chb2_SendDataType18;
        private System.Windows.Forms.ComboBox cmb2_SendIDType18;
        private System.Windows.Forms.Button btn2_Send18;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.CheckBox chb2_Send18;
        private System.Windows.Forms.TextBox txb2_SendID17;
        private System.Windows.Forms.TextBox txb2_SendData17;
        private System.Windows.Forms.ComboBox chb2_SendDataType17;
        private System.Windows.Forms.ComboBox cmb2_SendIDType17;
        private System.Windows.Forms.Button btn2_Send17;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.CheckBox chb2_Send17;
        private System.Windows.Forms.TextBox txb2_SendID16;
        private System.Windows.Forms.TextBox txb2_SendData16;
        private System.Windows.Forms.ComboBox chb2_SendDataType16;
        private System.Windows.Forms.ComboBox cmb2_SendIDType16;
        private System.Windows.Forms.Button btn2_Send16;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.CheckBox chb2_Send16;
        private System.Windows.Forms.TextBox txb2_SendID15;
        private System.Windows.Forms.TextBox txb2_SendData15;
        private System.Windows.Forms.ComboBox chb2_SendDataType15;
        private System.Windows.Forms.ComboBox cmb2_SendIDType15;
        private System.Windows.Forms.Button btn2_Send15;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.CheckBox chb2_Send15;
        private System.Windows.Forms.TextBox txb2_SendID14;
        private System.Windows.Forms.TextBox txb2_SendData14;
        private System.Windows.Forms.ComboBox chb2_SendDataType14;
        private System.Windows.Forms.ComboBox cmb2_SendIDType14;
        private System.Windows.Forms.Button btn2_Send14;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.CheckBox chb2_Send14;
        private System.Windows.Forms.TextBox txb2_SendID13;
        private System.Windows.Forms.TextBox txb2_SendData13;
        private System.Windows.Forms.ComboBox chb2_SendDataType13;
        private System.Windows.Forms.ComboBox cmb2_SendIDType13;
        private System.Windows.Forms.Button btn2_Send13;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.CheckBox chb2_Send13;
        private System.Windows.Forms.TextBox txb2_SendID12;
        private System.Windows.Forms.TextBox txb2_SendData12;
        private System.Windows.Forms.ComboBox chb2_SendDataType12;
        private System.Windows.Forms.ComboBox cmb2_SendIDType12;
        private System.Windows.Forms.Button btn2_Send12;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.CheckBox chb2_Send12;
        private System.Windows.Forms.TextBox txb2_SendID11;
        private System.Windows.Forms.TextBox txb2_SendData11;
        private System.Windows.Forms.ComboBox chb2_SendDataType11;
        private System.Windows.Forms.ComboBox cmb2_SendIDType11;
        private System.Windows.Forms.Button btn2_Send11;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.CheckBox chb2_Send11;
        private System.Windows.Forms.TextBox txb2_SendID10;
        private System.Windows.Forms.TextBox txb2_SendData10;
        private System.Windows.Forms.ComboBox chb2_SendDataType10;
        private System.Windows.Forms.ComboBox cmb2_SendIDType10;
        private System.Windows.Forms.Button btn2_Send10;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.CheckBox chb2_Send10;
        private System.Windows.Forms.TextBox txb2_SendID9;
        private System.Windows.Forms.TextBox txb2_SendData9;
        private System.Windows.Forms.ComboBox chb2_SendDataType9;
        private System.Windows.Forms.ComboBox cmb2_SendIDType9;
        private System.Windows.Forms.Button btn2_Send9;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.CheckBox chb2_Send9;
        private System.Windows.Forms.TextBox txb2_SendID8;
        private System.Windows.Forms.TextBox txb2_SendData8;
        private System.Windows.Forms.ComboBox chb2_SendDataType8;
        private System.Windows.Forms.ComboBox cmb2_SendIDType8;
        private System.Windows.Forms.Button btn2_Send8;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.CheckBox chb2_Send8;
        private System.Windows.Forms.TextBox txb2_SendID7;
        private System.Windows.Forms.TextBox txb2_SendData7;
        private System.Windows.Forms.ComboBox chb2_SendDataType7;
        private System.Windows.Forms.ComboBox cmb2_SendIDType7;
        private System.Windows.Forms.Button btn2_Send7;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.CheckBox chb2_Send7;
        private System.Windows.Forms.TextBox txb2_SendID6;
        private System.Windows.Forms.TextBox txb2_SendData6;
        private System.Windows.Forms.ComboBox chb2_SendDataType6;
        private System.Windows.Forms.ComboBox cmb2_SendIDType6;
        private System.Windows.Forms.Button btn2_Send6;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.CheckBox chb2_Send6;
        private System.Windows.Forms.TextBox txb2_SendID5;
        private System.Windows.Forms.TextBox txb2_SendData5;
        private System.Windows.Forms.ComboBox chb2_SendDataType5;
        private System.Windows.Forms.ComboBox cmb2_SendIDType5;
        private System.Windows.Forms.Button btn2_Send5;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.CheckBox chb2_Send5;
        private System.Windows.Forms.TextBox txb2_SendID4;
        private System.Windows.Forms.TextBox txb2_SendData4;
        private System.Windows.Forms.ComboBox chb2_SendDataType4;
        private System.Windows.Forms.ComboBox cmb2_SendIDType4;
        private System.Windows.Forms.Button btn2_Send4;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.CheckBox chb2_Send4;
        private System.Windows.Forms.TextBox txb2_SendID3;
        private System.Windows.Forms.TextBox txb2_SendData3;
        private System.Windows.Forms.ComboBox chb2_SendDataType3;
        private System.Windows.Forms.ComboBox cmb2_SendIDType3;
        private System.Windows.Forms.Button btn2_Send3;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox chb2_Send3;
        private System.Windows.Forms.TextBox txb2_SendID2;
        private System.Windows.Forms.TextBox txb2_SendData2;
        private System.Windows.Forms.ComboBox chb2_SendDataType2;
        private System.Windows.Forms.ComboBox cmb2_SendIDType2;
        private System.Windows.Forms.Button btn2_Send2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox chb2_Send2;
        private System.Windows.Forms.TextBox txb2_SendID1;
        private System.Windows.Forms.TextBox txb2_SendData1;
        private System.Windows.Forms.ComboBox chb2_SendDataType1;
        private System.Windows.Forms.ComboBox cmb2_SendIDType1;
        private System.Windows.Forms.Button btn2_Send1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.CheckBox chb2_Send1;
        private System.Windows.Forms.Button btn2_CycleSendCancle;
        private System.Windows.Forms.TextBox txb2_SendCycleTime;
        private System.Windows.Forms.Label lab2_cycleTimeInterval;
        private System.Windows.Forms.Button btn2_SendCycle;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 透明度ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity20Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity40Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity60Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity80Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity100Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity10Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity30Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity50Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity70Percent;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity90Percent;
        private System.Windows.Forms.Button btn_UpDowmExtend;
        private System.Windows.Forms.Button btn_Adapt;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.Timer tmr2_Send;
        private System.Windows.Forms.CheckBox chb_All;
    }
}