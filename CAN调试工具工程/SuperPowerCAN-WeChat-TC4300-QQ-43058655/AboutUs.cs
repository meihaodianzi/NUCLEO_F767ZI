﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;  // 用于打开网址

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    public partial class AboutUs : Form
    {
        public AboutUs()
        {
            InitializeComponent();
        }

        private void btn_OpenWebsite_Click(object sender, EventArgs e)
        {
            Process.Start("https://shop104166087.taobao.com/");
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.zmdzbbs.com/");
        }

        private void AboutUs_FormClosing(object sender, FormClosingEventArgs e)
        {
            Frm_Main.f1.tsm_AboutUs.Enabled = Enabled;
        }

    }
}
