﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;  // 用于保存配置信息

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    public partial class Frm_SendArray : Form
    {
        /***********************************************************/
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);//系统dll导入ini写函数
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);//系统dll导入ini读函数
        string IniFile = System.AppDomain.CurrentDomain.BaseDirectory + "CAN调试器配置信息_WeChat_TC4300_QQ_43058655.ini";//ini文件名

        StringBuilder pl2_chb1 = new StringBuilder(255);
        StringBuilder pl2_chb2 = new StringBuilder(255);
        StringBuilder pl2_chb3 = new StringBuilder(255);
        StringBuilder pl2_chb4 = new StringBuilder(255);
        StringBuilder pl2_chb5 = new StringBuilder(255);
        StringBuilder pl2_chb6 = new StringBuilder(255);
        StringBuilder pl2_chb7 = new StringBuilder(255);
        StringBuilder pl2_chb8 = new StringBuilder(255);
        StringBuilder pl2_chb9 = new StringBuilder(255);
        StringBuilder pl2_chb10 = new StringBuilder(255);
        StringBuilder pl2_chb11 = new StringBuilder(255);
        StringBuilder pl2_chb12 = new StringBuilder(255);
        StringBuilder pl2_chb13 = new StringBuilder(255);
        StringBuilder pl2_chb14 = new StringBuilder(255);
        StringBuilder pl2_chb15 = new StringBuilder(255);
        StringBuilder pl2_chb16 = new StringBuilder(255);
        StringBuilder pl2_chb17 = new StringBuilder(255);
        StringBuilder pl2_chb18 = new StringBuilder(255);
        StringBuilder pl2_chb19 = new StringBuilder(255);
        StringBuilder pl2_chb20 = new StringBuilder(255);
        StringBuilder pl2_chb21 = new StringBuilder(255);
        StringBuilder pl2_chb22 = new StringBuilder(255);
        StringBuilder pl2_chb23 = new StringBuilder(255);
        StringBuilder pl2_chb24 = new StringBuilder(255);
        StringBuilder pl2_chb25 = new StringBuilder(255);
        StringBuilder pl2_chb26 = new StringBuilder(255);
        StringBuilder pl2_chb27 = new StringBuilder(255);
        StringBuilder pl2_chb28 = new StringBuilder(255);
        StringBuilder pl2_chb29 = new StringBuilder(255);
        StringBuilder pl2_chb30 = new StringBuilder(255);
        StringBuilder pl2_chb31 = new StringBuilder(255);
        StringBuilder pl2_chb32 = new StringBuilder(255);

        StringBuilder pl2_txbSave1 = new StringBuilder(255);
        StringBuilder pl2_txbSave2 = new StringBuilder(255);
        StringBuilder pl2_txbSave3 = new StringBuilder(255);
        StringBuilder pl2_txbSave4 = new StringBuilder(255);
        StringBuilder pl2_txbSave5 = new StringBuilder(255);
        StringBuilder pl2_txbSave6 = new StringBuilder(255);
        StringBuilder pl2_txbSave7 = new StringBuilder(255);
        StringBuilder pl2_txbSave8 = new StringBuilder(255);
        StringBuilder pl2_txbSave9 = new StringBuilder(255);
        StringBuilder pl2_txbSave10 = new StringBuilder(255);
        StringBuilder pl2_txbSave11 = new StringBuilder(255);
        StringBuilder pl2_txbSave12 = new StringBuilder(255);
        StringBuilder pl2_txbSave13 = new StringBuilder(255);
        StringBuilder pl2_txbSave14 = new StringBuilder(255);
        StringBuilder pl2_txbSave15 = new StringBuilder(255);
        StringBuilder pl2_txbSave16 = new StringBuilder(255);
        StringBuilder pl2_txbSave17 = new StringBuilder(255);
        StringBuilder pl2_txbSave18 = new StringBuilder(255);
        StringBuilder pl2_txbSave19 = new StringBuilder(255);
        StringBuilder pl2_txbSave20 = new StringBuilder(255);
        StringBuilder pl2_txbSave21 = new StringBuilder(255);
        StringBuilder pl2_txbSave22 = new StringBuilder(255);
        StringBuilder pl2_txbSave23 = new StringBuilder(255);
        StringBuilder pl2_txbSave24 = new StringBuilder(255);
        StringBuilder pl2_txbSave25 = new StringBuilder(255);
        StringBuilder pl2_txbSave26 = new StringBuilder(255);
        StringBuilder pl2_txbSave27 = new StringBuilder(255);
        StringBuilder pl2_txbSave28 = new StringBuilder(255);
        StringBuilder pl2_txbSave29 = new StringBuilder(255);
        StringBuilder pl2_txbSave30 = new StringBuilder(255);
        StringBuilder pl2_txbSave31 = new StringBuilder(255);
        StringBuilder pl2_txbSave32 = new StringBuilder(255);

        StringBuilder pl2_cmb_IDType1 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType2 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType3 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType4 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType5 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType6 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType7 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType8 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType9 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType10 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType11 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType12 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType13 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType14 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType15 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType16 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType17 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType18 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType19 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType20 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType21 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType22 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType23 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType24 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType25 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType26 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType27 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType28 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType29 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType30 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType31 = new StringBuilder(255);
        StringBuilder pl2_cmb_IDType32 = new StringBuilder(255);

        StringBuilder pl2_cmb_DataType1 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType2 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType3 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType4 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType5 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType6 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType7 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType8 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType9 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType10 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType11 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType12 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType13 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType14 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType15 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType16 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType17 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType18 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType19 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType20 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType21 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType22 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType23 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType24 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType25 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType26 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType27 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType28 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType29 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType30 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType31 = new StringBuilder(255);
        StringBuilder pl2_cmb_DataType32 = new StringBuilder(255);

        StringBuilder pl2_txb_ID1 = new StringBuilder(255);
        StringBuilder pl2_txb_ID2 = new StringBuilder(255);
        StringBuilder pl2_txb_ID3 = new StringBuilder(255);
        StringBuilder pl2_txb_ID4 = new StringBuilder(255);
        StringBuilder pl2_txb_ID5 = new StringBuilder(255);
        StringBuilder pl2_txb_ID6 = new StringBuilder(255);
        StringBuilder pl2_txb_ID7 = new StringBuilder(255);
        StringBuilder pl2_txb_ID8 = new StringBuilder(255);
        StringBuilder pl2_txb_ID9 = new StringBuilder(255);
        StringBuilder pl2_txb_ID10 = new StringBuilder(255);
        StringBuilder pl2_txb_ID11 = new StringBuilder(255);
        StringBuilder pl2_txb_ID12 = new StringBuilder(255);
        StringBuilder pl2_txb_ID13 = new StringBuilder(255);
        StringBuilder pl2_txb_ID14 = new StringBuilder(255);
        StringBuilder pl2_txb_ID15 = new StringBuilder(255);
        StringBuilder pl2_txb_ID16 = new StringBuilder(255);
        StringBuilder pl2_txb_ID17 = new StringBuilder(255);
        StringBuilder pl2_txb_ID18 = new StringBuilder(255);
        StringBuilder pl2_txb_ID19 = new StringBuilder(255);
        StringBuilder pl2_txb_ID20 = new StringBuilder(255);
        StringBuilder pl2_txb_ID21 = new StringBuilder(255);
        StringBuilder pl2_txb_ID22 = new StringBuilder(255);
        StringBuilder pl2_txb_ID23 = new StringBuilder(255);
        StringBuilder pl2_txb_ID24 = new StringBuilder(255);
        StringBuilder pl2_txb_ID25 = new StringBuilder(255);
        StringBuilder pl2_txb_ID26 = new StringBuilder(255);
        StringBuilder pl2_txb_ID27 = new StringBuilder(255);
        StringBuilder pl2_txb_ID28 = new StringBuilder(255);
        StringBuilder pl2_txb_ID29 = new StringBuilder(255);
        StringBuilder pl2_txb_ID30 = new StringBuilder(255);
        StringBuilder pl2_txb_ID31 = new StringBuilder(255);
        StringBuilder pl2_txb_ID32 = new StringBuilder(255);

        StringBuilder pl2_txb_Data1 = new StringBuilder(255);
        StringBuilder pl2_txb_Data2 = new StringBuilder(255);
        StringBuilder pl2_txb_Data3 = new StringBuilder(255);
        StringBuilder pl2_txb_Data4 = new StringBuilder(255);
        StringBuilder pl2_txb_Data5 = new StringBuilder(255);
        StringBuilder pl2_txb_Data6 = new StringBuilder(255);
        StringBuilder pl2_txb_Data7 = new StringBuilder(255);
        StringBuilder pl2_txb_Data8 = new StringBuilder(255);
        StringBuilder pl2_txb_Data9 = new StringBuilder(255);
        StringBuilder pl2_txb_Data10 = new StringBuilder(255);
        StringBuilder pl2_txb_Data11 = new StringBuilder(255);
        StringBuilder pl2_txb_Data12 = new StringBuilder(255);
        StringBuilder pl2_txb_Data13 = new StringBuilder(255);
        StringBuilder pl2_txb_Data14 = new StringBuilder(255);
        StringBuilder pl2_txb_Data15 = new StringBuilder(255);
        StringBuilder pl2_txb_Data16 = new StringBuilder(255);
        StringBuilder pl2_txb_Data17 = new StringBuilder(255);
        StringBuilder pl2_txb_Data18 = new StringBuilder(255);
        StringBuilder pl2_txb_Data19 = new StringBuilder(255);
        StringBuilder pl2_txb_Data20 = new StringBuilder(255);
        StringBuilder pl2_txb_Data21 = new StringBuilder(255);
        StringBuilder pl2_txb_Data22 = new StringBuilder(255);
        StringBuilder pl2_txb_Data23 = new StringBuilder(255);
        StringBuilder pl2_txb_Data24 = new StringBuilder(255);
        StringBuilder pl2_txb_Data25 = new StringBuilder(255);
        StringBuilder pl2_txb_Data26 = new StringBuilder(255);
        StringBuilder pl2_txb_Data27 = new StringBuilder(255);
        StringBuilder pl2_txb_Data28 = new StringBuilder(255);
        StringBuilder pl2_txb_Data29 = new StringBuilder(255);
        StringBuilder pl2_txb_Data30 = new StringBuilder(255);
        StringBuilder pl2_txb_Data31 = new StringBuilder(255);
        StringBuilder pl2_txb_Data32 = new StringBuilder(255);

        StringBuilder pl2_txb_Time = new StringBuilder(255);
        /***********************************************************/
        public Frm_SendArray()
        {
            InitializeComponent();
        }

        private void Frm_SendArray_Load(object sender, EventArgs e)
        {
            this.Top = 0;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            foreach(Control ctl in this.pl2_SendArray.Controls)
            {
                if(ctl is ComboBox)
                {
                    ComboBox cmb = (ComboBox)ctl;
                    cmb.SelectedIndex = 0;
                }
            }
            LoadCFG();
            {//下面是为了防止卡CPU而在函数初始化的时候关闭批量发送窗口的批量发送功能
                chb_All.Visible = false;
                btn2_SendCycle.Visible = false;
                btn2_CycleSendCancle.Visible = false;
                lab2_cycleTimeInterval.Visible = false;
                txb2_SendCycleTime.Visible = false;
            }
        }
        private void btnPin_Click(object sender, EventArgs e)
        {
            switch (this.TopMost)
            {
                case true:
                    this.TopMost = false;
                    this.btnPin.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Pin0;
                 //   this.toolTip1.SetToolTip(this.btnPin, "前端显示");
                    break;
                case false:
                    this.TopMost = true;
                    this.btnPin.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.UnPin0;
                 //   this.toolTip1.SetToolTip(this.btnPin, "取消前端显示");
                    break;
                default: break;
            }
        }

        private void btn_Adapt_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = 718;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.Top = 0;
            }
            else
            {
                this.Width = 718;
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.Top = 0;
            }
        }

        private void btn_UpDowmExtend_Click(object sender, EventArgs e)
        {
                this.Height = Screen.PrimaryScreen.WorkingArea.Height;
                this.Top = 0;
        }

        private void btn_Shrink_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width;
                this.Left = 0;
                this.Height = 600;
            }
            else
            {
                this.Height = 600;
            }
        }

        private void Frm_SendArray_FormClosing(object sender, FormClosingEventArgs e)
        {
            Frm_Main.f1.tsm_OpenSendArrayWindow.Enabled = Enabled;
            SaveCFG( );
        }
/********************************* ↓发送↓ ********************************/
        static Send_Class SB2 = new Send_Class();
        public byte[] CANSendBatchData_Array2 = new byte[30];
        public byte SendBatch_SubFun2( byte tagIndex)
        {
            string SendData = string.Empty;
            byte[] CANDataBuf = new byte[16];
            int j = 0;  // 用于存放数组的下标
            int toSendLen = 0;  // 


            ComboBox cmb_IDType = pl2_SendArray.Controls["cmb2_SendIDType" + tagIndex.ToString()] as ComboBox;
            ComboBox cmb_DataType = pl2_SendArray.Controls["chb2_SendDataType" + tagIndex.ToString()] as ComboBox;
            TextBox txb_ID = pl2_SendArray.Controls["txb2_SendID" + tagIndex.ToString()] as TextBox;
            TextBox txb_Data = pl2_SendArray.Controls["txb2_SendData" + tagIndex.ToString()] as TextBox;

            try
            {
                SB2.ID_All = Convert.ToUInt32(txb_ID.Text.Substring(0, txb_ID.Text.Length), 16);
            }
            catch
            {
                tmr2_Send.Enabled = false;
                btn2_SendCycle.BackColor = Color.Transparent;
                btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                MessageBox.Show("错误xxx:帧ID文本框 " + tagIndex.ToString() + " 输入错误,请检查", "提示");
                return 0;
            }
            {
                try
                {
                    SendData = txb_Data.Text + " ";  // 处理函数
                    for (int i = 0; i < (SendData.Length - 1); ) // 减1处理，防止越界。
                    {
                        if (SendData[i] == ' ' && SendData[i + 1] == ' ')   // 连续两个空格
                        {
                            i = i + 2;  // 移动两个指针，然后继续扫描
                        }
                        else if (SendData[i] == ' ' && SendData[i + 1] != ' ')  // 空格+字符
                        {
                            i = i + 1;  // 移动一个指针，然后继续扫描
                        }
                        else if (SendData[i] != ' ' && SendData[i + 1] == ' ')  // 字符+空格
                        {
                            //     SendDataList.Add(SendData.Substring(i, 1));
                            CANDataBuf[j] = (byte)(Convert.ToInt32(SendData.Substring(i, 1), 16));
                            toSendLen = toSendLen + 1;
                            j = j + 1;
                            i = i + 2;   //显然这是一个单个字符，这个单个字符就是需要的数据
                        }
                        else if (SendData[i] != ' ' && SendData[i + 1] != ' ')  // 字符+字符
                        {
                            CANDataBuf[j] = (byte)(Convert.ToInt32(SendData.Substring(i, 2), 16));
                            toSendLen = toSendLen + 1;
                            j = j + 1;
                            i = i + 2;
                        }
                    }
                    if (toSendLen <= 8)
                    {
                        SB2.DataLength = (byte)toSendLen;
                        SB2.TDTR = SB2.DataLength;
                        if (j < 8)
                        {
                            do
                            {
                                CANDataBuf[j] = 0;
                                j++;
                            } while (j < 8);
                        }
                        else
                        {
                            // 不需要处理
                        }
                        /*********** 更新发送寄存器，没有数据都设置为0 *******/
                        SB2.TDLR_L_18 = CANDataBuf[0];  // 从低到高更新寄存器
                        SB2.TDLR_ML_17 = CANDataBuf[1];
                        SB2.TDLR_MH_16 = CANDataBuf[2];
                        SB2.TDLR_H_15 = CANDataBuf[3];
                        SB2.TDHR_L_14 = CANDataBuf[4];
                        SB2.TDHR_ML_13 = CANDataBuf[5];
                        SB2.TDHR_MH_12 = CANDataBuf[6];
                        SB2.TDHR_H_11 = CANDataBuf[7];
                    }
                    else
                    {
                        SB2.DataLength = 0;
                        SB2.TDTR = SB2.DataLength;
                        SB2.TDLR_L_18 = 0;
                        SB2.TDLR_ML_17 = 0;
                        SB2.TDLR_MH_16 = 0;
                        SB2.TDLR_H_15 = 0;
                        SB2.TDHR_L_14 = 0;
                        SB2.TDHR_ML_13 = 0;
                        SB2.TDHR_MH_12 = 0;
                        SB2.TDHR_H_11 = 0;

                        tmr2_Send.Enabled = false;
                        btn2_SendCycle.BackColor = Color.Transparent;
                        btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                        MessageBox.Show("错误xxx：数据" + tagIndex.ToString() + "超出长度，最多只能是8个数据", "提示");
                        return 0;
                    }
                    SB2.TDTR_H_7 = (byte)(SB2.TDTR >> 24);
                    SB2.TDTR_MH_8 = (byte)(SB2.TDTR >> 16);
                    SB2.TDTR_ML_9 = (byte)(SB2.TDTR >> 8);
                    SB2.TDTR_L_10 = (byte)(SB2.TDTR);
                }
                catch
                {
                    tmr2_Send.Enabled = false;
                    btn2_SendCycle.BackColor = Color.Transparent;
                    btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                    MessageBox.Show("错误xxx：数据" + tagIndex.ToString() + "数据出错,可输入十六进制和英文空格", "提示");
                    return 0;
                }
            }
            {
                switch (cmb_IDType.Text)
                {
                    case "标准帧":
                        SB2.IDType = 0;
                        SB2.TIR &= ~(UInt32)(1 << 2);
                        if (SB2.ID_All > 0x7FF)
                        {
                            tmr2_Send.Enabled = false;
                            btn2_SendCycle.BackColor = Color.Transparent;
                            btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                            MessageBox.Show("错误xxx:标准帧下,帧ID文本框 " + tagIndex.ToString() + "最大值为0x7FF", "提示");
                            return 0;
                        }
                        else { }
                        SB2.TIR &= (UInt32)0x00000007;
                        SB2.TIR |= (UInt32)(SB2.ID_All << 21);


                        break;
                    case "扩展帧":
                        SB2.IDType = 1;
                        SB2.TIR |= (UInt32)(1 << 2);
                        if (SB2.ID_All > 0x1FFFFFFF)
                        {
                            tmr2_Send.Enabled = false;
                            btn2_SendCycle.BackColor = Color.Transparent;
                            btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                            MessageBox.Show("错误xxx:扩展帧下,帧ID文本框 " + tagIndex.ToString() + "最大值为0x1FFFFFFF", "提示");
                            return 0;
                        }
                        else { }
                        SB2.TIR &= (UInt32)0x00000007;
                        SB2.TIR |= (UInt32)(SB2.ID_All << 3);
                        break;
                    default:
                        break;
                }
                switch (cmb_DataType.Text)
                {
                    case "数据帧":
                        SB2.DataType = 0;
                        SB2.TIR &= ~(UInt32)(1 << 1);
                        break;
                    case "远程帧":
                        SB2.DataType = 1;
                        SB2.TIR |= (UInt32)(1 << 1);
                        break;
                    default:
                        break;
                }
                SB2.TIR_H_3 = (byte)(SB2.TIR >> 24);
                SB2.TIR_MH_4 = (byte)(SB2.TIR >> 16);
                SB2.TIR_ML_5 = (byte)(SB2.TIR >> 8);
                SB2.TIR_L_6 = (byte)(SB2.TIR);
            }
            { //下面为发送函数
                CANSendBatchData_Array2[0] = SB2.SendHead0_0;
                CANSendBatchData_Array2[1] = SB2.SendHead1_1;
                CANSendBatchData_Array2[2] = SB2.FuntCode_2;
                CANSendBatchData_Array2[3] = SB2.TIR_H_3;
                CANSendBatchData_Array2[4] = SB2.TIR_MH_4;
                CANSendBatchData_Array2[5] = SB2.TIR_ML_5;
                CANSendBatchData_Array2[6] = SB2.TIR_L_6;
                CANSendBatchData_Array2[7] = SB2.TDTR_H_7;
                CANSendBatchData_Array2[8] = SB2.TDTR_MH_8;
                CANSendBatchData_Array2[9] = SB2.TDTR_ML_9;
                CANSendBatchData_Array2[10] = SB2.TDTR_L_10;
                CANSendBatchData_Array2[11] = SB2.TDHR_H_11;
                CANSendBatchData_Array2[12] = SB2.TDHR_MH_12;
                CANSendBatchData_Array2[13] = SB2.TDHR_ML_13;
                CANSendBatchData_Array2[14] = SB2.TDHR_L_14;
                CANSendBatchData_Array2[15] = SB2.TDLR_H_15;
                CANSendBatchData_Array2[16] = SB2.TDLR_MH_16;
                CANSendBatchData_Array2[17] = SB2.TDLR_ML_17;
                CANSendBatchData_Array2[18] = SB2.TDLR_L_18;

                CANSendBatchData_Array2[28] = Frm_Main.f1.GetSumForCommand(CANSendBatchData_Array2);
                CANSendBatchData_Array2[29] = SB2.Tail_29;

                if (Frm_Main.f1.serialPort1.IsOpen == true)
                {
                    Frm_Main.f1.serialPort1.Write(CANSendBatchData_Array2, 0, 30);
                }
                else
                {
                    tmr2_Send.Enabled = false;
                    btn2_SendCycle.BackColor = Color.Transparent;
                    btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                    MessageBox.Show("错误777:串口没有打开", "提示");
                    return 0;
                }
            }
            { // 下面为更新dataView
                if (Frm_Main.f1.chb_NotUpdateDataGrid.Checked == false)
                {
                    Frm_Main.f1.DGVD.Index++;
                    Frm_Main.f1.DGVD.TransferDir = "发送";
                    switch (SB2.IDType)
                    {
                        case 0:
                            Frm_Main.f1.DGVD.IDType = "标准帧";
                            break;
                        case 1:
                            Frm_Main.f1.DGVD.IDType = "扩展帧";
                            break;
                        default: break;
                    }
                    switch (SB2.DataType)
                    {
                        case 0:
                            Frm_Main.f1.DGVD.DataType = "数据帧";
                            break;
                        case 1:
                            Frm_Main.f1.DGVD.DataType = "远程帧";
                            break;
                        default: break;
                    }
                    Frm_Main.f1.DGVD.FrameID = SB2.ID_All;
                    Frm_Main.f1.DGVD.DataLength = SB2.DataLength;
                    {
                        Frm_Main.f1.DGVD.Data = string.Empty;
                        if (Frm_Main.f1.DGVD.DataType == "数据帧")
                        {
                            for (byte temp = 0; temp < SB2.DataLength; temp++)
                            {
                                Frm_Main.f1.DGVD.Data += CANDataBuf[temp].ToString("X").PadLeft(2, '0') + ' ';
                            }
                        }
                        else
                        {
                            Frm_Main.f1.DGVD.Data = "--";
                        }
                    }
                    Frm_Main.f1.DGVD.OperateTime = DateTime.Now.ToString("yy-MM-dd HH:mm:ss:fff");

                    //this.Invoke(new Action(delegate
                    //{
                        //Frm_Main.f1.dataGridView1.Rows.Add(Frm_Main.f1.DGVD.Index.ToString(), Frm_Main.f1.DGVD.TransferDir, Frm_Main.f1.DGVD.OperateTime, Frm_Main.f1.DGVD.IDType, Frm_Main.f1.DGVD.DataType, Frm_Main.f1.DGVD.FrameID.ToString("X").PadLeft(8, '0'), Frm_Main.f1.DGVD.DataLength.ToString(), Frm_Main.f1.DGVD.Data);
                        //if (Frm_Main.f1.dataGridView1.RowCount >= 2)
                        //{
                        //    Frm_Main.f1.dataGridView1.FirstDisplayedScrollingRowIndex = Frm_Main.f1.dataGridView1.RowCount - 1;
                        //}
                        {
                            if (Frm_Main.f1.chb_NotUpdateDataGrid.Checked == false)
                            {

                                Frm_Main.f1.sb.Append(Frm_Main.f1.DGVD.Index.ToString().PadRight(8, ' ') + "    " + Frm_Main.f1.DGVD.TransferDir + "    " +
                                                        Frm_Main.f1.DGVD.OperateTime + "   " + Frm_Main.f1.DGVD.IDType + "      " + Frm_Main.f1.DGVD.DataType + "     " +
                                                        Frm_Main.f1.DGVD.FrameID.ToString("X").PadLeft(8, '0') + "      " + Frm_Main.f1.DGVD.DataLength.ToString() + "    " + Frm_Main.f1.DGVD.Data + "\r\n");
                            }
                            else
                            {
                                Frm_Main.f1.DGVD.Index--;
                            }

                        }
                    //}));
                }
                else { };

            }

            return 1;
        }
        private void btn2_Send(object sender, EventArgs e)
        {
            Button btn_SendBatch = (Button)sender;
            byte tagIndex = Convert.ToByte(btn_SendBatch.Tag);
            SendBatch_SubFun2(tagIndex);
        }

        private void btn2_SendCycle_Click(object sender, EventArgs e)
        {
            int timeInterval = 0;
            try
            {
                timeInterval = Convert.ToInt32(txb2_SendCycleTime.Text.Substring(0, txb2_SendCycleTime.Text.Length));
            }
            catch
            {
                MessageBox.Show("错误xxx:请输入正确的十进制时间", "提示");
                return;
            }

            if (timeInterval < 1000)
            {
                MessageBox.Show("警告xxx:最小时间不能小于1000ms", "提示");
                return;
            }
            else { }
            tmr2_Send.Interval = timeInterval;
            tmr2_Send.Enabled = Enabled;
            btn2_SendCycle.BackColor = Color.LightSkyBlue;
            btn2_CycleSendCancle.BackColor = Color.Transparent;
        }

        private void btn2_CycleSendCancle_Click(object sender, EventArgs e)
        {
            tmr2_Send.Enabled = false;
            btn2_SendCycle.BackColor = Color.Transparent;
            btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
        }

        private void tmr2_Send_Tick(object sender, EventArgs e)
        {
            int timeInterval = 0;
            try
            {
                timeInterval = Convert.ToInt32(txb2_SendCycleTime.Text.Substring(0, txb2_SendCycleTime.Text.Length));
            }
            catch
            {
                tmr2_Send.Enabled = false;
                btn2_SendCycle.BackColor = Color.Transparent;
                btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                MessageBox.Show("错误xxx:请输入正确的十进制时间", "提示");
                return;
            }
            if (timeInterval < 1000)
            {
                tmr2_Send.Enabled = false;
                btn2_SendCycle.BackColor = Color.Transparent;
                btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                MessageBox.Show("警告xxx:最小时间不能小于1000ms", "提示");
                return;
            }
            else { }
            tmr2_Send.Interval = timeInterval;
            for (byte chbIndex = 1; chbIndex <= 20; chbIndex++)
            {
                CheckBox chbSend = pl2_SendArray.Controls["chb2_Send" + chbIndex.ToString()] as CheckBox;
                switch (chbSend.Checked)
                {
                    case true:
                        if (SendBatch_SubFun2(chbIndex) == 0)
                        {
                            tmr2_Send.Enabled = false;
                            btn2_SendCycle.BackColor = Color.Transparent;
                            btn2_CycleSendCancle.BackColor = Color.LightSkyBlue;
                            return;
                        }
                        else
                        {
                        }
                        break;
                    case false:
                        break;
                    default: break;
                }
            }
        }

        private void chb_All_CheckedChanged(object sender, EventArgs e)  // 
        {
            switch(chb_All.Checked)
            {
                case true:
                    foreach (Control ctl in this.pl2_SendArray.Controls)
                    {
                        if (ctl is CheckBox)
                        {
                            CheckBox cmb = (CheckBox)ctl;
                            cmb.Checked = true;
                        }
                    }
                    break;
                case false:
                    foreach (Control ctl in this.pl2_SendArray.Controls)
                    {
                        if (ctl is CheckBox)
                        {
                            CheckBox cmb = (CheckBox)ctl;
                            cmb.Checked = false;
                        }
                    }
                    break;
                default: break;
            }
        }

/********************************* ↑发送↑ ********************************/

/********************************* ↑支持鼠标滚轮事件↑ ********************************/
        private void pl2_SendArray_MouseClick(object sender, MouseEventArgs e)
        {
            this.pl2_SendArray.Focus();
        }
   
/********************************* ↑支持鼠标滚轮事件↑ ********************************/

/********************************* ↓保存配置信息↓ ********************************/
        public void LoadCFG( )
        {
            GetPrivateProfileString("pl2", "pl2_chb1", "False", pl2_chb1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb2", "False", pl2_chb2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb3", "False", pl2_chb3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb4", "False", pl2_chb4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb5", "False", pl2_chb5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb6", "False", pl2_chb6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb7", "False", pl2_chb7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb8", "False", pl2_chb8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb9", "False", pl2_chb9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb10", "False", pl2_chb10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb11", "False", pl2_chb11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb12", "False", pl2_chb12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb13", "False", pl2_chb13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb14", "False", pl2_chb14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb15", "False", pl2_chb15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb16", "False", pl2_chb16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb17", "False", pl2_chb17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb18", "False", pl2_chb18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb19", "False", pl2_chb19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb20", "False", pl2_chb20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb21", "False", pl2_chb21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb22", "False", pl2_chb22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb23", "False", pl2_chb23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb24", "False", pl2_chb24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb25", "False", pl2_chb25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb26", "False", pl2_chb26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb27", "False", pl2_chb27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb28", "False", pl2_chb28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb29", "False", pl2_chb29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb30", "False", pl2_chb30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb31", "False", pl2_chb31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_chb32", "False", pl2_chb32, 25, IniFile);
            switch(pl2_chb1.ToString())
                {
                    case "True":
                        chb2_Send1.Checked = true;
                        break;
                    case "False":
                        chb2_Send1.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
             switch (pl2_chb2.ToString())
                {
                    case "True":
                        chb2_Send2.Checked = true;
                        break;
                    case "False":
                        chb2_Send2.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb3.ToString())
                {
                    case "True":
                        chb2_Send3.Checked = true;
                        break;
                    case "False":
                        chb2_Send3.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb4.ToString())
                {
                    case "True":
                        chb2_Send4.Checked = true;
                        break;
                    case "False":
                        chb2_Send4.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb5.ToString())
                {
                    case "True":
                        chb2_Send5.Checked = true;
                        break;
                    case "False":
                        chb2_Send5.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb6.ToString())
                {
                    case "True":
                        chb2_Send6.Checked = true;
                        break;
                    case "False":
                        chb2_Send6.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb7.ToString())
                {
                    case "True":
                        chb2_Send7.Checked = true;
                        break;
                    case "False":
                        chb2_Send7.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb8.ToString())
                {
                    case "True":
                        chb2_Send8.Checked = true;
                        break;
                    case "False":
                        chb2_Send8.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb9.ToString())
                {
                    case "True":
                        chb2_Send9.Checked = true;
                        break;
                    case "False":
                        chb2_Send9.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb10.ToString())
                {
                    case "True":
                        chb2_Send10.Checked = true;
                        break;
                    case "False":
                        chb2_Send10.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb11.ToString())
                {
                    case "True":
                        chb2_Send11.Checked = true;
                        break;
                    case "False":
                        chb2_Send11.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb12.ToString())
                {
                    case "True":
                        chb2_Send12.Checked = true;
                        break;
                    case "False":
                        chb2_Send12.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb13.ToString())
                {
                    case "True":
                        chb2_Send13.Checked = true;
                        break;
                    case "False":
                        chb2_Send13.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb14.ToString())
                {
                    case "True":
                        chb2_Send14.Checked = true;
                        break;
                    case "False":
                        chb2_Send14.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb15.ToString())
                {
                    case "True":
                        chb2_Send15.Checked = true;
                        break;
                    case "False":
                        chb2_Send15.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb16.ToString())
                {
                    case "True":
                        chb2_Send16.Checked = true;
                        break;
                    case "False":
                        chb2_Send16.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb17.ToString())
                {
                    case "True":
                        chb2_Send17.Checked = true;
                        break;
                    case "False":
                        chb2_Send17.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb18.ToString())
                {
                    case "True":
                        chb2_Send18.Checked = true;
                        break;
                    case "False":
                        chb2_Send18.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb19.ToString())
                {
                    case "True":
                        chb2_Send19.Checked = true;
                        break;
                    case "False":
                        chb2_Send19.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb20.ToString())
                {
                    case "True":
                        chb2_Send20.Checked = true;
                        break;
                    case "False":
                        chb2_Send20.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
            switch(pl2_chb21.ToString())
                {
                    case "True":
                        chb2_Send21.Checked = true;
                        break;
                    case "False":
                        chb2_Send21.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
             switch (pl2_chb22.ToString())
                {
                    case "True":
                        chb2_Send22.Checked = true;
                        break;
                    case "False":
                        chb2_Send22.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb23.ToString())
                {
                    case "True":
                        chb2_Send23.Checked = true;
                        break;
                    case "False":
                        chb2_Send23.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb24.ToString())
                {
                    case "True":
                        chb2_Send24.Checked = true;
                        break;
                    case "False":
                        chb2_Send24.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb25.ToString())
                {
                    case "True":
                        chb2_Send25.Checked = true;
                        break;
                    case "False":
                        chb2_Send25.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb26.ToString())
                {
                    case "True":
                        chb2_Send26.Checked = true;
                        break;
                    case "False":
                        chb2_Send26.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb27.ToString())
                {
                    case "True":
                        chb2_Send27.Checked = true;
                        break;
                    case "False":
                        chb2_Send27.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb28.ToString())
                {
                    case "True":
                        chb2_Send28.Checked = true;
                        break;
                    case "False":
                        chb2_Send28.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb29.ToString())
                {
                    case "True":
                        chb2_Send29.Checked = true;
                        break;
                    case "False":
                        chb2_Send29.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb30.ToString())
                {
                    case "True":
                        chb2_Send30.Checked = true;
                        break;
                    case "False":
                        chb2_Send30.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb31.ToString())
                {
                    case "True":
                        chb2_Send31.Checked = true;
                        break;
                    case "False":
                        chb2_Send31.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
                switch (pl2_chb32.ToString())
                {
                    case "True":
                        chb2_Send32.Checked = true;
                        break;
                    case "False":
                        chb2_Send32.Checked = false;
                        break;
                    default:
                        MessageBox.Show("错误xxx：加载ini时错误，请重启!\n若重启后若仍然提示该信息则删除对应的ini文件！", "提示！");
                        return;
                }
            
            GetPrivateProfileString("pl2", "pl2_txbSave1", "", pl2_txbSave1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave2", "", pl2_txbSave2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave3", "", pl2_txbSave3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave4", "", pl2_txbSave4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave5", "", pl2_txbSave5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave6", "", pl2_txbSave6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave7", "", pl2_txbSave7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave8", "", pl2_txbSave8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave9", "", pl2_txbSave9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave10", "", pl2_txbSave10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave11", "", pl2_txbSave11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave12", "", pl2_txbSave12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave13", "", pl2_txbSave13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave14", "", pl2_txbSave14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave15", "", pl2_txbSave15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave16", "", pl2_txbSave16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave17", "", pl2_txbSave17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave18", "", pl2_txbSave18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave19", "", pl2_txbSave19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave20", "", pl2_txbSave20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave21", "", pl2_txbSave21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave22", "", pl2_txbSave22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave23", "", pl2_txbSave23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave24", "", pl2_txbSave24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave25", "", pl2_txbSave25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave26", "", pl2_txbSave26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave27", "", pl2_txbSave27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave28", "", pl2_txbSave28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave29", "", pl2_txbSave29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave30", "", pl2_txbSave30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave31", "", pl2_txbSave31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txbSave32", "", pl2_txbSave32, 25, IniFile);

            txb2_Specification1.Text = pl2_txbSave1.ToString();
            txb2_Specification2.Text = pl2_txbSave2.ToString();
            txb2_Specification3.Text = pl2_txbSave3.ToString();
            txb2_Specification4.Text = pl2_txbSave4.ToString();
            txb2_Specification5.Text = pl2_txbSave5.ToString();
            txb2_Specification6.Text = pl2_txbSave6.ToString();
            txb2_Specification7.Text = pl2_txbSave7.ToString();
            txb2_Specification8.Text = pl2_txbSave8.ToString();
            txb2_Specification9.Text = pl2_txbSave9.ToString();
            txb2_Specification10.Text = pl2_txbSave10.ToString();
            txb2_Specification11.Text = pl2_txbSave11.ToString();
            txb2_Specification12.Text = pl2_txbSave12.ToString();
            txb2_Specification13.Text = pl2_txbSave13.ToString();
            txb2_Specification14.Text = pl2_txbSave14.ToString();
            txb2_Specification15.Text = pl2_txbSave15.ToString();
            txb2_Specification16.Text = pl2_txbSave16.ToString();
            txb2_Specification17.Text = pl2_txbSave17.ToString();
            txb2_Specification18.Text = pl2_txbSave18.ToString();
            txb2_Specification19.Text = pl2_txbSave19.ToString();
            txb2_Specification20.Text = pl2_txbSave20.ToString();
            txb2_Specification21.Text = pl2_txbSave21.ToString();
            txb2_Specification22.Text = pl2_txbSave22.ToString();
            txb2_Specification23.Text = pl2_txbSave23.ToString();
            txb2_Specification24.Text = pl2_txbSave24.ToString();
            txb2_Specification25.Text = pl2_txbSave25.ToString();
            txb2_Specification26.Text = pl2_txbSave26.ToString();
            txb2_Specification27.Text = pl2_txbSave27.ToString();
            txb2_Specification28.Text = pl2_txbSave28.ToString();
            txb2_Specification29.Text = pl2_txbSave29.ToString();
            txb2_Specification30.Text = pl2_txbSave30.ToString();
            txb2_Specification31.Text = pl2_txbSave31.ToString();
            txb2_Specification32.Text = pl2_txbSave32.ToString();

            GetPrivateProfileString("pl2", "pl2_cmb_IDType1", "标准帧", pl2_cmb_IDType1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType2", "标准帧", pl2_cmb_IDType2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType3", "标准帧", pl2_cmb_IDType3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType4", "标准帧", pl2_cmb_IDType4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType5", "标准帧", pl2_cmb_IDType5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType6", "标准帧", pl2_cmb_IDType6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType7", "标准帧", pl2_cmb_IDType7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType8", "标准帧", pl2_cmb_IDType8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType9", "标准帧", pl2_cmb_IDType9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType10", "标准帧", pl2_cmb_IDType10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType11", "标准帧", pl2_cmb_IDType11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType12", "标准帧", pl2_cmb_IDType12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType13", "标准帧", pl2_cmb_IDType13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType14", "标准帧", pl2_cmb_IDType14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType15", "标准帧", pl2_cmb_IDType15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType16", "标准帧", pl2_cmb_IDType16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType17", "标准帧", pl2_cmb_IDType17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType18", "标准帧", pl2_cmb_IDType18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType19", "标准帧", pl2_cmb_IDType19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType20", "标准帧", pl2_cmb_IDType20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType21", "标准帧", pl2_cmb_IDType21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType22", "标准帧", pl2_cmb_IDType22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType23", "标准帧", pl2_cmb_IDType23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType24", "标准帧", pl2_cmb_IDType24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType25", "标准帧", pl2_cmb_IDType25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType26", "标准帧", pl2_cmb_IDType26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType27", "标准帧", pl2_cmb_IDType27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType28", "标准帧", pl2_cmb_IDType28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType29", "标准帧", pl2_cmb_IDType29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType30", "标准帧", pl2_cmb_IDType30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType31", "标准帧", pl2_cmb_IDType31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_IDType32", "标准帧", pl2_cmb_IDType32, 25, IniFile);
            cmb2_SendIDType1.Text = pl2_cmb_IDType1.ToString();
            cmb2_SendIDType2.Text = pl2_cmb_IDType2.ToString();
            cmb2_SendIDType3.Text = pl2_cmb_IDType3.ToString();
            cmb2_SendIDType4.Text = pl2_cmb_IDType4.ToString();
            cmb2_SendIDType5.Text = pl2_cmb_IDType5.ToString();
            cmb2_SendIDType6.Text = pl2_cmb_IDType6.ToString();
            cmb2_SendIDType7.Text = pl2_cmb_IDType7.ToString();
            cmb2_SendIDType8.Text = pl2_cmb_IDType8.ToString();
            cmb2_SendIDType9.Text = pl2_cmb_IDType9.ToString();
            cmb2_SendIDType10.Text = pl2_cmb_IDType10.ToString();
            cmb2_SendIDType11.Text = pl2_cmb_IDType11.ToString();
            cmb2_SendIDType12.Text = pl2_cmb_IDType12.ToString();
            cmb2_SendIDType13.Text = pl2_cmb_IDType13.ToString();
            cmb2_SendIDType14.Text = pl2_cmb_IDType14.ToString();
            cmb2_SendIDType15.Text = pl2_cmb_IDType15.ToString();
            cmb2_SendIDType16.Text = pl2_cmb_IDType16.ToString();
            cmb2_SendIDType17.Text = pl2_cmb_IDType17.ToString();
            cmb2_SendIDType18.Text = pl2_cmb_IDType18.ToString();
            cmb2_SendIDType19.Text = pl2_cmb_IDType19.ToString();
            cmb2_SendIDType20.Text = pl2_cmb_IDType20.ToString();
            cmb2_SendIDType21.Text = pl2_cmb_IDType21.ToString();
            cmb2_SendIDType22.Text = pl2_cmb_IDType22.ToString();
            cmb2_SendIDType23.Text = pl2_cmb_IDType23.ToString();
            cmb2_SendIDType24.Text = pl2_cmb_IDType24.ToString();
            cmb2_SendIDType25.Text = pl2_cmb_IDType25.ToString();
            cmb2_SendIDType26.Text = pl2_cmb_IDType26.ToString();
            cmb2_SendIDType27.Text = pl2_cmb_IDType27.ToString();
            cmb2_SendIDType28.Text = pl2_cmb_IDType28.ToString();
            cmb2_SendIDType29.Text = pl2_cmb_IDType29.ToString();
            cmb2_SendIDType30.Text = pl2_cmb_IDType30.ToString();
            cmb2_SendIDType31.Text = pl2_cmb_IDType31.ToString();
            cmb2_SendIDType32.Text = pl2_cmb_IDType32.ToString();


            GetPrivateProfileString("pl2", "pl2_cmb_DataType1", "数据帧", pl2_cmb_DataType1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType2", "数据帧", pl2_cmb_DataType2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType3", "数据帧", pl2_cmb_DataType3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType4", "数据帧", pl2_cmb_DataType4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType5", "数据帧", pl2_cmb_DataType5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType6", "数据帧", pl2_cmb_DataType6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType7", "数据帧", pl2_cmb_DataType7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType8", "数据帧", pl2_cmb_DataType8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType9", "数据帧", pl2_cmb_DataType9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType10", "数据帧", pl2_cmb_DataType10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType11", "数据帧", pl2_cmb_DataType11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType12", "数据帧", pl2_cmb_DataType12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType13", "数据帧", pl2_cmb_DataType13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType14", "数据帧", pl2_cmb_DataType14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType15", "数据帧", pl2_cmb_DataType15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType16", "数据帧", pl2_cmb_DataType16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType17", "数据帧", pl2_cmb_DataType17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType18", "数据帧", pl2_cmb_DataType18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType19", "数据帧", pl2_cmb_DataType19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType20", "数据帧", pl2_cmb_DataType20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType21", "数据帧", pl2_cmb_DataType21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType22", "数据帧", pl2_cmb_DataType22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType23", "数据帧", pl2_cmb_DataType23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType24", "数据帧", pl2_cmb_DataType24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType25", "数据帧", pl2_cmb_DataType25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType26", "数据帧", pl2_cmb_DataType26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType27", "数据帧", pl2_cmb_DataType27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType28", "数据帧", pl2_cmb_DataType28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType29", "数据帧", pl2_cmb_DataType29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType30", "数据帧", pl2_cmb_DataType30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType31", "数据帧", pl2_cmb_DataType31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_cmb_DataType32", "数据帧", pl2_cmb_DataType32, 25, IniFile);
            chb2_SendDataType1.Text = pl2_cmb_DataType1.ToString();
            chb2_SendDataType2.Text = pl2_cmb_DataType2.ToString();
            chb2_SendDataType3.Text = pl2_cmb_DataType3.ToString();
            chb2_SendDataType4.Text = pl2_cmb_DataType4.ToString();
            chb2_SendDataType5.Text = pl2_cmb_DataType5.ToString();
            chb2_SendDataType6.Text = pl2_cmb_DataType6.ToString();
            chb2_SendDataType7.Text = pl2_cmb_DataType7.ToString();
            chb2_SendDataType8.Text = pl2_cmb_DataType8.ToString();
            chb2_SendDataType9.Text = pl2_cmb_DataType9.ToString();
            chb2_SendDataType10.Text = pl2_cmb_DataType10.ToString();
            chb2_SendDataType11.Text = pl2_cmb_DataType11.ToString();
            chb2_SendDataType12.Text = pl2_cmb_DataType12.ToString();
            chb2_SendDataType13.Text = pl2_cmb_DataType13.ToString();
            chb2_SendDataType14.Text = pl2_cmb_DataType14.ToString();
            chb2_SendDataType15.Text = pl2_cmb_DataType15.ToString();
            chb2_SendDataType16.Text = pl2_cmb_DataType16.ToString();
            chb2_SendDataType17.Text = pl2_cmb_DataType17.ToString();
            chb2_SendDataType18.Text = pl2_cmb_DataType18.ToString();
            chb2_SendDataType19.Text = pl2_cmb_DataType19.ToString();
            chb2_SendDataType20.Text = pl2_cmb_DataType20.ToString();
            chb2_SendDataType21.Text = pl2_cmb_DataType21.ToString();
            chb2_SendDataType22.Text = pl2_cmb_DataType22.ToString();
            chb2_SendDataType23.Text = pl2_cmb_DataType23.ToString();
            chb2_SendDataType24.Text = pl2_cmb_DataType24.ToString();
            chb2_SendDataType25.Text = pl2_cmb_DataType25.ToString();
            chb2_SendDataType26.Text = pl2_cmb_DataType26.ToString();
            chb2_SendDataType27.Text = pl2_cmb_DataType27.ToString();
            chb2_SendDataType28.Text = pl2_cmb_DataType28.ToString();
            chb2_SendDataType29.Text = pl2_cmb_DataType29.ToString();
            chb2_SendDataType30.Text = pl2_cmb_DataType30.ToString();
            chb2_SendDataType31.Text = pl2_cmb_DataType31.ToString();
            chb2_SendDataType32.Text = pl2_cmb_DataType32.ToString();
             

            GetPrivateProfileString("pl2", "pl2_txb_ID1", "00000000", pl2_txb_ID1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID2", "00000001", pl2_txb_ID2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID3", "00000002", pl2_txb_ID3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID4", "00000003", pl2_txb_ID4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID5", "00000004", pl2_txb_ID5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID6", "00000005", pl2_txb_ID6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID7", "00000006", pl2_txb_ID7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID8", "00000007", pl2_txb_ID8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID9", "00000008", pl2_txb_ID9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID10", "00000009", pl2_txb_ID10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID11", "0000000A", pl2_txb_ID11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID12", "0000000B", pl2_txb_ID12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID13", "0000000C", pl2_txb_ID13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID14", "0000000D", pl2_txb_ID14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID15", "0000000E", pl2_txb_ID15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID16", "0000000F", pl2_txb_ID16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID17", "00000010", pl2_txb_ID17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID18", "00000011", pl2_txb_ID18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID19", "00000012", pl2_txb_ID19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID20", "00000013", pl2_txb_ID20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID21", "00000014", pl2_txb_ID21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID22", "00000015", pl2_txb_ID22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID23", "00000016", pl2_txb_ID23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID24", "00000017", pl2_txb_ID24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID25", "00000018", pl2_txb_ID25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID26", "00000019", pl2_txb_ID26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID27", "0000001A", pl2_txb_ID27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID28", "0000001B", pl2_txb_ID28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID29", "0000001C", pl2_txb_ID29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID30", "0000001D", pl2_txb_ID30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID31", "0000001E", pl2_txb_ID31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_ID32", "0000001F", pl2_txb_ID32, 25, IniFile);
            txb2_SendID1.Text = pl2_txb_ID1.ToString();
            txb2_SendID2.Text = pl2_txb_ID2.ToString();
            txb2_SendID3.Text = pl2_txb_ID3.ToString();
            txb2_SendID4.Text = pl2_txb_ID4.ToString();
            txb2_SendID5.Text = pl2_txb_ID5.ToString();
            txb2_SendID6.Text = pl2_txb_ID6.ToString();
            txb2_SendID7.Text = pl2_txb_ID7.ToString();
            txb2_SendID8.Text = pl2_txb_ID8.ToString();
            txb2_SendID9.Text = pl2_txb_ID9.ToString();
            txb2_SendID10.Text = pl2_txb_ID10.ToString();
            txb2_SendID11.Text = pl2_txb_ID11.ToString();
            txb2_SendID12.Text = pl2_txb_ID12.ToString();
            txb2_SendID13.Text = pl2_txb_ID13.ToString();
            txb2_SendID14.Text = pl2_txb_ID14.ToString();
            txb2_SendID15.Text = pl2_txb_ID15.ToString();
            txb2_SendID16.Text = pl2_txb_ID16.ToString();
            txb2_SendID17.Text = pl2_txb_ID17.ToString();
            txb2_SendID18.Text = pl2_txb_ID18.ToString();
            txb2_SendID19.Text = pl2_txb_ID19.ToString();
            txb2_SendID20.Text = pl2_txb_ID20.ToString();
            txb2_SendID21.Text = pl2_txb_ID21.ToString();
            txb2_SendID22.Text = pl2_txb_ID22.ToString();
            txb2_SendID23.Text = pl2_txb_ID23.ToString();
            txb2_SendID24.Text = pl2_txb_ID24.ToString();
            txb2_SendID25.Text = pl2_txb_ID25.ToString();
            txb2_SendID26.Text = pl2_txb_ID26.ToString();
            txb2_SendID27.Text = pl2_txb_ID27.ToString();
            txb2_SendID28.Text = pl2_txb_ID28.ToString();
            txb2_SendID29.Text = pl2_txb_ID29.ToString();
            txb2_SendID30.Text = pl2_txb_ID30.ToString();
            txb2_SendID31.Text = pl2_txb_ID31.ToString();
            txb2_SendID32.Text = pl2_txb_ID32.ToString();

            GetPrivateProfileString("pl2", "pl2_txb_Data1", "00 11 22 33 44 55 66 77", pl2_txb_Data1, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data2", "11 22 33 44 55 66 77 88", pl2_txb_Data2, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data3", "22 33 44 55 66 77 88 99", pl2_txb_Data3, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data4", "33 44 55 66 77 88 99 AA", pl2_txb_Data4, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data5", "44 55 66 77 88 99 AA BB", pl2_txb_Data5, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data6", "55 66 77 88 99 AA BB CC", pl2_txb_Data6, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data7", "66 77 88 99 AA BB CC DD", pl2_txb_Data7, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data8", "77 88 99 AA BB CC DD EE", pl2_txb_Data8, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data9", "88 99 AA BB CC DD EE FF", pl2_txb_Data9, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data10", "99 AA BB CC DD EE FF 00", pl2_txb_Data10, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data11", "AA BB CC DD EE FF 00 11", pl2_txb_Data11, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data12", "BB CC DD EE FF 00 11 22", pl2_txb_Data12, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data13", "CC DD EE FF 00 11 22 33", pl2_txb_Data13, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data14", "DD EE FF 00 11 22 33 44", pl2_txb_Data14, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data15", "EE FF 00 11 22 33 44 55", pl2_txb_Data15, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data16", "FF 00 11 22 33 44 55 66", pl2_txb_Data16, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data17", "00 11 22 33 44 55 66 77", pl2_txb_Data17, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data18", "11 22 33 44 55 66 77 88", pl2_txb_Data18, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data19", "22 33 44 55 66 77 88 99", pl2_txb_Data19, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data20", "33 44 55 66 77 88 99 AA", pl2_txb_Data20, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data21", "44 55 66 77 88 99 AA BB", pl2_txb_Data21, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data22", "55 66 77 88 99 AA BB CC", pl2_txb_Data22, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data23", "66 77 88 99 AA BB CC DD", pl2_txb_Data23, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data24", "77 88 99 AA BB CC DD EE", pl2_txb_Data24, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data25", "88 99 AA BB CC DD EE FF", pl2_txb_Data25, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data26", "99 AA BB CC DD EE FF 00", pl2_txb_Data26, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data27", "AA BB CC DD EE FF 00 11", pl2_txb_Data27, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data28", "BB CC DD EE FF 00 11 22", pl2_txb_Data28, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data29", "CC DD EE FF 00 11 22 33", pl2_txb_Data29, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data30", "DD EE FF 00 11 22 33 44", pl2_txb_Data30, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data31", "EE FF 00 11 22 33 44 55", pl2_txb_Data31, 25, IniFile);
            GetPrivateProfileString("pl2", "pl2_txb_Data32", "FF 00 11 22 33 44 55 66", pl2_txb_Data32, 25, IniFile);
            txb2_SendData1.Text = pl2_txb_Data1.ToString();
            txb2_SendData2.Text = pl2_txb_Data2.ToString();
            txb2_SendData3.Text = pl2_txb_Data3.ToString();
            txb2_SendData4.Text = pl2_txb_Data4.ToString();
            txb2_SendData5.Text = pl2_txb_Data5.ToString();
            txb2_SendData6.Text = pl2_txb_Data6.ToString();
            txb2_SendData7.Text = pl2_txb_Data7.ToString();
            txb2_SendData8.Text = pl2_txb_Data8.ToString();
            txb2_SendData9.Text = pl2_txb_Data9.ToString();
            txb2_SendData10.Text = pl2_txb_Data10.ToString();
            txb2_SendData11.Text = pl2_txb_Data11.ToString();
            txb2_SendData12.Text = pl2_txb_Data12.ToString();
            txb2_SendData13.Text = pl2_txb_Data13.ToString();
            txb2_SendData14.Text = pl2_txb_Data14.ToString();
            txb2_SendData15.Text = pl2_txb_Data15.ToString();
            txb2_SendData16.Text = pl2_txb_Data16.ToString();
            txb2_SendData17.Text = pl2_txb_Data17.ToString();
            txb2_SendData18.Text = pl2_txb_Data18.ToString();
            txb2_SendData19.Text = pl2_txb_Data19.ToString();
            txb2_SendData20.Text = pl2_txb_Data20.ToString();
            txb2_SendData21.Text = pl2_txb_Data21.ToString();
            txb2_SendData22.Text = pl2_txb_Data22.ToString();
            txb2_SendData23.Text = pl2_txb_Data23.ToString();
            txb2_SendData24.Text = pl2_txb_Data24.ToString();
            txb2_SendData25.Text = pl2_txb_Data25.ToString();
            txb2_SendData26.Text = pl2_txb_Data26.ToString();
            txb2_SendData27.Text = pl2_txb_Data27.ToString();
            txb2_SendData28.Text = pl2_txb_Data28.ToString();
            txb2_SendData29.Text = pl2_txb_Data29.ToString();
            txb2_SendData30.Text = pl2_txb_Data30.ToString();
            txb2_SendData31.Text = pl2_txb_Data31.ToString();
            txb2_SendData32.Text = pl2_txb_Data32.ToString();

            GetPrivateProfileString("pl2", "pl2_txb_Time", "1000", pl2_txb_Time, 25, IniFile);
            txb2_SendCycleTime.Text = pl2_txb_Time.ToString();
        }
        public void SaveCFG( )
        {
            for (byte temp0 = 1; temp0 <= 32; temp0++)
                {
                    CheckBox myBox = pl2_SendArray.Controls["chb2_Send" + temp0.ToString()] as CheckBox;
                    WritePrivateProfileString("pl2", "pl2_chb" + temp0.ToString(), myBox.Checked.ToString(), IniFile);

                    TextBox myTxb = pl2_SendArray.Controls["txb2_Specification" + temp0.ToString()] as TextBox;
                    WritePrivateProfileString("pl2", "pl2_txbSave" + temp0.ToString(), myTxb.Text, IniFile);

                    ComboBox myCmb = pl2_SendArray.Controls["cmb2_SendIDType" + temp0.ToString()] as ComboBox;
                    WritePrivateProfileString("pl2", "pl2_cmb_IDType" + temp0.ToString(), myCmb.Text, IniFile);

                    ComboBox myCmb1 = pl2_SendArray.Controls["chb2_SendDataType" + temp0.ToString()] as ComboBox;
                    WritePrivateProfileString("pl2", "pl2_cmb_DataType" + temp0.ToString(), myCmb1.Text, IniFile);

                    TextBox myTxb0 = pl2_SendArray.Controls["txb2_SendID" + temp0.ToString()] as TextBox;
                    WritePrivateProfileString("pl2", "pl2_txb_ID" + temp0.ToString(), myTxb0.Text, IniFile);

                    TextBox myTxb1 = pl2_SendArray.Controls["txb2_SendData" + temp0.ToString()] as TextBox;
                    WritePrivateProfileString("pl2", "pl2_txb_Data" + temp0.ToString(), myTxb1.Text, IniFile);
                }
                WritePrivateProfileString("pl2", "pl2_txb_Time", txb2_SendCycleTime.Text, IniFile);
        }

        
/********************************* ↑保存配置信息↑ ********************************/
        private void frm2_DataType(object sender, EventArgs e) // DataType改变时!
        {
            ComboBox myComboBox = (ComboBox)sender;
            byte tagIndex = Convert.ToByte(myComboBox.Tag);
            TextBox myTextBox = pl2_SendArray.Controls["txb2_SendData" + tagIndex.ToString()] as TextBox;
            switch (myComboBox.Text)
            {
                case "数据帧":
                    myTextBox.Enabled = true;
                    myComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
                    break;
                case "远程帧":
                    myComboBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
                    myTextBox.Enabled = false;
                    break;
                default: break;
            }
        }

/********************************* ↓工具栏点击事件程序↓ ********************************/

        private void MenuClick(object sender, EventArgs e)
        {
            ToolStripMenuItem ts = sender as ToolStripMenuItem;
            string tsText = ts.Text;
            {
                    toolStripMenuItem1.BackColor = Color.Transparent;
                    toolStripMenuItem2.BackColor = Color.Transparent;
                    toolStripMenuItem3.BackColor = Color.Transparent;
                    toolStripMenuItem4.BackColor = Color.Transparent;
                    toolStripMenuItem5.BackColor = Color.Transparent;
                    toolStripMenuItem6.BackColor = Color.Transparent;
                    toolStripMenuItem7.BackColor = Color.Transparent;
                    toolStripMenuItem8.BackColor = Color.Transparent;
                    toolStripMenuItem9.BackColor = Color.Transparent;
                    toolStripMenuItem10.BackColor = Color.Transparent;

                    tsm_Opacity10Percent.Checked = false;
                    tsm_Opacity20Percent.Checked = false;
                    tsm_Opacity30Percent.Checked = false;
                    tsm_Opacity40Percent.Checked = false;
                    tsm_Opacity50Percent.Checked = false;
                    tsm_Opacity60Percent.Checked = false;
                    tsm_Opacity70Percent.Checked = false;
                    tsm_Opacity80Percent.Checked = false;
                    tsm_Opacity90Percent.Checked = false;
                    tsm_Opacity100Percent.Checked = false;
            }
            
            switch(tsText)
            {
                case "10%":
                    this.Opacity = 0.1;
                    toolStripMenuItem1.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity10Percent.Checked = true;
                    break;
                case "20%":
                    this.Opacity = 0.2;
                    toolStripMenuItem2.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity20Percent.Checked = true;
                    break;
                case "30%":
                    this.Opacity = 0.3;
                    toolStripMenuItem3.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity30Percent.Checked = true;
                    break;
                case "40%":
                    this.Opacity = 0.4;
                    toolStripMenuItem4.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity40Percent.Checked = true;
                    break;
                case "50%":
                    this.Opacity = 0.5;
                    toolStripMenuItem5.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity50Percent.Checked = true;
                    break;
                case "60%":
                    this.Opacity = 0.6;
                    toolStripMenuItem6.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity60Percent.Checked = true;
                    break;
                case "70%":
                    this.Opacity = 0.7;
                    toolStripMenuItem7.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity70Percent.Checked = true;
                    break;
                case "80%":
                    this.Opacity = 0.8;
                    toolStripMenuItem8.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity80Percent.Checked = true;
                    break;
                case "90%":
                    this.Opacity = 0.9;
                    toolStripMenuItem9.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity90Percent.Checked = true;
                    break;
                case "100%":
                    this.Opacity = 1.0;
                    toolStripMenuItem10.BackColor = System.Drawing.SystemColors.Highlight;
                    tsm_Opacity100Percent.Checked = true;
                    break;
                default: break;
            }
        }


    }
}





