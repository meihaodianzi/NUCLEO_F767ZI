﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    public partial class FilterCal : Form
    {
        public FilterCal()
        {
            InitializeComponent();
        }
        private void FilterCal_Load(object sender, EventArgs e)
        {
            btn_SetTopmost_Click(null, null);
            st.Reload(); // 开始的时候加载配置值

            SetFilterFun(null,null);
            AnalysisFilterFun(null, null);

        }
        Properties.Settings st = new Properties.Settings(); // 用于保存设置
        private void btn_SetTopmost_Click(object sender, EventArgs e)
        {
            switch (this.TopMost)
            {
                case true:
                    this.TopMost = false;
                    this.btn_SetTopmost.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Pin0;
                    this.toolTip1.SetToolTip(this.btn_SetTopmost, "前端显示");
                    break;
                case false:
                    this.TopMost = true;
                    this.btn_SetTopmost.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.UnPin0;
                    this.toolTip1.SetToolTip(this.btn_SetTopmost, "取消前端显示");
                    break;
                default: break;
            }
            txb_SetExtendID11.Focus();
        }

        private void AnalysisFilterFun(object sender, EventArgs e)
        {
            UInt32 AIDValue = 0;
            UInt32 MASKValue = 0;
            string FilterStandardIDValue = string.Empty;
            string FilterExtendIDValue = string.Empty;
            try
            {
                AIDValue = Convert.ToUInt32(txb_AnalysisFilter_AID.Text.Substring(0,txb_AnalysisFilter_AID.Text.Length),16);
                MASKValue = Convert.ToUInt32(txb_AnalysisFilter_MASK.Text.Substring(0, txb_AnalysisFilter_MASK.Text.Length), 16);
            }
            catch
            {
                lb_AnalysisFilter_FrameFormat.Text = "--";
                lb_AnalysisFilter_FrameType.Text = "--";
                lb_AnalysisFilter_StandardID.Text = "--";
                lb_AnalysisFilter_ExtendID.Text = "--";
                return;
            }
            {
                {   // 不管怎样，都检索出标准帧ID和扩展帧ID
                    {
                        UInt32 ID_Temp = (UInt32)(AIDValue >> 21);
                        UInt32 Mask_Temp = (UInt32)(MASKValue >> 21);
                        string ID_TempS = Convert.ToString(ID_Temp, 2).PadLeft(11, '0');
                        string Mask_TempS = Convert.ToString(Mask_Temp, 2).PadLeft(11, '0');
                        for ( int i = 0; i < ID_TempS.Length;i++ )
                        {
                            if (Mask_TempS[i] == '1')
                            {
                                FilterStandardIDValue += ID_TempS[i];
                            }
                            else
                            {
                                FilterStandardIDValue += 'X';
                            }
                            if (i == 2)
                            {
                                FilterStandardIDValue += ' ';
                            }
                        }
                    }
                    {
                        UInt32 ID_Temp = (UInt32)(AIDValue >> 3);
                        UInt32 Mask_Temp = (UInt32)(MASKValue >> 3);
                        string ID_TempS = Convert.ToString(ID_Temp, 2).PadLeft(29, '0');
                        string Mask_TempS = Convert.ToString(Mask_Temp, 2).PadLeft(29, '0');
                        for (int i = 0; i < ID_TempS.Length;i++ )
                        {
                            if (Mask_TempS[i] == '1')
                            {
                                FilterExtendIDValue += ID_TempS[i];
                            }
                            else
                            {
                                FilterExtendIDValue += 'X';
                            }
                            if ((i == 4) || (i == 12) || (i == 20))
                            {
                                FilterExtendIDValue += ' ';
                            }
                        }
                    }
                }
                {
                    if ((MASKValue & 0x02) == 0x02) // 必须关心,第一位，数据帧和远程帧
                    {
                        if ((AIDValue & 0x02) == 0x02) // 为1
                        {
                            lb_AnalysisFilter_FrameType.Text = "远程帧";
                        }
                        else // 为0
                        {
                            lb_AnalysisFilter_FrameType.Text = "数据帧";
                        }
                    }
                    else // 不必关心
                    {
                        lb_AnalysisFilter_FrameType.Text = "数据帧和远程帧";
                    }
                }
                {
                    if ((MASKValue & 0x04) == 0x04) // 必须关心，标准帧和扩展帧
                    {
                        if ((AIDValue & 0x04) == 0x04) // 扩展帧
                        {
                            lb_AnalysisFilter_FrameFormat.Text = "扩展帧";
                            {
                                lb_AnalysisFilter_DisplayExtendID.Visible = true;
                                lb_AnalysisFilter_ExtendID.Visible = true;
                                lb_AnalysisFilter_ExtendID.Text = FilterExtendIDValue;
                                lb_AnalysisFilter_DisplayStandardID.Visible = false;
                                lb_AnalysisFilter_StandardID.Visible = false;
                            }
                        }
                        else  // 标准帧
                        {
                            lb_AnalysisFilter_FrameFormat.Text = "标准帧";
                            {
                                lb_AnalysisFilter_DisplayStandardID.Visible = true;
                                lb_AnalysisFilter_StandardID.Visible = true;
                                lb_AnalysisFilter_StandardID.Text = FilterStandardIDValue;
                                lb_AnalysisFilter_DisplayExtendID.Visible = false;
                                lb_AnalysisFilter_ExtendID.Visible = false;
                            }
                        }
                        
                    }
                    else // 不必关心
                    {
                        lb_AnalysisFilter_FrameFormat.Text = "标准帧或扩展帧";
                        lb_AnalysisFilter_DisplayStandardID.Visible = true;
                        lb_AnalysisFilter_StandardID.Visible = true;
                        lb_AnalysisFilter_StandardID.Text = FilterStandardIDValue;

                        lb_AnalysisFilter_DisplayExtendID.Visible = true;
                        lb_AnalysisFilter_ExtendID.Visible = true;
                        lb_AnalysisFilter_ExtendID.Text = FilterExtendIDValue;
                    }
                }
            }
        }
        /// <summary>
        /// 设置滤波器更新函数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetFilterFun(object sender, EventArgs e)
        {
            {   // 首先先更新标准帧ID
                txb_SetStandardID10.Text = txb_SetExtendID28.Text;
                txb_SetStandardID9.Text = txb_SetExtendID27.Text;
                txb_SetStandardID8.Text = txb_SetExtendID26.Text;
                txb_SetStandardID7.Text = txb_SetExtendID25.Text;
                txb_SetStandardID6.Text = txb_SetExtendID24.Text;
                txb_SetStandardID5.Text = txb_SetExtendID23.Text;
                txb_SetStandardID4.Text = txb_SetExtendID22.Text;
                txb_SetStandardID3.Text = txb_SetExtendID21.Text;
                txb_SetStandardID2.Text = txb_SetExtendID20.Text;
                txb_SetStandardID1.Text = txb_SetExtendID19.Text;
                txb_SetStandardID0.Text = txb_SetExtendID18.Text;
            }
            {
                if (rdb_StandardFrame.Checked) // 标准帧
                {
                    {
                        lb_SetFilterStardardFrame.Visible = true;
                        for(byte i =0;i<=10;i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetStandardID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = true;
                        }
                        lb_SetFilterExtendFrame.Visible = false;
                        for (byte i = 0; i <= 17; i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetExtendID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = false;
                        }
                    }
                    string SetValueBasic = string.Empty;
                    for (int i = 10; i >= 0; i--)
                    {
                        TextBox txb_Temp = gb_SetFilter.Controls["txb_SetStandardID" + i.ToString()] as TextBox;
                        SetValueBasic = SetValueBasic + txb_Temp.Text;
                    }
                    SetValueBasic = SetValueBasic.Replace(" ", "");
                    if (SetValueBasic.Length == 11) // 说明每个txb都有正确的数据
                    {
                        string SetAIDValue = string.Empty;
                        SetAIDValue = SetValueBasic;
                        SetAIDValue = SetAIDValue.Replace('X', '0'); // 不管代替0/1都可以,因为MASK位为0,表示不关心
                        SetAIDValue = SetAIDValue.Replace('x', '0');
                        SetAIDValue = SetAIDValue + "000000000000000000" + "0"; // 最后一个0表示为标准帧

                        string SetMaskValue = string.Empty;
                        SetMaskValue = SetValueBasic;
                        SetMaskValue = SetMaskValue.Replace("0", "1"); // 显然为0/1的都用1来替换,表示必须关心
                        SetMaskValue = SetMaskValue.Replace("X", "0"); // X/x用0替换,表示不用关心
                        SetMaskValue = SetMaskValue.Replace("x", "0");
                        SetMaskValue = SetMaskValue + "000000000000000000" + "1"; // 显然后面的一串零的值无关紧要,因为确定是标准帧下! 最后一个1表示必须是标准帧
                        if (rdb_DataFrame.Checked) // 数据帧
                        {
                            SetAIDValue = SetAIDValue + "0";
                            SetMaskValue = SetMaskValue + "1";  // 必须关心
                        }
                        else if (rdb_RemoteFrame.Checked) // 远程帧
                        {
                            SetAIDValue = SetAIDValue + "1";
                            SetMaskValue = SetMaskValue + "1";  // 必须关心
                        }
                        else // 数据帧和远程帧
                        {
                            SetAIDValue = SetAIDValue + "0"; // 这个可为0/1.
                            SetMaskValue = SetMaskValue + "0";  // 表不必关心
                        }
                        SetAIDValue = SetAIDValue + "0"; // 最后一位没有用
                        SetMaskValue = SetMaskValue + "0"; // 最后一位没有用

                        txb_SetFilter_AID.Text = (Convert.ToUInt32(SetAIDValue, 2)).ToString("X").PadLeft(8, '0');
                        txb_SetFilter_MASK.Text = (Convert.ToUInt32(SetMaskValue, 2)).ToString("X").PadLeft(8, '0');
                    }
                    else
                    {
                        txb_SetFilter_AID.Text = "--";
                        txb_SetFilter_MASK.Text = "--";
                    }
                }
                else // 扩展帧/标准帧和扩展帧
                {
                    if(rdb_ExtendFrame.Checked) // 扩展帧
                    {
                        lb_SetFilterStardardFrame.Visible = false;
                        for (byte i = 0; i <= 10; i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetStandardID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = false;
                        }
                        lb_SetFilterExtendFrame.Visible = true;
                        for (byte i = 0; i <= 28; i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetExtendID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = true;
                        }
                    }
                    else // 标准帧和扩展帧
                    {
                        lb_SetFilterStardardFrame.Visible = true;
                        for (byte i = 0; i <= 10; i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetStandardID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = true;
                        }
                        lb_SetFilterExtendFrame.Visible = true;
                        for (byte i = 0; i <= 28; i++)
                        {
                            TextBox txb_Temp = gb_SetFilter.Controls["txb_SetExtendID" + i.ToString()] as TextBox;
                            txb_Temp.Visible = true;
                        }
                    }

                    string SetValueBasic = string.Empty;
                    for (int i = 28; i >= 0; i--)
                    {
                        TextBox txb_Temp = gb_SetFilter.Controls["txb_SetExtendID" + i.ToString()] as TextBox;
                        SetValueBasic = SetValueBasic + txb_Temp.Text;
                    }
                    SetValueBasic = SetValueBasic.Replace(" ", "");
                    if (SetValueBasic.Length == 29) // 说明每个txb都有正确的数据
                    {
                        string SetAIDValue = string.Empty;
                        SetAIDValue = SetValueBasic;
                        SetAIDValue = SetAIDValue.Replace('X', '0'); // 不管代替0/1都可以,因为MASK位为0,表示不关心
                        SetAIDValue = SetAIDValue.Replace('x', '0');
                        SetAIDValue = SetAIDValue + "1"; // 在扩展帧模式下:最后一个1表示为扩展帧
                                                         // 在标准帧和扩展帧模式下,这个无所谓

                        string SetMaskValue = string.Empty;
                        SetMaskValue = SetValueBasic;
                        SetMaskValue = SetMaskValue.Replace("0", "1"); // 显然为0/1的都用1来替换,表示必须关心
                        SetMaskValue = SetMaskValue.Replace("X", "0"); // X/x用0替换,表示不用关心
                        SetMaskValue = SetMaskValue.Replace("x", "0");
                        if (rdb_ExtendFrame.Checked) // 在扩展帧模式下
                        {
                            SetMaskValue = SetMaskValue + "1"; // 最后一个1表示必须是扩展帧
                        }
                        else // 在标准帧和扩展帧模式下
                        {
                            SetMaskValue = SetMaskValue + "0"; // 最后一个0表示无需关心
                        }
                        if (rdb_DataFrame.Checked) // 数据帧
                        {
                            SetAIDValue = SetAIDValue + "0";
                            SetMaskValue = SetMaskValue + "1";  // 必须关心
                        }
                        else if (rdb_RemoteFrame.Checked) // 远程帧
                        {
                            SetAIDValue = SetAIDValue + "1";
                            SetMaskValue = SetMaskValue + "1";  // 必须关心
                        }
                        else // 数据帧和远程帧
                        {
                            SetAIDValue = SetAIDValue + "0"; // 这个可为0/1.
                            SetMaskValue = SetMaskValue + "0";  // 表不必关心
                        }
                        SetAIDValue = SetAIDValue + "0"; // 最后一位没有用
                        SetMaskValue = SetMaskValue + "0"; // 最后一位没有用

                        txb_SetFilter_AID.Text = (Convert.ToUInt32(SetAIDValue, 2)).ToString("X").PadLeft(8, '0');
                        txb_SetFilter_MASK.Text = (Convert.ToUInt32(SetMaskValue, 2)).ToString("X").PadLeft(8, '0');
                    }
                    else
                    {
                        txb_SetFilter_AID.Text = "--";
                        txb_SetFilter_MASK.Text = "--";
                    }
                }
            }
            if (    ( rdb_DataFrame.Checked == false) 
                  &&(rdb_RemoteFrame.Checked == false)
                  &&(rdb_DataAndRemoteFrame.Checked == false) )
            {
                txb_SetFilter_AID.Text = "--";
                txb_SetFilter_MASK.Text = "--";
            }
            if ((rdb_StandardFrame.Checked == false)
                  && (rdb_ExtendFrame.Checked == false)
                  && (rdb_StandardAndExtendFrame.Checked == false))
            {
                txb_SetFilter_AID.Text = "--";
                txb_SetFilter_MASK.Text = "--";
            }
        }

        private void txb_AnalysisFilter_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsNumber(e.KeyChar)) && e.KeyChar != (char)8
                 && e.KeyChar != 'A' && e.KeyChar != 'B' && e.KeyChar != 'C' && e.KeyChar != 'D' && e.KeyChar != 'E' && e.KeyChar != 'F'
                 && e.KeyChar != 'a' && e.KeyChar != 'b' && e.KeyChar != 'c' && e.KeyChar != 'd' && e.KeyChar != 'e' && e.KeyChar != 'f')
            {
                e.Handled = true;  // 取消
            }
        }

        private void txb_SetID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (    e.KeyChar == '0'
                 || e.KeyChar == '1'
                 || e.KeyChar == 'x'
                 || e.KeyChar == 'X')
            {
                TextBox txb_SetExtendID = (TextBox)sender;
                byte tagIndex = Convert.ToByte(txb_SetExtendID.Tag);
                if (tagIndex > 0)
                {
                    tagIndex--;
                    TextBox txb_SetExtendIDIndex = gb_SetFilter.Controls["txb_SetExtendID" + tagIndex.ToString()] as TextBox;
                    txb_SetExtendIDIndex.Focus();
                    txb_SetExtendIDIndex.SelectAll();
                }
                else if (tagIndex == 0)
                {
                    txb_SetFilter_AID.Focus();
                }
                
            }
            else if( e.KeyChar == (char)8 )
            {
            }
            else
            {
                e.Handled = true;  // 取消
            }
        }

        private void FilterCal_FormClosing(object sender, FormClosingEventArgs e)
        {
            st.Cal_Aalysis_AIDValue = txb_AnalysisFilter_AID.Text;
            st.Cal_Aalysis_MASKValue = txb_AnalysisFilter_MASK.Text;

            st.rdb_DataFrameCheckStatus = rdb_DataFrame.Checked;
            st.rdb_RemoteFrameCheckStatus = rdb_RemoteFrame.Checked;
            st.rdb_DataAndRemoteFrameCheckStatus = rdb_DataAndRemoteFrame.Checked;
            st.rdb_StandardFrameCheckStatus = rdb_StandardFrame.Checked;
            st.rdb_ExtendFrameCheckStatus = rdb_ExtendFrame.Checked;
            st.rdb_StandardAndExtendFrameCheckStatus = rdb_StandardAndExtendFrame.Checked;

            st.txb_SetExtendIDSetting28 = txb_SetExtendID28.Text;
            st.txb_SetExtendIDSetting27 = txb_SetExtendID27.Text;
            st.txb_SetExtendIDSetting26 = txb_SetExtendID26.Text;
            st.txb_SetExtendIDSetting25 = txb_SetExtendID25.Text;
            st.txb_SetExtendIDSetting24 = txb_SetExtendID24.Text;
            st.txb_SetExtendIDSetting23 = txb_SetExtendID23.Text;
            st.txb_SetExtendIDSetting22 = txb_SetExtendID22.Text;
            st.txb_SetExtendIDSetting21 = txb_SetExtendID21.Text;
            st.txb_SetExtendIDSetting20 = txb_SetExtendID20.Text;
            st.txb_SetExtendIDSetting19 = txb_SetExtendID19.Text;
            st.txb_SetExtendIDSetting18 = txb_SetExtendID18.Text;
            st.txb_SetExtendIDSetting17 = txb_SetExtendID17.Text;
            st.txb_SetExtendIDSetting16 = txb_SetExtendID16.Text;
            st.txb_SetExtendIDSetting15 = txb_SetExtendID15.Text;
            st.txb_SetExtendIDSetting14 = txb_SetExtendID14.Text;
            st.txb_SetExtendIDSetting13 = txb_SetExtendID13.Text;
            st.txb_SetExtendIDSetting12 = txb_SetExtendID12.Text;
            st.txb_SetExtendIDSetting11 = txb_SetExtendID11.Text;
            st.txb_SetExtendIDSetting10 = txb_SetExtendID10.Text;
            st.txb_SetExtendIDSetting9 = txb_SetExtendID9.Text;
            st.txb_SetExtendIDSetting8 = txb_SetExtendID8.Text;
            st.txb_SetExtendIDSetting7 = txb_SetExtendID7.Text;
            st.txb_SetExtendIDSetting6 = txb_SetExtendID6.Text;
            st.txb_SetExtendIDSetting5 = txb_SetExtendID5.Text;
            st.txb_SetExtendIDSetting4 = txb_SetExtendID4.Text;
            st.txb_SetExtendIDSetting3 = txb_SetExtendID3.Text;
            st.txb_SetExtendIDSetting2 = txb_SetExtendID2.Text;
            st.txb_SetExtendIDSetting1 = txb_SetExtendID1.Text;
            st.txb_SetExtendIDSetting0 = txb_SetExtendID0.Text;
            st.Save();
        }
    }
}
