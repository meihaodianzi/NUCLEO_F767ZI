﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655.Resources
{
    class DataGrid_Class
    {
        public UInt32 Index = 0;
        public string TransferDirectiong = string.Empty;
        public string TransferTime = string.Empty;
        public string IDType = "标准帧";
        public string DataType = "数据帧";
        public UInt32 FrameID = 0;
        public byte DataLength = 0;
        public byte[] Data = new byte[8];
    }
}
