﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    public class DataGridViewDisplay_Class
    {
        public UInt32 Index = 0;
        public string TransferDir = "发送";
        public string OperateTime = "____-_-__ __:__:__";
        public string IDType = "标准帧";
        public string DataType = "标准帧";
        public UInt32 FrameID = 0;
        public byte DataLength = 0;
        public string Data = "__ __ __ __ __ __ __ __";
    }
}
