﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class Send_Class
    {
        public byte SendHead0_0 = 0x55;
        public byte SendHead1_1 = 0xAA;
        public byte FuntCode_2 = 0x03;

        public UInt32 ID_All = 0;
        public UInt16 TIR_StandardID = 0;
        public UInt32 TIR_ExtendID = 0;
        public byte IDType = 0;
        public byte DataType = 0; 
        public byte DataLength =0;


        public UInt32 TIR = 0;
        public byte TIR_H_3 = 0;
        public byte TIR_MH_4 = 0;
        public byte TIR_ML_5 = 0;
        public byte TIR_L_6 = 0;

        public UInt32 TDTR = 0;
        public byte TDTR_H_7 = 0;
        public byte TDTR_MH_8 = 0;
        public byte TDTR_ML_9 = 0;
        public byte TDTR_L_10 = 0;

        public UInt32 TDHR = 0;
        public byte TDHR_H_11 = 0;
        public byte TDHR_MH_12 = 0;
        public byte TDHR_ML_13 = 0;
        public byte TDHR_L_14 = 0;

        public UInt32 TDLR = 0;
        public byte TDLR_H_15 = 0;
        public byte TDLR_MH_16 = 0;
        public byte TDLR_ML_17 = 0;
        public byte TDLR_L_18 = 0;

        public byte Sum_28 = 0;
        public byte Tail_29 = 0x88;
    }
}
