﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class ForSaveExcel_Class
    {
        public string 序号 { get; set; }
        public string 传输方向 { get; set; }
        public string 时间标识 { get; set; }
        public string 帧类型 { get; set; }
        public string 帧格式 { get; set; }
        public string 帧ID { get; set; }
        public string 数据长度 { get; set; }
        public string 数据 { get; set; }
    }
}
