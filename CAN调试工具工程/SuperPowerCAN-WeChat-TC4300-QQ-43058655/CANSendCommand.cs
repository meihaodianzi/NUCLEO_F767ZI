﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class CANSendCommand_Class
    {
        public byte SendHead0 = 0x55;
        public byte SendHead1 = 0xAA;
        public byte FunctionCode = 0x03;

        public byte TIR_H    = 0x00;
        public byte TIR_MH   = 0x00;
        public byte TIR_ML   = 0x00;
        public byte TIR_L    = 0x00;
        public byte TDTR_H   = 0x00;
        public byte TDTR_MH  = 0x00;
        public byte TDTR_ML  = 0x00;
        public byte TDTR_L   = 0x00;
        public byte TDHR_H   = 0x00;
        public byte TDHR_MH  = 0x00;
        public byte TDHR_ML  = 0x00;
        public byte TDHR_L   = 0x00;
        public byte TDLR_H   = 0x00;
        public byte TDLR_MH  = 0x00;
        public byte TDLR_ML  = 0x00;
        public byte TDLR_L   = 0x00;

        public byte Dummy    = 0x00;
        public byte Sum      = 0x00;
        public byte SendTail = 0x88;
    }
}
