﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class StatusMsg_Class  // 定时发送状态
    {
        public byte Head0 = 0xAA;
        public byte Head1 = 0x55;
        public byte FunCode = 0x01;
		public byte FilterEnabledSum;
		public byte ErrorWarningFlag;
		public byte ErrorPassiveFlag;
		public byte OffLineFlag;
		public byte LastErrorCode;
		public byte TransferErrorSum;
		public byte RecvErrorSum;

        public UInt32 Baudrate = 0;
		public byte Baudrate_H;
		public byte Baudrate_M;
		public byte Baudrate_L;

        public UInt32 TransferOrderSum = 0;
		public byte TransferOrderSum_H;
		public byte TransferOrderSum_MH;
		public byte TransferOrderSum_ML;
		public byte TransferOrderSum_L;

        public UInt32 TransferSucessedSum = 0;
		public byte TransferSucessedSum_H;
		public byte TransferSucessedSum_MH;
		public byte TransferSucessedSum_ML;
		public byte TransferSucessedSum_L;

        public UInt32 TransferFailedSum = 0;
		public byte TransferFailedSum_H;
		public byte TransferFailedSum_MH;
		public byte TransferFailedSum_ML;
		public byte TransferFailedSum_L;

        public byte WorkMode;
        public byte AutoRetransferEnable;

        public byte Dummy = 0;
        public byte Sum;
        public byte Tail = 0x88;

        public UInt32 RecvSucessedSum = 0;
    }
}
